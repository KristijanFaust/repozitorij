﻿<?php
session_start();
unset($_SESSION['id']);
unset($_SESSION['tip']);
session_destroy();
header("Location: index.htm");
?>