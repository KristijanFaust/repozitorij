-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2016 at 11:39 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `muzika`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(5) NOT NULL,
  `album` char(32) NOT NULL,
  `datum_izdavanja` date NOT NULL,
  `lajkovi` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `album`, `datum_izdavanja`, `lajkovi`) VALUES
(5, 'Album1', '2016-09-14', 6),
(6, 'Album2', '2016-09-22', 0),
(7, 'Album3', '2016-09-17', 0),
(8, 'Album4', '2016-09-29', 0),
(9, 'Album5', '2016-09-16', 1),
(10, 'Album6', '2016-09-17', 12),
(11, 'Album7', '2016-09-02', 0),
(12, 'Album8', '2016-09-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `id` int(5) NOT NULL,
  `naziv` char(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `naziv`) VALUES
(1, 'Artist1'),
(2, 'Artist3'),
(3, 'Artist3'),
(4, 'Artist4'),
(5, 'Artist5'),
(6, 'Artist6'),
(7, 'Artist7'),
(8, 'Artist8');

-- --------------------------------------------------------

--
-- Table structure for table `izdaje`
--

CREATE TABLE `izdaje` (
  `id_artista` int(5) NOT NULL,
  `id_albuma` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `izdaje`
--

INSERT INTO `izdaje` (`id_artista`, `id_albuma`) VALUES
(2, 5),
(2, 6),
(3, 6),
(3, 7),
(4, 9),
(3, 8),
(4, 7),
(5, 10),
(1, 2),
(3, 9),
(5, 11),
(6, 11),
(7, 11),
(8, 12);

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(5) NOT NULL,
  `username` char(32) NOT NULL,
  `password` char(32) NOT NULL,
  `tip` set('administrator','korisnik') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `username`, `password`, `tip`) VALUES
(5, 'admin', '4a7d1ed414474e4033ac29ccb8653d9b', 'administrator'),
(6, 'korisnik', '4a7d1ed414474e4033ac29ccb8653d9b', 'korisnik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `izdaje`
--
ALTER TABLE `izdaje`
  ADD KEY `id_artista` (`id_artista`),
  ADD KEY `id_albuma` (`id_albuma`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `izdaje`
--
ALTER TABLE `izdaje`
  ADD CONSTRAINT `izdaje_ibfk_1` FOREIGN KEY (`id_artista`) REFERENCES `artist` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `izdaje_ibfk_2` FOREIGN KEY (`id_albuma`) REFERENCES `album` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
