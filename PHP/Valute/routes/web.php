<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Ruta na početnu stranicu
Route::get('/', 'ControllerPages@pocetna');

//Ruta na popis valuta
Route::get('popis', 'ControllerPages@popis');

//Ruta na konverziju valuta
Route::get('konverzija', 'ControllerPages@konverzija');

//Ruta na izračun konverzije
Route::post('konverzija', 'ControllerIzracun@pretvorba');