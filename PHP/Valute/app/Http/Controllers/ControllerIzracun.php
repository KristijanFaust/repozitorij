<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Fadion\Fixerio\Exchange;
use Fadion\Fixerio\Currency;

class ControllerIzracun extends Controller
{
     public function pretvorba(Request $request) {
      
      $valuta1 = $request->valuta1;
      $valuta2 = $request->valuta2;
      $iznos = $request->iznos;
        
      $rezultat = round($iznos * ($valuta2 / $valuta1), 2);
    
      $rates = (new Exchange())->get(); // Dobavlja sve vrijednosti svih valuta u obliku asocijativnog polja
      
      
      return view('pages.konverzija')->with('rates', $rates)->with('rezultat', $rezultat);
       
      echo $rezultat;
            
    }
}