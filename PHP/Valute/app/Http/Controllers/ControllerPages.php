<?php

namespace App\Http\Controllers;

use Fadion\Fixerio\Exchange;
use Fadion\Fixerio\Currency;

class ControllerPages extends Controller
{
    public function pocetna() {
      
      return view('pocetna');
      
    }
  
    public function popis() {
      
      $result = (new Exchange())->symbols()->getResult();

      $date = $result->getDate(); // Datum zadnjeg ažuriranja
      $rates = $result->getRates(); // Polje vrijednosti valuta
            
      return view('pages.popis')->with('date', $date)->with('rates', $rates);
      
    }
  
    public function konverzija() {
      
      $rezultat = 0.00;
      $rates = (new Exchange())->get(); // Dobavlja sve vrijednosti svih valuta u obliku asocijativnog polja
      
      return view('pages.konverzija')->with('rates', $rates)->with('rezultat', $rezultat);
            
    }
}
