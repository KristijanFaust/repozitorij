@extends('layout')

@section('header')

  <div class="container">
    <div class="content">
      <div class="title">Popis valuta</div>
      <div class="napomena"> <b>(Vrijednost naspram EUR)</b> </div>
      <hr>
    </div>
  </div>
@stop

@section('content')

<div class="container">
    <div class="content">
      
      <p><b>Datum zadnjeg ažuriranja: {{ $date->format('Y-m-d') }}</b></p>
       
      <p>
        <table align="center">
          <thead>
            
            <tr>
              <th>Valuta</th>
              <th>Srednja vrijednost</th>
            </tr>
          
          @foreach($rates as $key => $value)
          
          <tr><td><b>{{$key}}:</b></td> 
            
          <td><b>{{$value}}</b></td></tr>
          
          @endforeach
          
        </table>
      
      </p>
  </div>
</div>
@stop

@section('footer')
<div class="container">
  <div class="content">
    <div class="links" align="center">
   <hr>
      <a href="konverzija">Konverzija valuta</a>
      <a href="index.php">Početna stranica</a>
  
   <br>
   <br>
   <br>
      
    </div>
  </div>
</div>

@stop