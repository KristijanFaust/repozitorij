@extends('layout')

<head>
  
  <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
  
  <script>
  $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
  </script>
  
</head>

@section('header')

  <div class="container">
    <div class="content">
      <div class="title">Konverzija valuta</div>
      <hr>
    </div>
  </div>

@stop

@section('content')

<div class="container">
    <div class="content">

        <div class="form-group">
        <p><b>Odabir valute za pretvorbu: 
  
        <select name="valuta1" id="valuta1">
        <option value='1'> EUR </option> 
  
        @foreach($rates as $key => $value)
  
          <option value={{$value}} class="form-control"> {{$key}} </option>
                     
        @endforeach
  
        </select>
      </div>
      
      <div class="form-group">  
        Željenei iznos za pretvorbu: <input type="number" name="iznos" id="iznos" step="0.01" min=0> </p>
      </div>

  <br>

    
      <div class="form-group">
        <p> Odabir valute za protuvrijednost: 
  
        <select name="valuta2" id="valuta2">
          <option value='1'> EUR </option>
  
          @foreach($rates as $key => $value)
  
            <option value={{$value}} class="form-control"> {{$key}} </option>
                     
          @endforeach
        </select>
        </p>

      </div>
      
      <div class="form-group">  
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <button type="submit" id="pretvori" class="btn btn-primary">Pretvori</b></button>
      </div>
      
      <br>
  <p><b><input type="text" name="rezultat" id="rezultat" value={{$rezultat}} readonly style="text-align:center;/"></b><p>
      
    </div>
</div>
    
@stop

@section('footer')

<div class="container">
    <div class="content">
      <div class="links" align="center">
   <hr>
      <a href="popis">Popis valuta</a>
      <a href="index.php">Početna stranica</a>
  
   <br>
   <br>
   <br>
        
    </div>
  </div>
</div>

@stop

<script>

$(document).ready(function(){

	$('#pretvori').click(function() {
      
		var valuta1 = $('#valuta1').val();
        var valuta2 = $('#valuta2').val();
        var iznos = $('#iznos').val();
      
		$.ajax({
		
			type: 'POST',
			url: 'izracun.php',
			data: {valuta1: valuta1, valuta2: valuta2, iznos: iznos},
			success: function(msg){
				$('#rezultat').val(msg);
			}
		
		});
	
	
	});

});
  
</script>