<?php

require_once('db.php');
require_once('clanak.php');
require_once('clanakmanager.php');

if(!isset($_GET['a'])) { $a = ''; } else {  $a = $_GET['a']; }

switch($a){
    
	case 'pogledaj': Pogledaj(); break;
	case 'objavi': Objavi(); break;
	case 'sakrij': Sakrij(); break;
	default:	Pregled();
}

function Pregled(){
Manager::PrikazSvihClanaka();
}

function Pogledaj(){
$id=$_GET['id'];
Clanak::Prikaz($id);
echo ' <a href=index.php>Pocetna stranica</a>';	
}

function Objavi(){
$id=$_GET['id'];
Clanak::Objavi($id);
echo ' <a href=zadatak2.php>Natrag</a>';	
}

function Sakrij(){
$id=$_GET['id'];
Clanak::Sakrij($id);
echo ' <a href=zadatak2.php>Natrag</a>';	
}

?>
