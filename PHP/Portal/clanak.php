<?php

class Clanak{

	public $id;
	public $naslov;
	public $vk_kategorije;
	public $datum;
	public $objavljen;
	public $uvod;
	public $tekst;
	public $pogledi;
	public $broj_ocjena;
	public $suma_ocjena;
	public $kom_ur;

public function __construct($id, $naslov, $vk_kategorije, $datum, $objavljen, $uvod, $tekst, $pogledi, $broj_ocjena, $suma_ocjena, $kom_ur) {

	$this->id = $id;
	$this->naslov = $naslov;
	$this->vk_kategorije = $vk_kategorije;
	$this->datum = $datum;
	$this->objavljen = $objavljen;
	$this->uvod = $uvod;
	$this->tekst = $tekst;
	$this->pogledi = $pogledi;
	$this->broj_ocjena = $broj_ocjena;
	$this->suma_ocjena = $suma_ocjena;
	$this->kom_ur = $kom_ur;

}

public function Objavi($id){

	$c=DB::conn();
	$query = "UPDATE clanci SET objavljen=1 WHERE id=$id";
	$r = $c->query($query);
	if (!$r) {

   		echo "Neispravan upit";
		exit();
	}
	else {
		echo "Vrijednost uspjesno pomjenjena.";
	}


}

public function Sakrij($id){

	$c=DB::conn();
	$query = "UPDATE clanci SET objavljen=0 WHERE id=$id";
	$r = $c->query($query);
	if (!$r) {

   		echo "Neispravan upit";
		exit();
	}
	else {
		echo "Vrijednost uspjesno pomjenjena.";
	}


}

public function Link_na_Clanak($id) {
    $c=DB::conn();
	$query = "SELECT id, naslov FROM clanci WHERE id=$id";
	$r = $c->query($query);
	if (!$r) {

   		echo "Neispravan upit";
		exit();
	}
	
		$rez = $r->fetch_assoc();
		echo '<a href="?a=pogledaj&id='. $rez['id'] .'">'.$rez['naslov'].'</a><br>';
		echo 'Opcije: ';
		echo '<a href="?a=objavi&id='. $rez['id'] .'">OBJAVI</a>';
		echo "\t | \t";
		echo '<a href="?a=sakrij&id='. $rez['id'] .'">SAKRIJ</a><br>';
		echo '<br>';

}

public static function Prikaz($id) {
	$c=DB::conn();
	$query = "SELECT * FROM clanci WHERE id=$id";
	$r = $c->query($query);
	if (!$r) {

   		echo "Neispravan upit";
		exit();
	}
	else {
		$rez = $r->fetch_assoc();
		echo '<table border=1>';
        echo '<tr>';
		echo '<td>ID: '.$rez['id'].'</td>';
		echo '<td>Naslov: '.$rez['naslov'] .'</td>';
		echo '<td>VK_kategorije: '. $rez['vk_kategorije'].'</td>';
		echo '<td>Datum: '. $rez['datum'].'</td>';
		echo '<td>Objavljen: '. $rez['objavljen'].'</td>';
		echo '<td>Uvod: '.$rez['uvod'].'</td>';
		echo '<td>Pogledi: '.$rez['pogledi'].'</td>';	
		echo '<td>tekst: '.$rez['tekst'] .'</td>';
		echo '<td>Broj ocjena: '. $rez['broj_ocjena'].'</td>';
		echo '<td>Suma ocjena: '. $rez['suma_ocjena'].'</td>';
		echo '<td>Komentari: '. $rez['kom_ur'].'</td>';

	echo '</tr>';
        echo '</table>';
		
	}
}

}

?>
