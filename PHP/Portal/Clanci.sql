-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2016 at 01:16 AM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Clanci`
--

-- --------------------------------------------------------

--
-- Table structure for table `clanci`
--

CREATE TABLE IF NOT EXISTS `clanci` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naslov` varchar(100) NOT NULL,
  `vk_kategorije` int(5) unsigned NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `objavljen` tinyint(1) unsigned NOT NULL,
  `uvod` text NOT NULL,
  `tekst` text NOT NULL,
  `pogledi` int(10) unsigned NOT NULL,
  `broj_ocjena` int(5) unsigned NOT NULL,
  `suma_ocjena` int(5) unsigned NOT NULL,
  `kom_ur` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `clanci`
--

INSERT INTO `clanci` (`id`, `naslov`, `vk_kategorije`, `datum`, `objavljen`, `uvod`, `tekst`, `pogledi`, `broj_ocjena`, `suma_ocjena`, `kom_ur`) VALUES
(1, 'OOP u PHP', 1, '2014-04-10 07:11:27', 1, 'Objektno programiranje u PHP-u', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium quam quis elit molestie, at sollicitudin arcu pretium. Pellentesque vel sem eu libero ullamcorper vestibulum quis a metus. Vestibulum dignissim magna ac mi sagittis blandit. Suspendisse mattis lectus leo, hendrerit imperdiet leo tristique eget. Nulla laoreet sem ac facilisis facilisis. Duis in massa vitae sem auctor suscipit pharetra id nisi. Ut at nulla non justo feugiat ornare. Integer non nisl accumsan, rhoncus nulla id, aliquet dui. Nullam interdum accumsan ligula, vel lobortis nisl sodales eu. Donec aliquet ullamcorper bibendum. Maecenas scelerisque urna sed condimentum blandit. Quisque arcu tellus, commodo quis dignissim eget, sollicitudin eget enim. Mauris pellentesque mi et tempus auctor.\r\n\r\nQuisque pharetra purus ut dui gravida volutpat. Donec luctus aliquam molestie. Etiam ac nunc et ligula faucibus tincidunt eu vel augue. Maecenas ac massa neque. Quisque sapien augue, blandit id aliquet vel, malesuada et nisl. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi id dictum ipsum. Donec nec massa nisl. Nullam a velit lobortis purus pulvinar tincidunt. Donec at massa sodales, malesuada arcu ut, lobortis turpis. Cras purus felis, cursus at nunc nec, gravida venenatis magna. Morbi vitae elementum mauris, id egestas mauris. Pellentesque tempus sem et diam adipiscing consequat. ', 220, 1, 5, NULL),
(2, 'Uvod u HTML5', 2, '2014-04-22 12:34:53', 1, 'Blah blah', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium quam quis elit molestie, at sollicitudin arcu pretium. Pellentesque vel sem eu libero ullamcorper vestibulum quis a metus. Vestibulum dignissim magna ac mi sagittis blandit. Suspendisse mattis lectus leo, hendrerit imperdiet leo tristique eget. Nulla laoreet sem ac facilisis facilisis. Duis in massa vitae sem auctor suscipit pharetra id nisi. Ut at nulla non justo feugiat ornare. Integer non nisl accumsan, rhoncus nulla id, aliquet dui. Nullam interdum accumsan ligula, vel lobortis nisl sodales eu. Donec aliquet ullamcorper bibendum. Maecenas scelerisque urna sed condimentum blandit. Quisque arcu tellus, commodo quis dignissim eget, sollicitudin eget enim. Mauris pellentesque mi et tempus auctor.\r\n\r\nQuisque pharetra purus ut dui gravida volutpat. Donec luctus aliquam molestie. Etiam ac nunc et ligula faucibus tincidunt eu vel augue. Maecenas ac massa neque. Quisque sapien augue, blandit id aliquet vel, malesuada et nisl. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi id dictum ipsum. Donec nec massa nisl. Nullam a velit lobortis purus pulvinar tincidunt. Donec at massa sodales, malesuada arcu ut, lobortis turpis. Cras purus felis, cursus at nunc nec, gravida venenatis magna. Morbi vitae elementum mauris, id egestas mauris. Pellentesque tempus sem et diam adipiscing consequat. ', 3, 0, 0, 'NEKI KOMENATARreterterterterterter'),
(5, 'Probni clanak', 2, '2015-04-13 15:02:25', 1, 'Ovo je uvodni dio teksta!', 'Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! Ovo je glavni dio teksta!!! ', 0, 0, 0, 'OVO SU MOJI KOMENTARI'),
(9, 'Random generic', 1, '2016-06-12 22:12:47', 0, 'hjgjkhkjh', 'klhlkjljl', 0, 0, 0, NULL),
(10, 'Random generic', 1, '2016-06-12 22:13:22', 0, 'hjgjkhkjh', 'klhlkjljl', 0, 0, 0, NULL),

-- --------------------------------------------------------

--
-- Table structure for table `kategorije`
--

CREATE TABLE IF NOT EXISTS `kategorije` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kategorije`
--

INSERT INTO `kategorije` (`id`, `naziv`) VALUES
(1, 'PHP'),
(2, 'HTML5'),
(4, 'CSS3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
