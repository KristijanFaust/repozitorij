-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2016 at 08:42 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tecaji`
--

-- --------------------------------------------------------

--
-- Table structure for table `tablica`
--

CREATE TABLE `tablica` (
  `sifra` int(3) NOT NULL,
  `kratica` char(3) NOT NULL,
  `jedinica` set('100','001') NOT NULL,
  `kupovni` double NOT NULL,
  `srednji` double NOT NULL,
  `prodajni` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tablica`
--

INSERT INTO `tablica` (`sifra`, `kratica`, `jedinica`, `kupovni`, `srednji`, `prodajni`) VALUES
(1, 'AUD', '001', 5.008494, 5.023565, 5.038636),
(2, 'JPY', '100', 5.365941, 5.382087, 5.398233),
(3, 'GBP', '001', 9.120941, 9.148386, 9.175831),
(4, 'USD', '001', 5.558198, 5.574923, 5.591648),
(5, 'EUR', '001', 7.601392, 7.624265, 7.647138),
(6, 'HUF', '100', 2.535572, 2.543202, 2.550832);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tablica`
--
ALTER TABLE `tablica`
  ADD PRIMARY KEY (`sifra`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tablica`
--
ALTER TABLE `tablica`
  MODIFY `sifra` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
