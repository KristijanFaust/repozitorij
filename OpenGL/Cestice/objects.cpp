float scale;
int border;
int initialSpin;


class Obj
{
public:
  float position_x, position_y;
  float new_position_x, new_position_y;
  float prevPosition_x, prevPosition_y;
  float radius;
  float mass;

  Obj(float position_x, float position_y, float prevPosition_x, float prevPosition_y, float radius);
  Obj(int n, int d);

  void calcMass();
  int calcGravityPosition(Obj &o);
  void calcInertPosition();
  void merge(Obj &o, bool skipped);
  bool outOfBounds();
};



Obj::Obj(float position_x, float position_y, float prevPosition_x, float prevPosition_y, float radius)
{
  this->position_x = position_x;
  this->position_y = position_y;
  this->radius = radius;
  this->prevPosition_x = prevPosition_x;
  this->prevPosition_y = prevPosition_y;
}

Obj::Obj(int n, int d)
{
  position_x = (rand() % 1000 - 500) / 10. * d;
  position_y = (rand() % 1000 - 500) / 10. * d;

  radius = (rand() % n) / 10. + 0.2;

  float ratioXY = position_x / position_y;
  float ratioYX = position_y / position_x;
  if (ratioXY < 0) ratioXY = - ratioXY;
  if (ratioYX < 0) ratioYX = - ratioYX;

  if (position_x > 0)
    prevPosition_y = position_y + initialSpin * ratioXY;
  else
    prevPosition_y = position_y - initialSpin * ratioXY;
  if (position_y > 0)
    prevPosition_x = position_x - initialSpin * ratioYX;
  else
    prevPosition_x = position_x + initialSpin * ratioYX;
}


void Obj::calcMass()
{
  mass = 4/3 * 3.14 * pow(radius,3) * 5;
}


int Obj::calcGravityPosition(Obj &o)
{
  float xDistance = position_x - o.position_x;
  float yDistance = position_y - o.position_y;

  float relXDistance = xDistance;
  float relYDistance = yDistance;

  if (xDistance < 0) xDistance = -xDistance;
  if (yDistance < 0) yDistance = -yDistance;

  float distance = sqrt(pow(xDistance,2) + pow(yDistance,2));
  float forceGravity = (mass * o.mass) / pow(distance,2);

  bool skipped = false;
  float skip_relx1 = position_x - o.prevPosition_x;
  float skip_relx2 = position_x - o.position_x;
  float skip_rely1 = position_y - o.prevPosition_y;
  float skip_rely2 = position_y - o.position_y;
  if (skip_relx1 * skip_relx2 <= 0 && skip_rely1 * skip_rely2 <= 0)
    skipped = true;

  float xy_Distance = xDistance + yDistance;
  if (distance > radius + o.radius && !skipped)
    {
      float xForce = relXDistance / xy_Distance * forceGravity;
      float yForce = relYDistance / xy_Distance * forceGravity;

      new_position_x -= xForce / mass;
      new_position_y -= yForce / mass;
    }
  else
    {
      if (radius >= o.radius)
	{
	  merge(o, skipped);
	  return 2;    // remove "o" object
	}
      else
	return 1;    // remove this object
    }
  return 0;    // don't remove any object
}


void Obj::calcInertPosition()
{
  new_position_x += position_x - prevPosition_x;
  new_position_y += position_y - prevPosition_y;

  if (border)
    {
      if (new_position_x > 50*scale || new_position_x < -50*scale)
	new_position_x = position_x;
      if (new_position_y > 50*scale || new_position_y < -50*scale)
	new_position_y = position_y;
    }

  prevPosition_x = position_x;
  prevPosition_y = position_y;
}


void Obj::merge(Obj &o, bool skipped)
{
  radius = pow((pow(radius,3) + pow(o.radius,3)), 1/3.);

  float diffX = position_x - o.position_x;
  float diffY = position_y - o.position_y;
  o.prevPosition_x += diffX;
  o.prevPosition_y += diffY;

  float massRatio = o.mass / mass;
  prevPosition_x = (prevPosition_x - (prevPosition_x - o.prevPosition_x) * massRatio);
  prevPosition_y = (prevPosition_y - (prevPosition_y - o.prevPosition_y) * massRatio);

  calcMass();
}


bool Obj::outOfBounds()
{
  float x, y;

  if (new_position_x < 0) x = - new_position_x;
  else x = new_position_x;
  if (new_position_y < 0) y = - new_position_y;
  else y = new_position_y;

  if (x > 50 * scale * 4 || y > 50 * scale * 4) return true;
  else return false;
}
