//g++ main.cpp -w -lGL -lGLU -lglut -o sol

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

#include "objects.cpp"
#include "draw.cpp"

using namespace std;


//Screen Constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 640;
const int SCREEN_FPS = 30;



bool initGL()
{
  //Initialize Projection Matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  //Initialize Modelview Matrix
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  //Initialize clear color
  glClearColor(0.f, 0.f, 0.f, 1.f);

  //Set scale
  gluOrtho2D(-50, 50, -50, 50);

  //Check for error
  GLenum error = glGetError();
  if (error != GL_NO_ERROR)
    {
      cout << "Error initializing OpenGL!\n" << gluErrorString( error );
      return false;
    }

  return true;
}



vector<Obj> obj;
int stationarySun;
extern float scale;
extern int border;
extern int initialSpin;

void update()
{
  int objSize = obj.size();
  for (int i=0; i<objSize; i++)
    {
      for (int j=0; j<objSize; j++)
	{
	  if (i != j)
	    if (obj[i].calcGravityPosition(obj[j]))
	      {
		obj.erase(obj.begin() + j);
		objSize--;
	      }
	}
      obj[i].calcInertPosition();
      if (obj[i].outOfBounds()) obj.erase(obj.begin() + i);
    }

  for (int i=stationarySun; i<obj.size(); i++)
    {
      obj[i].position_x = obj[i].new_position_x;
      obj[i].position_y = obj[i].new_position_y;
    }
}




void render()
{
  //Clear buffer
  glClear(GL_COLOR_BUFFER_BIT);

  //Render
  for (int i=0; i<obj.size(); i++)
    {
      glBegin(GL_POLYGON);
      {
	drawCircle(obj[i].position_x / scale, obj[i].position_y / scale, obj[i].radius / scale);
      }
      glEnd();
    }

  //Update screen
  glutSwapBuffers();
}



void mainLoop(int val)
{
  update();
  render();

  glutTimerFunc(1000 / SCREEN_FPS, mainLoop, val);
}



void config()
{
  ifstream configFile;
  configFile.open("config.txt");

  int numberOfObjects;
  int avgSizeOfObjects;
  float sizeOfSun;
  int distanceFromSun;

  string input;
  do{ configFile >> input; }while(input != ":");
  configFile >> scale;
  do{ configFile >> input; }while(input != ":");
  configFile >> border;
  do{ configFile >> input; }while(input != ":");
  configFile >> sizeOfSun;
  do{ configFile >> input; }while(input != ":");
  configFile >> stationarySun;
  do{ configFile >> input; }while(input != ":");
  configFile >> numberOfObjects;
  do{ configFile >> input; }while(input != ":");
  configFile >> avgSizeOfObjects;
  do{ configFile >> input; }while(input != ":");
  configFile >> initialSpin;
  do{ configFile >> input; }while(input != ":");
  configFile >> distanceFromSun;

  obj.push_back(Obj(0, 0, 0, 0, sizeOfSun));    // Sun
  obj[0].calcMass();

  srand(time(NULL));
  for (int i=1; i<=numberOfObjects; i++)
    {
      obj.push_back(Obj(avgSizeOfObjects, distanceFromSun));    // planets
      obj[i].calcMass();
    }

  for (int i=0; i<obj.size(); i++)
    {
      obj[i].new_position_x = obj[i].position_x;
      obj[i].new_position_y = obj[i].position_y;
    }

  configFile.close();
}






int main(int argc, char* args[])
{
  config();

  //Initialize FreeGLUT
  glutInit(&argc, args);

  //Create OpenGL 2.1 context
  glutInitContextVersion(2, 1);

  //Create Double Buffered Window
  glutInitDisplayMode(GLUT_DOUBLE);
  glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
  glutCreateWindow("Sol");

  //Do post window/context creation initialization
  if (!initGL())
    {
      cout << "Unable to initialize graphics library!\n";
      return 1;
    }

  //Set rendering function
  glutDisplayFunc(render);

  //Set main loop
  glutTimerFunc(1000 / SCREEN_FPS, mainLoop, 0);

  //Start GLUT main loop
  glutMainLoop();

  return 0;
}
