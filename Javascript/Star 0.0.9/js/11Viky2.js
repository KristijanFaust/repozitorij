var counterflat3=0; //Brojac interakcija
var countersiluette=0; //Brojac interakcija sa siluetom

//Varijable za omogucavanje inputa sa tipkovnice/////////////////////////////////

var input_mode_txt = false;
var input_mode_object = false;
var input_mode_viky = false;
var input_mode_txt_siluette = false;

////////////////////////////////////////////////////////////////////////////////

var spaceKey; //Varijabla za SPACEBAR
var spacecount=0; //Brojac udarca na SPACEBAR za tonove
 
game.viky2 = function (the_game) {};

game.viky2.prototype = {
    
      
    create: function () {
        
    spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); //Dodjeljivanje signala za SPACEBAR
        
//Dodavanje zvukova///////////////////////////////////////////////////////////
        
    hover_sound = this.add.audio("hoover");
    click_sound = this.add.audio("click");
    doors = this.add.audio("doors");
    trafic = this.add.audio("trafic_night");
    steps = this.add.audio("steps");
    theme = this.add.audio("basstheme");
    D = this.add.audio("D");
    Dm = this.add.audio("Dm");
    C = this.add.audio("C");
    Dsh = this.add.audio("Dsh"); 
        
//////////////////////////////////////////////////////////////////////////////
        
        
    
//Dodavanje pocetnih slika////////////////////////////////////////////////////
   
    flat3 = this.add.sprite(0, 0, 'flat3');
        flat3.alpha=0;
    money = this.add.sprite(175, 680, 'novac');
        money.alpha=0;
                    
    selviky=this.add.sprite(400,530,'selviky2');
        selviky.alpha=0;
        selviky.inputEnabled=true;
        selviky.events.onInputOver.add(hoverSve2, this);
        selviky.events.onInputOut.add(hoverOutSve2, this);
        selviky.events.onInputDown.add(clickviky2, this);
                
    seldoor=this.add.sprite(890,236,'seldoor2');
        seldoor.alpha=0;
        seldoor.inputEnabled=true;
        seldoor.events.onInputOver.add(hoverSve2, this);
        seldoor.events.onInputOut.add(hoverOutSve2, this);
        seldoor.events.onInputDown.add(clickdoor2, this);
        
    selmoney=this.add.sprite(162,660,'selmoney');
        selmoney.alpha=0;
        selmoney.inputEnabled=true;
        selmoney.events.onInputOver.add(hoverSve2, this);
        selmoney.events.onInputOut.add(hoverOutSve2, this);
        selmoney.events.onInputDown.add(clickmoney, this);
        
                            
//////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
//Pokretanje/////////////////////////////////////////////////////////////////////////////////
       
    this.add.tween(flat3).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
    this.add.tween(money).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
        
    this.time.events.add(5000, function() {
        
        txt = this.add.sprite(490, 50, 'flt3txt1');
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
            click.inputEnabled=true;
            input_mode_txt=true;
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(clicktxtnext2, this);
                    
    }, this)
    
/////////////////////////////////////////////////////////////////////////////////////////////
        
    },
    
    update: function () {
    
//Provjere za input SPACEBAR tipke za svaki frame u igri/////////////////////////////////////
        
    spaceKey.onDown.add(noclickflat3, this);
    spaceKey.onDown.add(clicktxtnext2, this);
    spaceKey.onDown.add(clicktxtviky2, this);
    spaceKey.onDown.add(clicktxtsiluette, this);
                
/////////////////////////////////////////////////////////////////////////////////////////////        
    },
    
    
};




//Generic funkcije//////////////////////////////////////////////////////////////////////

//Funkcija za hover iznad odabira:
function hoverSve2(event, sprite){
    
    if(input_mode_object==true){
        hover_sound.play();
        this.add.tween(event).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true); 

        }
}

//Funkcija za izlazak iz hovera:
function hoverOutSve2(event, sprite){      
    
    if(input_mode_object==true){
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true); 
        } 
}

//Funkcija za hover strelice:
function hovernext(event, sprite){
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
}

//Funkcija za izlaz iz hoovera strelice:
function hoverOutnext (event, sprite) {
        event.scale.setTo(1.0, 1.0);
}

//Funkcija za SPACEBAR kada nema input na klik:
function noclickflat3 (event, sprite) {
    
    if((input_mode_txt==false) && (input_mode_txt_siluette==false)) {
            
    if(spacecount%4==0)
        D.play();
    else if(spacecount%4==1)
        Dm.play();
    else if(spacecount%4==2)
        C.play();
    else if(spacecount%4==3)
        Dsh.play();
    
    spacecount++;
        
    }
}

//Funkcija za text:

function clicktxtnext2(event, sprite){
        if ((input_mode_txt==true) && (input_mode_viky==false)){
            
            click_sound.play();
            
            this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
            click.inputEnabled=false;
            input_mode_txt=false;
            input_mode_object = true;
            if(counterflat3==0)
                selmoney.inputEnabled=true;
            selviky.inputEnabled=true;
            seldoor.inputEnabled=true;
        
        }
    
}

//Funkcija za viky:

function clickviky2(event, sprite){
    
    if(input_mode_object==true){
        
        if(counterflat3==1){
            
        click_sound.play();
        selmoney.inputEnabled=false;
        selviky.inputEnabled=false;
        seldoor.inputEnabled=false;
            
         txtlayer1 = this.add.sprite(503, 373, 'f3l1');
            txtlayer1.alpha=0;
            
        txtlayer2 = this.add.sprite(3, 850, 'f3l2');
            txtlayer2.alpha=0;
            
        this.add.tween(txtlayer1).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(txtlayer2).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
        txt = this.add.sprite(563, 424, 'vtxt1');
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
        click.inputEnabled=true;
        input_mode_txt=true;
        input_mode_viky=true;
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        click.events.onInputDown.add(clicktxtviky2, this);
            
        }
        
        else {
            
            click_sound.play();
            selmoney.inputEnabled=false;
            selviky.inputEnabled=false;
            seldoor.inputEnabled=false;
            
            txt = this.add.sprite(610, 50, 'flt3txtsleep');
            txt.alpha = 0;
            click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
            click.alpha=0;
            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
            click.inputEnabled=true;
            input_mode_txt=true;
            input_mode_viky=false;
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(clicktxtnext2, this);
            
                    
        }
    }
    
}

function clickdoor2(event, sprite){
    
if(input_mode_object==true){
    
    if(counterflat3<1){
            click_sound.play();
            selmoney.inputEnabled=false;
            selviky.inputEnabled=false;
            seldoor.inputEnabled=false;
            
            txt = this.add.sprite(360, 50, 'flt3txtdoor');
            txt.alpha = 0;
            click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
            click.alpha=0;
            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
            click.inputEnabled=true;
            input_mode_txt=true;
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(clicktxtnext2, this);
    }
    
    else {
        
    selviky.inputEnabled=false;
    seldoor.inputEnabled=false;      
    doors.play();
    this.add.tween(flat3).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
    selviky.destroy();
    seldoor.destroy();
        
     this.time.events.add(4000, function() {
            
        trafic.fadeIn(4000,true);
        cityn=this.add.sprite(0, 0,'citynight');
        cityn.alpha=0;
        this.add.tween(cityn).to( { alpha: 1 }, 6000, Phaser.Easing.Linear.None, true );
        steps.fadeIn();
        theme.fadeIn(6000,true);
         
          this.time.events.add(10000, function() {
              
              this.add.tween(cityn).to( { alpha: 0 }, 2000, Phaser.Easing.Linear.None, true );
              steps.fadeIn();
              
              this.time.events.add(4000, function() {
              
                alley=this.add.sprite(0, 0,'alley');
                alley.alpha=0;
                this.add.tween(alley).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true );
                  
                this.time.events.add(4000, function() {
                  
                    lik=this.add.sprite(925, 335,'alleylik');
                    lik.alpha=0;
                    this.add.tween(lik).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
                    
                    this.time.events.add(2000, function() {
                  
                    l1=this.add.sprite(405, 245,'atxtl1');
                    l1.alpha=0;
                    this.add.tween(l1).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
                        
                    l2=this.add.sprite(2, 640,'atxtl2');
                    l2.alpha=0;
                    this.add.tween(l1).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
                        
                        this.time.events.add(1000, function() {
                  
                    
                            txt = this.add.sprite(77, 916, 'amtxt1');
                            txt.alpha = 0;
                            click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
                            click.alpha=0;
        
                            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
                            click.inputEnabled=true;
                            input_mode_txt_siluette=true;
                            click.events.onInputOver.add(hovernext, this);
                            click.events.onInputOut.add(hoverOutnext, this);
                            click.events.onInputDown.add(clicktxtsiluette, this);
                        
                    
                    }, this)
                    
                }, this)
                        
                        
                    
             }, this)
                    
         }, this)
                  
              
    }, this)
              
}, this)
        
            
    }
    
  }
}


//Funkcija za klik na novac
function clickmoney(event, sprite){
    
    if(input_mode_object==true){
    
            click_sound.play();
            this.add.tween(money).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
            selmoney.inputEnabled=false;
            selviky.inputEnabled=false;
            seldoor.inputEnabled=false;
    
            selmoney.destroy();
            
            txt = this.add.sprite(400, 50, 'flt3txtmoney');
            txt.alpha = 0;
            click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
            click.alpha=0;
            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
            counterflat3++;
        
            click.inputEnabled=true;
            input_mode_txt=true;
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(clicktxtnext2, this);
        
    }
    
}

//Funkcija  za dijalog s Viky
function clicktxtviky2(event, sprite){
    
    if((input_mode_txt==true) && (input_mode_viky==true)){
        
        if(counterflat3==8){
            
            counterflat3++;
            click_sound.play();
            
            this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(txtlayer1).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            this.add.tween(txtlayer2).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            
            click.inputEnabled=false;
            input_mode_txt=false;
            input_mode_viky==false;
            selviky.inputEnabled=true;
            seldoor.inputEnabled=true;
                
        }
        
        else {
            
        selviky.inputEnabled=false;
        selviky.inputEnabled=false;
            
        counterflat3++;    
        click_sound.play();
                                
        this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
        if(counterflat3==2)
            txt = this.add.sprite(77, 916, 'mtxt1');
        
        else if(counterflat3==3)
            txt = this.add.sprite(563, 424, 'vtxt2');
        
        else if(counterflat3==4)
            txt = this.add.sprite(77, 916, 'mtxt2');
        
        else if(counterflat3==5)
            txt = this.add.sprite(563, 404, 'vtxt3');
        
        else if(counterflat3==6)
            txt = this.add.sprite(77, 916, 'mtxt3');
        
        else if(counterflat3==7)
            txt = this.add.sprite(563, 424, 'vtxt4');
        
        else if(counterflat3==8)
            txt = this.add.sprite(77, 916, 'mtxt4');
        
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
        click.inputEnabled=true;
        input_mode_txt=true;
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        click.events.onInputDown.add(clicktxtviky2, this);
            
        }        
    }
}

//Funkcija  za dijalog sa siluetom:
function clicktxtsiluette(event, sprite){
    
    if(input_mode_txt_siluette==true){
        
        click.inputEnabled=false;
        input_mode_txt_siluette=false;
        
        countersiluette++;
        click_sound.play();
        
        this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
        if(countersiluette==1)
            txt = this.add.sprite(350, 280, 'astxt1');
            
        else if(countersiluette==2)
             txt = this.add.sprite(77, 916, 'amtxt2');
            
        else if(countersiluette==3)
            txt = this.add.sprite(350, 280, 'astxt2');
            
        else if(countersiluette==4)
             txt = this.add.sprite(77, 916, 'amtxt3');
            
        else if(countersiluette==5)
            txt = this.add.sprite(350, 280, 'astxt3');
            
        else if(countersiluette==6)
             txt = this.add.sprite(77, 916, 'amtxt4');
            
        else if(countersiluette==7)
            txt = this.add.sprite(350, 280, 'astxt4');
            
        else if(countersiluette==8)
             txt = this.add.sprite(77, 916, 'amtxt5');
            
        else if(countersiluette==9)
            txt = this.add.sprite(350, 280, 'astxt5');
            
        else if(countersiluette==10)
             txt = this.add.sprite(77, 916, 'amtxt6');
            
        else if(countersiluette==11)
            txt = this.add.sprite(350, 280, 'astxt6');
            
        else if(countersiluette==12)
             txt = this.add.sprite(77, 916, 'amtxt7');
        
        else if(countersiluette==13)
            txt = this.add.sprite(350, 280, 'astxt7');
        
        if (countersiluette==14){
            
            this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(l1).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(l2).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            
            this.add.tween(alley).to({alpha: 0}, 2000, Phaser.Easing.Linear.None, true);
            this.add.tween(lik).to({alpha: 0}, 2000, Phaser.Easing.Linear.None, true);
            
            trafic.fadeOut(4000);
            
            this.time.events.add(2000, function() {
                  
                            click.destroy();
                
                            txt = this.add.sprite(this.world.centerX, this.world.centerY, 'aout1');
                            txt.anchor.setTo(0.5, 0.5);
                            txt.alpha = 0;
                                    
                            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
                            this.time.events.add(5000, function() {
                            
                                this.add.tween(txt).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
                    
                                                    
                                    this.time.events.add(1000, function() {
                                    
                                        txt = this.add.sprite(this.world.centerX, this.world.centerY, 'aout2');
                                        txt.anchor.setTo(0.5, 0.5);
                                        txt.alpha = 0;
                            
                                        this.add.tween(txt).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
                                        
                                        this.time.events.add(5000, function() {
                            
                                            this.add.tween(txt).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
                    
                                                    
                                            this.time.events.add(1000, function() {
                                    
                                                txt = this.add.sprite(this.world.centerX, this.world.centerY, 'aout3');
                                                txt.anchor.setTo(0.5, 0.5);
                                                txt.alpha = 0;
                                                theme.fadeOut(5000);
                            
                                                this.add.tween(txt).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
                    
                                    
                                                this.time.events.add(5000, function() {
                            
                                                    this.add.tween(txt).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
                                                    
                                                    this.time.events.add(2000, function() {
                            
                                                    
                                                        counterflat3=0; 
                                                        countersiluette=0;

                                                        input_mode_txt = false;
                                                        input_mode_object = false;
                                                        input_mode_viky = false;
                                                        input_mode_txt_siluette = false;
                                                        
                                                        this.state.start('MainMenu');
                                                    
                                                    
                                                    }, this)
                                                    
                                                }, this)
                                                
                                            }, this)  
                    
                                        }, this)  
                    
                                    }, this)
                    
                                }, this)  
                            
                             }, this)  
            
        }
            
        else {
            
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
        click.inputEnabled=true;
        input_mode_txt_siluette=true;
            
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        click.events.onInputDown.add(clicktxtsiluette, this);
            
        }

            
        }
            
            
}
            
        
            
        
            
        

