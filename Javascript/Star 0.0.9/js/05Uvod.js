var counter=0; //Brojac za tekst
var input_mode = false; //Varijabla za omogucavanje inputa sa tipkovnice
var spaceKey; //Varijabla za SPACEBAR
var spacecount=0;
 
game.intro = function (the_game) {};

game.intro.prototype = {
    
      
    create: function () {
    
    spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); //Dodjeljivanje signala za SPACEBAR
        
    //Dodavanje zvukova:
    hover_sound = this.add.audio("hoover");
    click_sound = this.add.audio("click");        
    trafic = this.add.audio("trafic");
    rain = this.add.audio("rain");
    D = this.add.audio("D");
    Dm = this.add.audio("Dm");
    C = this.add.audio("C");
    Dsh = this.add.audio("Dsh");
    
//Dodavanje pocetnih slika///////////////////////////////////////////////////////////////////////////////
   
    introtxt = this.add.sprite(this.world.centerX, this.world.centerY, 'introtext');
        introtxt.anchor.setTo(0.5, 0.5);
        introtxt.alpha=0;
    
    intro1 = this.add.sprite(0, 0, 'city1');
        intro1.alpha=0;
        
    introtxt1 = this.add.sprite(100, 50, 'introtxt1');
        introtxt1.alpha=0;
    introtxt2 = this.add.sprite(100, 75, 'introtxt2');
        introtxt2.alpha=0;
    introtxt3 = this.add.sprite(100, 125, 'introtxt3');
        introtxt3.alpha=0;
    
    click = this.add.sprite(introtxt1.x + introtxt1.width + 20, introtxt1.y + introtxt1.height - 30, 'click');
        click.alpha=0;
        
//////////////////////////////////////////////////////////////////////////////////////////////////////  
        
        
//Pokretanje//////////////////////////////////////////////////////////////////////////////////////////
        
    this.add.tween(introtxt).to( { alpha: 1 }, 4000, Phaser.Easing.Linear.None, true );
        
    this.time.events.add(7000, function() {
        trafic.loop = true;
        trafic.fadeIn();
        this.add.tween(introtxt).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
    }, 
    this)
    
    this.time.events.add(11000, function() {
        this.add.tween(intro1).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true );
    }, 
    this)
    
    this.time.events.add(13000, function() {
        this.add.tween(introtxt1).to( { alpha: 1 }, 4000, Phaser.Easing.Linear.None, true );
        this.add.tween(click).to( { alpha: 1 }, 5000, Phaser.Easing.Linear.None, true );
        
        this.time.events.add(500, function() {
        click.inputEnabled = true;
        input_mode = true;
        click.events.onInputDown.add(next, this);
        click.events.onInputOver.add(hover, this);
        click.events.onInputOut.add(hoverOut, this);
         }, this)       
    }, 
    this)
    
    intro2 = this.add.sprite(0, 0, 'city2');
        intro2.alpha=0;
    introtxt4 = this.add.sprite(365, 200, 'introtxt4');
        introtxt4.alpha=0;
    introtxt5 = this.add.sprite(245, 300, 'introtxt5');
        introtxt5.alpha=0;
    introtxt6 = this.add.sprite(370, 220, 'introtxt6');
        introtxt6.alpha=0;
    introtxt7 = this.add.sprite(305, 290, 'introtxt7');
        introtxt7.alpha=0;
    introtxt8 = this.add.sprite(280, 310, 'introtxt8');
        introtxt8.alpha=0;
    introtxt9 = this.add.sprite(420, 190, 'introtxt9');
        introtxt9.alpha=0;
    introtxt10 = this.add.sprite(440, 320, 'introtxt10');
        introtxt10.alpha=0;
    
    iout1 = this.add.sprite(this.world.centerX-150, this.world.centerY, 'iout1');
        iout1.anchor.setTo(0.5, 0.5);
        iout1.alpha=0;
    iout2 = this.add.sprite(this.world.centerX+150, this.world.centerY + 2, 'iout2');
        iout2.anchor.setTo(0.5, 0.5);
        iout2.alpha=0;
    
    //Potrebno kako bi se gumb za klik vidio ispred intro2 pozadine:   
    this.world.bringToTop(click);

////////////////////////////////////////////////////////////////////////////////////////////////////////  
                
    //Counter debug (Koristite pri provjeri vrijednosti željenih varijabli zajedno sa text.text atributom na odgovarajućem mjestu):
    /*
     text = this.add.text(this.world.centerX, this.world.centerY, "- You have clicked -\n0 times !", {
        font: "65px Arial",
        fill: "#ff0044",
        align: "center"
    });
    text.anchor.setTo(0.5, 0.5);
    */ 
    
    //Counter debug dio:
    //text.text = "- You have clicked -\n" + counter + " times !";
        
    
    },
    
    update: function () {
        
    spaceKey.onDown.add(next, this); //Provjera za input SPACEBAR tipke za svaki frame u igri
        
    },
    
};

//Funkcija za hover:
function hover(event, sprite){
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
}

//Funkcija za izlaz iz hoovera:
function hoverOut (event, sprite) {
        event.scale.setTo(1.0, 1.0);
}

//Funkcija za klik:
function next () {
    
    if (input_mode==true){
    //Univerzalno ponašanje:
    input_mode = false;
    click.inputEnabled=false;
    click_sound.play();
    counter++;
    click.scale.setTo(1.0, 1.0);
    
    //Ponašanja ovisna o tekstu na ekranu (variabla counter):
    if (counter==1){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt1).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                
        this.time.events.add(1000, function() {
            introtxt1.destroy();
            click.x=introtxt2.x + introtxt2.width + 20;
            click.y=introtxt2.y + introtxt2.height - 30;
            this.add.tween(introtxt2).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(500, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)    
        }, 
        this)
    }
    
    else if (counter==2){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt2).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                
        this.time.events.add(1000, function() {
            introtxt2.destroy();
            click.x=introtxt3.x + introtxt3.width + 20;
            click.y=introtxt3.y + introtxt3.height - 30;
            this.add.tween(introtxt3).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(1000, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else if (counter==3){
        rain.loop = true;
        rain.fadeIn();
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt3).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(intro1).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
        //Za slučaj da igrač jako brzo klikče next:
        this.time.events.add(500, function() {
            introtxt3.destroy();
        }, 
        this)
                
        this.time.events.add(3000, function() {
            click.x=introtxt4.x + introtxt4.width + 20;
            click.y=introtxt4.y + introtxt4.height - 30;
            this.add.tween(intro2).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.add.tween(introtxt4).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(500, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else if (counter==4){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt4).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                        
        this.time.events.add(1000, function() {
            introtxt4.destroy();
            click.x=introtxt5.x + introtxt5.width + 20;
            click.y=introtxt5.y + introtxt5.height - 30;
            this.add.tween(introtxt5).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(500, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else if (counter==5){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt5).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                        
        this.time.events.add(1000, function() {
            introtxt5.destroy();
            click.x=introtxt6.x + introtxt6.width - 30;
            click.y=introtxt6.y + introtxt6.height - 30;
            this.add.tween(introtxt6).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(500, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else if (counter==6){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt6).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                        
        this.time.events.add(1000, function() {
            introtxt6.destroy();
            click.x=introtxt7.x + introtxt7.width + 20;
            click.y=introtxt7.y + introtxt7.height - 30;
            this.add.tween(introtxt7).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(500, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else if (counter==7){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt7).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                        
        this.time.events.add(1000, function() {
            introtxt7.destroy();
            click.x=introtxt8.x + introtxt8.width + 20;
            click.y=introtxt8.y + introtxt8.height - 30;
            this.add.tween(introtxt8).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(500, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else if (counter==8){
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt8).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        
        this.time.events.add(1000, function() {
            introtxt8.destroy();
            click.x=introtxt10.x + introtxt10.width + 20;
            click.y=introtxt10.y + introtxt10.height - 30;
            this.add.tween(introtxt9).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
        }, 
        this)
                        
        this.time.events.add(4000, function() {
            this.add.tween(introtxt10).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(click).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
            this.time.events.add(2000, function() {
                click.inputEnabled = true;
                input_mode = true;
            }, this)  
        }, 
        this)
    }
    
    else {
        click.inputEnabled = false;
        input_mode = false;
        trafic.fadeOut();
        this.add.tween(click).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt9).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(introtxt10).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
        this.add.tween(intro2).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
        
        this.time.events.add(4000, function() {
            introtxt9.destroy();
            introtxt10.destroy();
            this.add.tween(iout1).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            }, 
        this)
        
        this.time.events.add(6000, function() {
            this.add.tween(iout2).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            }, 
        this)
        
        this.time.events.add(12000, function() {
            rain.fadeOut();
            this.add.tween(iout1).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(iout2).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
            }, 
        this)
        
        this.time.events.add(18000, function() {
            rain.fadeOut();
            this.add.tween(iout1).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
            this.add.tween(iout2).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
            
            counter=0; 
            input_mode = false;
            
            this.state.start("Stan");
        
        }, this)
        
        
    }
}
else{
    
    if(spacecount%4==0)
        D.play();
    else if(spacecount%4==1)
        Dm.play();
    else if(spacecount%4==2)
        C.play();
    else if(spacecount%4==3)
        Dsh.play();
    
    spacecount++;
    
}
    
}
    