var counterflat=0; //Brojac interakcija do poziva
var phoneTalk=0; //Brojac za dijaloge tijekom poziva

//Varijable za omogucavanje inputa sa tipkovnice/////////////////////////////////

var input_mode_me = false;
var input_mode_tv = false;
var input_mode_fridge = false;
var input_mode_cig = false;
var input_mode_door = false;
var input_mode_phone = false;
var input_mode_pic = false;
var input_mode_talk = false;

////////////////////////////////////////////////////////////////////////////////

var spaceKey; //Varijabla za SPACEBAR
var spacecount=0;//Brojac udarca na SPACEBAR za tonove
 
game.stan = function (the_game) {};

game.stan.prototype = {
    
      
    create: function () {
        
    spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); //Dodjeljivanje signala za SPACEBAR
        
//Dodavanje zvukova://////////////////////////////////////////////////////////
        
    hover_sound = this.add.audio("hoover");
    click_sound = this.add.audio("click");
    telering = this.add.audio("telering");
    pickup = this.add.audio("pickup");
    doors = this.add.audio("doors");
    statictv = this.add.audio("tvstatic");
    dial = this.add.audio("phonedial");
    rainindoor = this.add.audio("rainindoor");
    D = this.add.audio("D");
    Dm = this.add.audio("Dm");
    C = this.add.audio("C");
    Dsh = this.add.audio("Dsh");    
//////////////////////////////////////////////////////////////////////////////
        
        
    
//Dodavanje pocetnih slika////////////////////////////////////////////////////
   
    flat = this.add.sprite(0, 0, 'stan');
        flat.alpha=0;
        
    selme=this.add.sprite(580,460,'selectme');
        selme.alpha=0;
        selme.inputEnabled=true;
        selme.events.onInputOver.add(hoverSve, this);
        selme.events.onInputOut.add(hoverOutSve, this);
        selme.events.onInputDown.add(clickMe, this);
        
    
    seltv=this.add.sprite(0,200,'selecttv');
        seltv.alpha=0;
        seltv.inputEnabled=true;
        seltv.events.onInputOver.add(hoverSve, this);
        seltv.events.onInputOut.add(hoverOutSve, this);
        seltv.events.onInputDown.add(clicktv, this);
        
    selfridge=this.add.sprite(-30,370,'selectfridge');
        selfridge.alpha=0;
        selfridge.inputEnabled=true;
        selfridge.events.onInputOver.add(hoverSve, this);
        selfridge.events.onInputOut.add(hoverOutSve, this);
        selfridge.events.onInputDown.add(clickfridge, this);
        
    selcig=this.add.sprite(800,620,'selectcig');
        selcig.alpha=0;
        selcig.inputEnabled=true;
        selcig.events.onInputOver.add(hoverSve, this);
        selcig.events.onInputOut.add(hoverOutSve, this);
        selcig.events.onInputDown.add(clickcig, this);
        
    selpic=this.add.sprite(740,300,'selectpic');
        selpic.alpha=0;
        selpic.inputEnabled=true;
        selpic.events.onInputOver.add(hoverSve, this);
        selpic.events.onInputOut.add(hoverOutSve, this);
        selpic.events.onInputDown.add(clickpic, this);
                
    selphone=this.add.sprite(920,560,'selectphone');
        selphone.alpha=0;
        selphone.inputEnabled=true;
        selphone.events.onInputOver.add(hoverSve, this);
        selphone.events.onInputOut.add(hoverOutSve, this);
        selphone.events.onInputDown.add(clickphone, this);
        
    seldoor=this.add.sprite(275,280,'selectdoor');
        seldoor.alpha=0;
        seldoor.inputEnabled=true;
        seldoor.events.onInputOver.add(hoverSve, this);
        seldoor.events.onInputOut.add(hoverOutSve, this);
        seldoor.events.onInputDown.add(clickdoor, this);
        
//////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
//Pokretanje/////////////////////////////////////////////////////////////////////////////////
        
    rainindoor.onDecoded.add(faderain, this); //Bez onDecoded ne radi, vjerovatno jer ga ne stigne ucitati...
    this.add.tween(flat).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
        
/////////////////////////////////////////////////////////////////////////////////////////////
        
    },
    
    update: function () {
    
//Provjere za input SPACEBAR tipke za svaki frame u igri/////////////////////////////////////
        
    spaceKey.onDown.add(noclick, this);
    spaceKey.onDown.add(nextme, this);
    spaceKey.onDown.add(nexttv, this);
    spaceKey.onDown.add(nextdoor1, this);
    spaceKey.onDown.add(nextphone1, this);
    spaceKey.onDown.add(nextphone2, this);
    spaceKey.onDown.add(nextphone3, this);
    spaceKey.onDown.add(nextpic, this);
    spaceKey.onDown.add(nextfridge, this);
    spaceKey.onDown.add(nextcig, this);
            
/////////////////////////////////////////////////////////////////////////////////////////////        
    },
    
    
};




//Generic funkcije//////////////////////////////////////////////////////////////////////

function faderain() {
    rainindoor.fadeIn(1000,true);
}

//Funkcija za hover iznad odabira:
function hoverSve(event, sprite){      
        hover_sound.play();
        this.add.tween(event).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za izlazak iz hovera:
function hoverOutSve(event, sprite){      
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za hover strelice:
function hovernext(event, sprite){
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
}

//Funkcija za izlaz iz hoovera strelice:
function hoverOutnext (event, sprite) {
        event.scale.setTo(1.0, 1.0);
}

//Funkcija za SPACEBAR kada nema input na klik:
function noclick (event, sprite) {
    if((input_mode_cig==false) && (input_mode_me==false) && (input_mode_tv==false) && (input_mode_fridge==false) && (input_mode_pic==false) && (input_mode_phone==false) && (input_mode_door==false) && (input_mode_talk==false)) {
        
    if(spacecount%4==0)
        D.play();
    else if(spacecount%4==1)
        Dm.play();
    else if(spacecount%4==2)
        C.play();
    else if(spacecount%4==3)
        Dsh.play();
    
    spacecount++;
        
    }
}

///////////////////////////////////////////////////////////////////////////////////////



//Specificne funkcije odabira//////////////////////////////////////////////////////////

//Funkcija za klik na lika////////////////////////////////////////////////////////////

function clickMe(event, sprite){
    
    event.inputEnabled=false;
    seltv.inputEnabled=false;
    selfridge.inputEnabled=false;
    selphone.inputEnabled=false;
    selpic.inputEnabled=false;
    selcig.inputEnabled=false;
    seldoor.inputEnabled=false;
    
    click_sound.play();
    this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
    
    metxt = this.add.sprite(240, 40, 'metxt');
    metxt.alpha = 0;
    click = this.add.sprite(metxt.x + metxt.width + 20, metxt.y + metxt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(metxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_me = true;
    click.events.onInputDown.add(nextme, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

/////////////////////////////////////////////////////////////////////////////////////

//Funkcija za klik na tv/////////////////////////////////////////////////////////////

function clicktv(event, sprite){
    
    event.inputEnabled=false;
    selme.inputEnabled=false;
    selfridge.inputEnabled=false;
    selphone.inputEnabled=false;
    selpic.inputEnabled=false;
    selcig.inputEnabled=false;
    seldoor.inputEnabled=false;
    
    click_sound.play();
    statictv.play();
    this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
    
    tvtxt = this.add.sprite(180, 25, 'tvtxt');
    tvtxt.alpha = 0;
    click = this.add.sprite(tvtxt.x + tvtxt.width + 20, tvtxt.y + tvtxt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(tvtxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_tv=true;
    click.events.onInputDown.add(nexttv, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

/////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za klik na frizider//////////////////////////////////////////////////////////////

function clickfridge(event, sprite){
    
    event.inputEnabled=false;
    selme.inputEnabled=false;
    seltv.inputEnabled=false;
    selphone.inputEnabled=false;
    selpic.inputEnabled=false;
    selcig.inputEnabled=false;
    seldoor.inputEnabled=false;
    
    click_sound.play();
    this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
    
    fridgetxt = this.add.sprite(35, 25, 'fridgetxt');
    fridgetxt.alpha = 0;
    click = this.add.sprite(fridgetxt.x + fridgetxt.width + 20, fridgetxt.y + fridgetxt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(fridgetxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_fridge = true;
    click.events.onInputDown.add(nextfridge, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za klik na cigare/////////////////////////////////////////////////////////////////////////////////

function clickcig(event, sprite){
    
    event.inputEnabled=false;
    selme.inputEnabled=false;
    seltv.inputEnabled=false;
    selphone.inputEnabled=false;
    selpic.inputEnabled=false;
    seldoor.inputEnabled=false;
    selfridge.inputEnabled=false;
    
    click_sound.play();
    this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
    
    cigtxt = this.add.sprite(300, 50, 'cigtxt');
    cigtxt.alpha = 0;
    click = this.add.sprite(cigtxt.x + cigtxt.width + 20, cigtxt.y + cigtxt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(cigtxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_cig = true;
    click.events.onInputDown.add(nextcig, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za klik na telefon//////////////////////////////////////////////////////////////////////
function clickphone(event, sprite){
    
//Prije nego telefon zvoni////////////////////////////////////////////////////////////////////////
    
    if (counterflat<4){
        
        event.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selfridge.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        seldoor.inputEnabled=false;
        
        click_sound.play();
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
        
        phonetxt1 = this.add.sprite(195, 25, 'phonetxt1');
        phonetxt1.alpha = 0;
        click = this.add.sprite(phonetxt1.x + phonetxt1.width + 20, phonetxt1.y + phonetxt1.height - 30, 'click');
        click.alpha=0;
        this.add.tween(phonetxt1).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        input_mode_phone = true;
        click.events.onInputDown.add(nextphone1, this);
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        
    }
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//Dok telefon zvoni////////////////////////////////////////////////////////////////////////////////////////////
    
    else if (telering.isPlaying){
        
        event.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selfridge.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        seldoor.inputEnabled=false;
        telering.stop();
        
        pickup.play();
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
        
        txtlayerphone = this.add.sprite(5, 400, 'txtlphone');
        txtlayerphone.alpha = 0;
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt1');
        phonetxt2.alpha = 0;
        click = this.add.sprite(phonetxt2.x + phonetxt2.width + 20, phonetxt2.y + phonetxt2.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txtlayerphone).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(phonetxt2).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        input_mode_talk = true;
        click.events.onInputDown.add(nextphone2, this);
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        
        phoneTalk++;
        
    }
    
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//Nakon razgovora prvog razgovora////////////////////////////////////////////////////////////////////////////////
    
    else if (phoneTalk==15){
        
        event.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selfridge.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        seldoor.inputEnabled=false;
        
        dial.play();
        
        //Cekanje da se zvuk odvrti/////////////
        this.time.events.add(12000, function() {
            
            this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
            
            txtlayerphone = this.add.sprite(5, 400, 'txtlphone');
            txtlayerphone.alpha = 0;
            phonetxt3 = this.add.sprite(55, 485, 'memumtxt1');
            phonetxt3.alpha = 0;
            click = this.add.sprite(phonetxt3.x + phonetxt3.width + 20, phonetxt3.y + phonetxt3.height - 30, 'click');
            click.alpha=0;
            this.add.tween(txtlayerphone).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(phonetxt3).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            
            click.inputEnabled=true;
            input_mode_talk = true;
            click.events.onInputDown.add(nextphone3, this);
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            
            phoneTalk++;
            
            }, this)
        
    }
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//Nakon svih razgovora//////////////////////////////////////////////////////////////////////////////////////////////

    else{
        
        event.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selfridge.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        seldoor.inputEnabled=false;
        
        click_sound.play();
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
        
        phonetxt1 = this.add.sprite(425, 65, 'phonetxt2');
        phonetxt1.alpha = 0;
        click = this.add.sprite(phonetxt1.x + phonetxt1.width + 20, phonetxt1.y + phonetxt1.height - 30, 'click');
        click.alpha=0;
        this.add.tween(phonetxt1).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        input_mode_phone = true;
        click.events.onInputDown.add(nextphone1, this);
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        
        
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za klik na vrata//////////////////////////////////////////////////////////////////////////////////////

function clickdoor(event, sprite){
  
//Prije prvog razgovora////////////////////////////////////////////////////////////////////////////////////////// 
    
    if (phoneTalk<1){
        
        event.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selphone.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        selfridge.inputEnabled=false;
        
        click_sound.play();
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
        
        doortxt1 = this.add.sprite(180, 25, 'doortxt1');
        doortxt1.alpha = 0;
        click = this.add.sprite(doortxt1.x + doortxt1.width + 20, doortxt1.y + doortxt1.height - 30, 'click');
        click.alpha=0;
        this.add.tween(doortxt1).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        input_mode_door = true;
        click.events.onInputDown.add(nextdoor1, this);
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        
        }
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
//Poslije prvog razgovora//////////////////////////////////////////////////////////////////////////////////////////
        else {
        
            event.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selphone.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
        
            click_sound.play();
            this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
        
            doortxtl = this.add.sprite(315, 335, 'doortxtlyr');
            doortxtl.alpha = 0;
            exittxt = this.add.sprite(340, 380, 'exittxt');
            exittxt.alpha=0;
            noexittxt = this.add.sprite(340, 455, 'staytxt');
            noexittxt.alpha=0;
            this.add.tween(doortxtl).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(exittxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(noexittxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
            exittxt.inputEnabled=true;
            noexittxt.inputEnabled=true;
            exittxt.events.onInputDown.add(nextdoor2, this);
            exittxt.events.onInputOver.add(hovernext, this);
            exittxt.events.onInputOut.add(hoverOutnext, this);
            noexittxt.events.onInputDown.add(nextdoor2, this);
            noexittxt.events.onInputOver.add(hovernext, this);
            noexittxt.events.onInputOut.add(hoverOutnext, this);
        
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za klik na sliku///////////////////////////////////////////////////////////////////////////////////////

function clickpic(event, sprite){
    
    event.inputEnabled=false;
    selme.inputEnabled=false;
    seltv.inputEnabled=false;
    selphone.inputEnabled=false;
    seldoor.inputEnabled=false;
    selcig.inputEnabled=false;
    selfridge.inputEnabled=false;
    
    click_sound.play();
    this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
    
    pictxt = this.add.sprite(180, 25, 'pictxt');
    pictxt.alpha = 0;
    click = this.add.sprite(pictxt.x + pictxt.width + 20, pictxt.y + pictxt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(pictxt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_pic = true;
    click.events.onInputDown.add(nextpic, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//Funkcije za upravljanje tekstom////////////////////////////////////////////////////////////////////////////////

//Funkcija za potvrdu na obican text vrata///////////////////////////////////////////////////////////////////////

function nextdoor1 (event, sprite) {
    
if (input_mode_door==true){
    
    event.inputEnabled=false;
    input_mode_door=false;    
    
    click_sound.play();
    
    this.add.tween(doortxt1).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    
    counterflat++;
    
    if (counterflat==4){
        
            telering.loop = true;
            telering.play();
            seldoor.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
        
        }
    }
    
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za odabir na vratima//////////////////////////////////////////////////////////////////////////////////

function nextdoor2 (event, sprite) {
    
    //Izlaz iz stana//////////////////////////////////////////////////////////////////////
    
        if (event == exittxt){
            
            doors.play();
            
            seldoor.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selphone.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
            exittxt.inputEnabled=false;
            noexittxt.inputEnabled=false;
            
            this.add.tween(doortxtl).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(exittxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(noexittxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(flat).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
            
            outrotxt = this.add.sprite(this.world.centerX, this.world.centerY, 'doorexittxt');
            outrotxt.anchor.setTo(0.5, 0.5);
            outrotxt.alpha=0;
            
            this.time.events.add(3000, function() {
            this.add.tween(outrotxt).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true );
            rainindoor.fadeOut();
            }, this)
        
            this.time.events.add(8000, function() {
            this.add.tween(outrotxt).to( { alpha: 0 }, 2000, Phaser.Easing.Linear.None, true );
                this.time.events.add(2000, function() {
                    
                    counterflat=0;
                    phoneTalk=0;

                    input_mode_me = false;
                    input_mode_tv = false;
                    input_mode_fridge = false;
                    input_mode_cig = false;
                    input_mode_door = false;
                    input_mode_phone = false;
                    input_mode_pic = false;
                    input_mode_talk = false;
                    
                    this.state.start("Metro");
                    
                }, this)
            }, 
            this)
            
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////
    
    //Ostanak u stanu/////////////////////////////////////////////////////////////////////////
    
        else{
            
            click_sound.play();
        
            this.add.tween(doortxtl).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(exittxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(noexittxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
            exittxt.inputEnabled=false;
            noexittxt.inputEnabled=false;
            seldoor.inputEnabled=true;
            selme.inputEnabled=true;
            seltv.inputEnabled=true;
            selphone.inputEnabled=true;
            selpic.inputEnabled=true;
            selcig.inputEnabled=true;
            selfridge.inputEnabled=true;
            exittxt.inputEnabled=false;
            noexittxt.inputEnabled=false;
            
        }
    
///////////////////////////////////////////////////////////////////////////////////////////////
    
}

//////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za text frizidera//////////////////////////////////////////////////////////////////

function nextfridge (event, sprite) {
    
if (input_mode_fridge==true){
    
    event.inputEnabled=false;
    input_mode_fridge=false;
    
    click_sound.play();
    
    this.add.tween(fridgetxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    
    counterflat++;
    
    if (counterflat==4){
        
        telering.loop = true;
        telering.play();
        seldoor.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        selfridge.inputEnabled=false;
        
        }
    }    
}

////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za text telefona prije i nakon svih poziva//////////////////////////////////////////

function nextphone1 (event, sprite) {
    
if (input_mode_phone==true){ 
        
    event.inputEnabled=false;
    input_mode_phone=false; 
    
    click_sound.play();
    
    this.add.tween(phonetxt1).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    
    counterflat++;
    
    if (counterflat==4){
        
            telering.loop = true;
            telering.play();
            seldoor.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
        
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za text cigara//////////////////////////////////////////////////////////////////////////////

function nextcig (event, sprite) {
    
if (input_mode_cig==true){

    event.inputEnabled=false;
    input_mode_cig = false;
    
    click_sound.play();
        
    this.add.tween(cigtxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    
    counterflat++;
    
    if (counterflat==4){
        
            telering.loop = true;
            telering.play();
            seldoor.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
        
        }
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za text tva////////////////////////////////////////////////////////////////////////////////

function nexttv (event, sprite) {

if (input_mode_tv==true){
    
    event.inputEnabled=false;
    input_mode_tv = false;
    
    click_sound.play();
    
    this.add.tween(tvtxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    counterflat++;
    
    if (counterflat==4){
        
        telering.loop = true;
        telering.play();
        seldoor.inputEnabled=false;
        selme.inputEnabled=false;
        seltv.inputEnabled=false;
        selpic.inputEnabled=false;
        selcig.inputEnabled=false;
        selfridge.inputEnabled=false;
        
        }
    
    }
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za text lika///////////////////////////////////////////////////////////////////////////////

function nextme (event, sprite) {
    
if (input_mode_me==true){
    
    event.inputEnabled=false;
    input_mode_me = false;
    
    click_sound.play();
    
    this.add.tween(metxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    
    counterflat++;
    
    if (counterflat==4){
        
            telering.loop = true;
            telering.play();
            seldoor.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
        }
    }
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcija za potvrdu na text slike/////////////////////////////////////////////////////////////////////

function nextpic (event, sprite) {
    
if(input_mode_pic==true){
    
    event.inputEnabled=false;
    input_mode_pic=false;
    
    click_sound.play();
    
    this.add.tween(pictxt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    seldoor.inputEnabled=true;
    selme.inputEnabled=true;
    seltv.inputEnabled=true;
    selphone.inputEnabled=true;
    selpic.inputEnabled=true;
    selcig.inputEnabled=true;
    selfridge.inputEnabled=true;
    
    counterflat++;
    
    if (counterflat==4){
        
            telering.loop = true;
            telering.play();
            seldoor.inputEnabled=false;
            selme.inputEnabled=false;
            seltv.inputEnabled=false;
            selpic.inputEnabled=false;
            selcig.inputEnabled=false;
            selfridge.inputEnabled=false;
        
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcije za prvi telefonski razgovor////////////////////////////////////////////////////////////////

function nextphone2 (event, sprite) {
    
if((input_mode_talk==true) && (phoneTalk<16)){
        
    event.inputEnabled=false;
    input_mode_talk=false;
    click_sound.play();
    this.add.tween(phonetxt2).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    if(phoneTalk==1)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt1');
    
    else if(phoneTalk==2)
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt2');
    
    else if(phoneTalk==3)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt2');
    
    else if(phoneTalk==4)
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt3');
    
    else if(phoneTalk==5)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt3');
    
    else if(phoneTalk==6)
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt4');
    
    else if(phoneTalk==7)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt4');
    
    else if(phoneTalk==8)
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt5');
    
    else if(phoneTalk==9)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt5');
    
    else if(phoneTalk==10)
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt6');
    
    else if(phoneTalk==11)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt6');
    
    else if(phoneTalk==12)
        phonetxt2= this.add.sprite(55, 485, 'vikyphonetxt7');
    
    else if(phoneTalk==13)
        phonetxt2 = this.add.sprite(55, 485, 'mephonetxt7');
    
    else if(phoneTalk==14)
        phonetxt2 = this.add.sprite(55, 485, 'vikyphonetxt8');
                    
    phonetxt2.alpha = 0;
    click = this.add.sprite(phonetxt2.x + phonetxt2.width + 20, phonetxt2.y + phonetxt2.height - 30, 'click');
    click.alpha=0;
    this.add.tween(phonetxt2).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    phoneTalk++;
    
    click.inputEnabled=true;
    input_mode_talk=true;
    click.events.onInputDown.add(nextphone2, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
    if(phoneTalk==15){
        
        click.inputEnabled=false;
        input_mode_talk=false;
    
        pickup.play();
        
        this.add.tween(phonetxt2).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(txtlayerphone).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
        seldoor.inputEnabled=true;
        selme.inputEnabled=true;
        seltv.inputEnabled=true;
        selphone.inputEnabled=true;
        selpic.inputEnabled=true;
        selcig.inputEnabled=true;
        selfridge.inputEnabled=true;
            
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Funkcije za drugi telefonski razgovor/////////////////////////////////////////////////////////////////////////////////

function nextphone3 (event, sprite) {
    
if ((input_mode_talk==true) && (phoneTalk>15)) {
            
    event.inputEnabled=false;
    input_mode_talk=false;
    
    click_sound.play();
    
    this.add.tween(phonetxt3).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    
    if(phoneTalk==16)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt1');
        
    else if(phoneTalk==17)
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt2');
    
    else if(phoneTalk==18)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt2');
    
    else if(phoneTalk==19)
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt3');
    
    else if(phoneTalk==20)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt3');
    
    else if(phoneTalk==21)
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt4');
    
    else if(phoneTalk==22)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt4');
    
    else if(phoneTalk==23)
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt5');
    
    else if(phoneTalk==24)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt5');
    
    else if(phoneTalk==25)
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt6');
    
    else if(phoneTalk==26)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt6');
    
    else if((phoneTalk==27) || (phoneTalk==29) || (phoneTalk==31))
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt789');
    
    else if(phoneTalk==28)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt7');
    
    else if(phoneTalk==30)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt8');
    
    else if(phoneTalk==32)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt9');
    
    else if(phoneTalk==33)
        phonetxt3 = this.add.sprite(55, 485, 'memumtxt10');
    
    else if(phoneTalk==34)
        phonetxt3 = this.add.sprite(55, 485, 'mumtxt10');
            
    phonetxt3.alpha = 0;
    click = this.add.sprite(phonetxt3.x + phonetxt3.width + 20, phonetxt3.y + phonetxt3.height - 30, 'click');
    click.alpha=0;
    this.add.tween(phonetxt3).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
    phoneTalk++;
    
    click.inputEnabled=true;
    input_mode_talk=true;
    click.events.onInputDown.add(nextphone3, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
    if(phoneTalk==35){
        
        click.inputEnabled=false;
        input_mode_talk=false;
        
        pickup.play();
        
        this.add.tween(phonetxt3).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(txtlayerphone).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
        seldoor.inputEnabled=true;
        selme.inputEnabled=true;
        seltv.inputEnabled=true;
        selphone.inputEnabled=true;
        selpic.inputEnabled=true;
        selcig.inputEnabled=true;
        selfridge.inputEnabled=true;
                
        }
    }    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////