var spaceKey; //Varijabla za SPACEBAR
var spacecount=0;//Brojac udarca na SPACEBAR za tonove
 
game.video1 = function (the_game) {};

game.video1.prototype = {
    
    create: function () {
        
    spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); //Dodjeljivanje signala za SPACEBAR
        
//Dodavanje zvukova///////////////////////////////////////////////////////////
        
    D = this.add.audio("D");
    Dm = this.add.audio("Dm");
    C = this.add.audio("C");
    Dsh = this.add.audio("Dsh"); 
        
//////////////////////////////////////////////////////////////////////////////
        
        
    
//Pokretanje/////////////////////////////////////////////////////////////////////////////////
       
    video = this.add.video('video1');
    video.addToWorld(this.world.centerX, this.world.centerY, 0.5, 0.5, 1, 1);
    video.play();
    
    this.time.events.add(40000, function() {
        
        this.state.start("Viky2");
        
    }, this)
    
/////////////////////////////////////////////////////////////////////////////////////////////
        
    },
    
    update: function () {
    
//Provjere za input SPACEBAR tipke za svaki frame u igri/////////////////////////////////////
        
    spaceKey.onDown.add(noclickvideo1, this);
                    
/////////////////////////////////////////////////////////////////////////////////////////////        
    },
    
    
};




//Generic funkcije//////////////////////////////////////////////////////////////////////

//Funkcija za SPACEBAR kada nema input na klik:
function noclickvideo1 (event, sprite) {
    
    if(spacecount%4==0)
        D.play();
    else if(spacecount%4==1)
        Dm.play();
    else if(spacecount%4==2)
        C.play();
    else if(spacecount%4==3)
        Dsh.play();
    
    spacecount++;
        
}