game.logo = function (the_game) {};

game.logo.prototype = {
          
    create: function () {
        
        //Muzika:
        intro_music = this.add.audio("intro");
        intro_music.play();
        
        ////Plejsholder logoa, ići će slika jednom kad je budemo imali :P:
        logo = this.add.text(500, 500, "Yet_to_be_named_studio_logo", {
            font: "bold 16px Arial",
            fill: "#ffffff",
            align: "center"
            });
            logo.alpha=0;
        
        msg = this.add.text(320, 500, "This game is best experienced with headphones in a full screen browser (F11)", {
            font: "bold 16px Arial",
            fill: "#ffffff",
            align: "center"
            });
            msg.alpha=0;
            
        //Fade in za sliku:
        this.add.tween(logo).to( { alpha: 1 }, 3500, Phaser.Easing.Linear.None, true );
        
        //Fade out za sliku:
        this.time.events.add(4000, function() {
        this.add.tween(logo).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
        }, 
        this)
        
        //Fade in za poruku:
        this.time.events.add(8000, function() {
        this.add.tween(msg).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
        }, 
        this)
        
        //Fade out za poruku:
        this.time.events.add(13000, function() {
        this.add.tween(msg).to( { alpha: 0 }, 3000, Phaser.Easing.Linear.None, true );
        }, 
        this)
                
        //Sljedeci state
        this.time.events.add(16000, function() {
        this.state.start("MainMenu");
        }, 
        this)
    }
};