game.loading = function (the_game) {};

game.loading.prototype = 
    
    {

    preload: function () 
    
        {
        
            // učitavanje slika:
            
            //Izbornik:

            this.load.image('mainmenu_bg', 'img/mainmenu_bg.png');
		    this.load.image('title', 'img/title.png');
            this.load.image('new_game', 'img/newgame_button.png');
            this.load.image('credits', 'img/credits_button.png');
            this.load.image('select', 'img/select.png');
            this.load.image('aura1', 'img/aurora1.png');
            this.load.image('aura2', 'img/aurora2.png');
            this.load.image('aura3', 'img/aurora3.png');
            this.load.image('aura4', 'img/aurora4.png');
            
            //Uvod:
            
            this.load.image('city1', 'img/intro1.png');
            this.load.image('click', 'img/next.png');
            this.load.image('introtext', 'img/introtext.png');
            this.load.image('introtxt1', 'img/introtxt1.png');
            this.load.image('introtxt2', 'img/introtxt2.png');
            this.load.image('introtxt3', 'img/introtxt3.png');
            this.load.image('city2', 'img/intro2.png');
            this.load.image('introtxt4', 'img/introtxt4.png');
            this.load.image('introtxt5', 'img/introtxt5.png');
            this.load.image('introtxt6', 'img/introtxt6.png');
            this.load.image('introtxt7', 'img/introtxt7.png');
            this.load.image('introtxt8', 'img/introtxt8.png');
            this.load.image('introtxt9', 'img/introtxt9.png');
            this.load.image('introtxt10', 'img/introtxt10.png');
            this.load.image('iout1', 'img/introoutro1.png');
            this.load.image('iout2', 'img/introoutro2.png');
            
            //Stan:
            
            this.load.image('stan', 'img/soba.png');
            this.load.image('selectme', 'img/SelectMe.png');
            this.load.image('selecttv', 'img/SelectTV.png');
            this.load.image('selectfridge', 'img/SelectFridge.png');
            this.load.image('selectcig', 'img/SelectCig.png');
            this.load.image('selectpic', 'img/SelectPicture.png');
            this.load.image('selectphone', 'img/SelectPhone.png');
            this.load.image('selectdoor', 'img/SelectDoor.png');
            this.load.image('doortxt1', 'img/flat1doortxt1.png');
            this.load.image('fridgetxt', 'img/flat1fridgetxt.png');
            this.load.image('phonetxt1', 'img/flat1phonetxt1.png');
            this.load.image('cigtxt', 'img/flat1cigtxt.png');
            this.load.image('tvtxt', 'img/flat1tvtxt.png');
            this.load.image('pictxt', 'img/flat1pictxt.png');
            this.load.image('metxt', 'img/flat1metxt.png');
            this.load.image('txtlphone', 'img/textlayerphone.png');
            this.load.image('mephonetxt1', 'img/mephonetxt1.png');
            this.load.image('vikyphonetxt1', 'img/vikiphonetxt1.png');
            this.load.image('mephonetxt2', 'img/mephonetxt2.png');
            this.load.image('vikyphonetxt2', 'img/vikiphonetxt2.png');
            this.load.image('mephonetxt3', 'img/mephonetxt3.png');
            this.load.image('vikyphonetxt3', 'img/vikiphonetxt3.png');
            this.load.image('mephonetxt4', 'img/mephonetxt4.png');
            this.load.image('vikyphonetxt4', 'img/vikiphonetxt4.png');
            this.load.image('mephonetxt5', 'img/mephonetxt5.png');
            this.load.image('vikyphonetxt5', 'img/vikiphonetxt5.png');
            this.load.image('mephonetxt6', 'img/mephonetxt6.png');
            this.load.image('vikyphonetxt6', 'img/vikiphonetxt6.png');
            this.load.image('mephonetxt7', 'img/mephonetxt7.png');
            this.load.image('vikyphonetxt7', 'img/vikiphonetxt7.png');
            this.load.image('vikyphonetxt8', 'img/vikiphonetxt8.png');
            this.load.image('doortxtlyr', 'img/doorlayer.png');
            this.load.image('exittxt', 'img/exittxt.png');
            this.load.image('staytxt', 'img/noexittxt.png');
            this.load.image('doorexittxt', 'img/flat1outrotxt.png');
            this.load.image('memumtxt1', 'img/memumtxt1.png');
            this.load.image('mumtxt1', 'img/mumtxt1.png');
            this.load.image('memumtxt2', 'img/memumtxt2.png');
            this.load.image('mumtxt2', 'img/mumtxt2.png');
            this.load.image('memumtxt3', 'img/memumtxt3.png');
            this.load.image('mumtxt3', 'img/mumtxt3.png');
            this.load.image('memumtxt4', 'img/memumtxt4.png');
            this.load.image('mumtxt4', 'img/mumtxt4.png');
            this.load.image('memumtxt5', 'img/memumtxt5.png');
            this.load.image('mumtxt5', 'img/mumtxt5.png');
            this.load.image('memumtxt6', 'img/memumtxt6.png');
            this.load.image('mumtxt6', 'img/mumtxt6.png');
            this.load.image('memumtxt789', 'img/memumtxt789.png');
            this.load.image('mumtxt7', 'img/mumtxt7.png');
            this.load.image('mumtxt8', 'img/mumtxt8.png');
            this.load.image('mumtxt9', 'img/mumtxt9.png');
            this.load.image('memumtxt10', 'img/memumtxt10.png');
            this.load.image('mumtxt10', 'img/mumtxt10.png');
            this.load.image('phonetxt2', 'img/flat1phonetxt2.png');
            
            //Metro:
            
            this.load.image('metro', 'img/metro.png');
            this.load.image('mvrata', 'img/metrodoor.png');
            this.load.image('mnovine', 'img/metropaper.png');
            this.load.image('mnigdje', 'img/metronowhere.png');
            this.load.image('lik1', 'img/metrolik1.png');
            this.load.image('lik2', 'img/metrolik2.png');
            this.load.image('mnovinetxt', 'img/metronovinetxt.png');
            this.load.image('mvratatxt', 'img/metrovratatxt.png');
            this.load.image('mnigdjetxt', 'img/metronigdjetxt.png');
            this.load.image('mtxtlm', 'img/metrotlme.png');
            this.load.image('mtxtl1', 'img/metrotl1.png');
            this.load.image('mtxtl2', 'img/metrotl2.png');
            this.load.image('mtxtl3', 'img/metrotl3.png');
            this.load.image('ml1txt1', 'img/ml1txt1.png');
            this.load.image('ml1txt2', 'img/ml1txt2.png');
            this.load.image('ml1txt3', 'img/ml1txt3.png');
            this.load.image('mlm1txt1', 'img/mlm1txt1.png');
            this.load.image('mlm1txt2', 'img/mlm1txt2.png');
            this.load.image('ml2txt1', 'img/ml2txt1.png');
            this.load.image('ml2txt2', 'img/ml2txt2.png');
            this.load.image('ml2txt3', 'img/ml2txt3.png');
            this.load.image('mlm2txt1', 'img/mlm2txt1.png');
            this.load.image('mlm2txt2', 'img/mlm2txt2.png');
            this.load.image('metroizlaz', 'img/metroizlaz.png');
            this.load.image('lik3', 'img/metrolik3.png');
            this.load.image('ml3txt1', 'img/ml3txt1.png');
            this.load.image('ml3txt2', 'img/ml3txt2.png');
            this.load.image('ml3txt3', 'img/ml3txt3.png');
            this.load.image('mlm3txt1', 'img/mlm3txt1.png');
            this.load.image('mlm3txt2', 'img/mlm3txt2.png');
            this.load.image('mlm3txt3', 'img/mlm3txt3.png');
            this.load.image('mout', 'img/moutrotxt.png');
            
            //Viky:
            
            this.load.image('flat2', 'img/flat2.png');
            this.load.image('cigdown', 'img/flat2cigdown.png');
            this.load.image('cigup', 'img/flat2cigup.png');
            this.load.image('smoke', 'img/flat2smoke.png');
            this.load.image('selviky', 'img/flat2viky.png');
            this.load.image('selbed', 'img/flat2bed.png');
            this.load.image('txtlayer2', 'img/flat2textlayer.png');
            this.load.image('f2txt1', 'img/flat2txt1.png');
            this.load.image('f2txt2', 'img/flat2txt2.png');
            this.load.image('f2txt3', 'img/flat2txt3.png');
            this.load.image('f2txt4', 'img/flat2txt4.png');
            this.load.image('f2txt5', 'img/flat2txt5.png');
            this.load.image('selvikytxt', 'img/flat2selvikytxt.png');
            
            this.load.image('kadar2', 'img/flat2kadar2.png');
            this.load.image('viky', 'img/viky.png');
            this.load.image('f2txtlyr2', 'img/flat2txtlayer2.png');
            this.load.image('bloodscr', 'img/bloodscreen.png');
            this.load.image('vttxt1', 'img/vttxt1.png');
            this.load.image('vttxt2', 'img/vttxt2.png');
            this.load.image('vttxt3', 'img/vttxt3.png');
            this.load.image('vttxt4', 'img/vttxt4.png');
            this.load.image('vttxt5', 'img/vttxt5.png');
            this.load.image('vttxt6', 'img/vttxt6.png');
            this.load.image('vttxt7', 'img/vttxt7.png');
            this.load.image('vttxt8', 'img/vttxt8.png');
            this.load.image('vttxt9', 'img/vttxt9.png');
            this.load.image('vttxt10', 'img/vttxt10.png');
            this.load.image('vttxt11', 'img/vttxt11.png');
            this.load.image('vttxt12', 'img/vttxt12.png');
            this.load.image('vttxt13', 'img/vttxt13.png');
            this.load.image('vttxt14', 'img/vttxt14.png');
            this.load.image('vttxt15', 'img/vttxt15.png');
            this.load.image('vttxt16', 'img/vttxt16.png');
            this.load.image('vttxtfail', 'img/vttxtfail.png');
            this.load.image('nod', 'img/nod.png');
            this.load.image('scream', 'img/SCREAM.png');
            this.load.image('flat2outrotxt', 'img/flat2outrotxt.png');
            this.load.image('die', 'img/die.png');
            
            this.load.image('flat3', 'img/flat3.png');
            this.load.image('seldoor2', 'img/flat3seldoor.png');
            this.load.image('selviky2', 'img/flat3selviky.png');
            this.load.image('novac', 'img/novac.png');
            this.load.image('selmoney', 'img/selmoney.png');
            this.load.image('flt3txt1', 'img/flat3txt1.png');
            this.load.image('flt3txtdoor', 'img/flat3txtdoor.png');
            this.load.image('flt3txtsleep', 'img/flat3txtsleep.png');
            this.load.image('flt3txtmoney', 'img/flat3txtmoney.png');
            this.load.image('f3l1', 'img/flat3txtlayer1.png');
            this.load.image('f3l2', 'img/flat3txtlayer2.png');
            this.load.image('vtxt1', 'img/flat3viky1.png');
            this.load.image('vtxt2', 'img/flat3viky2.png');
            this.load.image('vtxt3', 'img/flat3viky3.png');
            this.load.image('vtxt4', 'img/flat3viky4.png');
            this.load.image('mtxt1', 'img/flat3me1.png');
            this.load.image('mtxt2', 'img/flat3me2.png');
            this.load.image('mtxt3', 'img/flat3me3.png');
            this.load.image('mtxt4', 'img/flat3me4.png');
            this.load.image('citynight', 'img/cityatnight.png');
            this.load.image('alley', 'img/alley.png');
            this.load.image('alleylik', 'img/alleylik.png');
            this.load.image('atxtl1', 'img/alleytxtlayer1.png');
            this.load.image('atxtl2', 'img/alleytxtlayer2.png');
            this.load.image('amtxt1', 'img/alleytxtm1.png');
            this.load.image('astxt1', 'img/alleytxts1.png');
            this.load.image('amtxt2', 'img/alleytxtm2.png');
            this.load.image('astxt2', 'img/alleytxts2.png');
            this.load.image('amtxt3', 'img/alleytxtm3.png');
            this.load.image('astxt3', 'img/alleytxts3.png');
            this.load.image('amtxt4', 'img/alleytxtm4.png');
            this.load.image('astxt4', 'img/alleytxts4.png');
            this.load.image('amtxt5', 'img/alleytxtm5.png');
            this.load.image('astxt5', 'img/alleytxts5.png');
            this.load.image('amtxt6', 'img/alleytxtm6.png');
            this.load.image('astxt6', 'img/alleytxts6.png');
            this.load.image('amtxt7', 'img/alleytxtm7.png');
            this.load.image('astxt7', 'img/alleytxts7.png');
            this.load.image('aout1', 'img/alleyoutrotxt1.png');
            this.load.image('aout2', 'img/alleyoutrotxt2.png');
            this.load.image('aout3', 'img/alleyoutrotxt3.png');
            
            
            
                       
            // učitavanje muzike:
            this.load.audio("mainmenu_loop", "snd/mainmenu.mp3");
            this.load.audio("intro", "snd/intro.mp3");
            this.load.audio("hoover", "snd/hoover.mp3");
            this.load.audio("click", "snd/click.mp3");
            
            this.load.audio("trafic", "snd/trafic.mp3");
            this.load.audio("rain", "snd/rain.mp3");
            
            this.load.audio("rainindoor", "snd/rainindoor.mp3");
            this.load.audio("telering", "snd/telering.mp3");
            this.load.audio("pickup", "snd/phonepickup.mp3");
            this.load.audio("doors", "snd/doors.mp3");
            this.load.audio("phonedial", "snd/phonedial.mp3");
            this.load.audio("tvstatic", "snd/tvstatic.mp3");
            this.load.audio("D", "snd/pianokeyD.mp3");
            this.load.audio("Dm", "snd/pianokeydm.mp3");
            this.load.audio("C", "snd/pianokeyC.mp3");
            this.load.audio("Dsh", "snd/pianokeyDsh.mp3");
            
            this.load.audio("metromoving", "snd/metromoving.mp3");
            this.load.audio("metrostoping", "snd/metrostoping.mp3");
            this.load.audio("metronoise", "snd/metronoise.mp3");
            
            this.load.audio("lighter", "snd/lighter.mp3");
            this.load.audio("cuffs", "snd/cuffs.mp3");
            this.load.audio("heels", "snd/heels.mp3");
            this.load.audio("vikymusic", "snd/vikymusic.mp3");
            this.load.audio("knife", "snd/slashknife.mp3");
            this.load.audio("moan", "snd/scream.mp3");
            
            this.load.audio("trafic_night", "snd/trafic_night.mp3");
            this.load.audio("steps", "snd/steps.mp3");
            this.load.audio("basstheme", "snd/themebassonly.mp3");
            
            // učitavanje videa:
            this.load.video("video1", "vid/video1.mp4");

		        
        },
        
     create: function() 
    
        {
        
            //sljedeci state - tu mjenjajte state za testiranje
            this.state.start("Logo");
            //this.state.start("Metro");
    
        }
    };

