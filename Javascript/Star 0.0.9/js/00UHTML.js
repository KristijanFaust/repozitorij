window.onload = function() {

    //instanca igre postavljena na 100% browser prozora
    var the_game = new Phaser.Game(1280, 1024, Phaser.AUTO, "");

    
    //popis stateova
    the_game.state.add("Beginning", game.beginning);
    the_game.state.add("Loading", game.loading);
    the_game.state.add("Logo", game.logo);
	the_game.state.add("MainMenu", game.mainmenu);
    the_game.state.add("Intro", game.intro);
    the_game.state.add("Stan", game.stan);
    the_game.state.add("Metro", game.metro);
    the_game.state.add("Viky", game.viky);
    the_game.state.add("Video1", game.video1);
    the_game.state.add("Viky2", game.viky2);
    
    
    // pocetni state
    the_game.state.start("Beginning");
 }


