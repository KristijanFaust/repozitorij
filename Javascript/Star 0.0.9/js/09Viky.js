var counterflat2=0; //Brojac interakcija

//Varijable za omogucavanje inputa sa tipkovnice/////////////////////////////////

var input_mode_txt = false;
var input_mode_viky = false;

////////////////////////////////////////////////////////////////////////////////

var spaceKey; //Varijabla za SPACEBAR
var spacecount=0;//Brojac udarca na SPACEBAR za tonove
 
game.viky = function (the_game) {};

game.viky.prototype = {
    
      
    create: function () {
        
    spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); //Dodjeljivanje signala za SPACEBAR
        
//Dodavanje zvukova///////////////////////////////////////////////////////////
        
    hover_sound = this.add.audio("hoover");
    click_sound = this.add.audio("click");
    doors = this.add.audio("doors");
    lighter = this.add.audio("lighter");
    cuffs = this.add.audio("cuffs");
    heels = this.add.audio("heels");
    music = this.add.audio("vikymusic");
    knife = this.add.audio("knife");
    moan = this.add.audio("moan");
    D = this.add.audio("D");
    Dm = this.add.audio("Dm");
    C = this.add.audio("C");
    Dsh = this.add.audio("Dsh"); 
        
//////////////////////////////////////////////////////////////////////////////
        
        
    
//Dodavanje pocetnih slika////////////////////////////////////////////////////
   
    flat2 = this.add.sprite(0, 0, 'flat2');
        flat2.alpha=0;
            
    selviky=this.add.sprite(209,494,'selviky');
        selviky.alpha=0;
                
    selbed=this.add.sprite(340,550,'selbed');
        selbed.alpha=0;
                            
//////////////////////////////////////////////////////////////////////////////////////////////
        
        
        
//Pokretanje/////////////////////////////////////////////////////////////////////////////////
    
    doors.play();
        
    this.time.events.add(3000, function() {
        
    this.add.tween(flat2).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
        
    this.time.events.add(5000, function() {
        
        txtlayer = this.add.sprite(300, 350, 'txtlayer2');
        txtlayer.alpha = 0;
        txt = this.add.sprite(330, 390, 'f2txt1');
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(txt).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
        
        this.time.events.add(1000, function() {
            
            click.inputEnabled=true;
            input_mode_txt=true;
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(clicktxtnext, this);
            
         }, this)
        
    }, this)
    
     }, this)
    
/////////////////////////////////////////////////////////////////////////////////////////////
        
    },
    
    update: function () {
    
//Provjere za input SPACEBAR tipke za svaki frame u igri/////////////////////////////////////
        
    spaceKey.onDown.add(noclickflat2, this);
    spaceKey.onDown.add(clicktxtnext, this);
    spaceKey.onDown.add(nextViky, this);
                
/////////////////////////////////////////////////////////////////////////////////////////////        
    },
    
    
};




//Generic funkcije//////////////////////////////////////////////////////////////////////

//Funkcija za hover iznad odabira:
function hoverSve(event, sprite){      
        hover_sound.play();
        this.add.tween(event).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za izlazak iz hovera:
function hoverOutSve(event, sprite){      
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za hover strelice:
function hovernext(event, sprite){
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
}

//Funkcija za izlaz iz hoovera strelice:
function hoverOutnext (event, sprite) {
        event.scale.setTo(1.0, 1.0);
}

//Funkcija za SPACEBAR kada nema input na klik:
function noclickflat2 (event, sprite) {
    
    if((input_mode_txt==false) && (input_mode_viky==false)) {
            
    if(spacecount%4==0)
        D.play();
    else if(spacecount%4==1)
        Dm.play();
    else if(spacecount%4==2)
        C.play();
    else if(spacecount%4==3)
        Dsh.play();
    
    spacecount++;
        
    }
}

//Funkcija za loop cigare:
function smoking() {

                cigup = this.add.sprite(265, 525, 'cigup');
                cigup.alpha=0;
                this.add.tween(cigup).to({alpha: 1}, 200, Phaser.Easing.Linear.None, true);
                
                this.time.events.add(1000, function() {
                
                    this.add.tween(cigup).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
                    cigdown = this.add.sprite(267, 528, 'cigdown');
                    cigdown.alpha=0;
                    smoke = this.add.sprite(130, 230, 'smoke');
                    smoke.alpha=0;
                    this.add.tween(cigdown).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                    this.add.tween(smoke).to({alpha: 1}, 1500, Phaser.Easing.Linear.None, true);
                    
                        this.time.events.add(3000, function() {
                            
                                this.add.tween(smoke).to({alpha: 0}, 3500, Phaser.Easing.Linear.None, true);
                                this.add.tween(cigdown).to({alpha: 0}, 9500, Phaser.Easing.Linear.None, true);
                            
                         }, this)
                    }, this)

}

//Funkcija za upravljanje monologom:

function clicktxtnext(event, sprite){ 
    
    if (input_mode_txt==true){
        
        event.inputEnabled=false;
        input_mode_talk=false;
        
        click_sound.play();
        
        this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
        counterflat2++;
    
        if(counterflat2==1)
            txt = this.add.sprite(330, 385, 'f2txt2');
            
        else if(counterflat2==2)
            txt = this.add.sprite(330, 385, 'f2txt3');  
        
        else if(counterflat2==4)
            txt = this.add.sprite(330, 385, 'f2txt5');
        
        else if(counterflat2==7)
            txt = this.add.sprite(25, 385, 'vttxt2');
        
        else if(counterflat2==8)
            txt = this.add.sprite(25, 385, 'vttxt3');
        
        else if(counterflat2==9)
            txt = this.add.sprite(25, 385, 'vttxt4');
        
        else if(counterflat2==10)
            txt = this.add.sprite(25, 385, 'vttxt5');
        
        else if(counterflat2==11)
            txt = this.add.sprite(25, 385, 'vttxt6');
        
        else if(counterflat2==12)
            txt = this.add.sprite(25, 385, 'vttxt7');
        
        if(counterflat2==3){
            
            click.inputEnabled=false;
            input_mode_txt=false;
            
            this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(txtlayer).to({alpha: 0}, 1500, Phaser.Easing.Linear.None, true);
            
            lighter.play();
            this.time.events.add(3000, function() {
                
                cigup = this.add.sprite(265, 525, 'cigup');
                cigup.alpha=0;
                this.add.tween(cigup).to({alpha: 1}, 200, Phaser.Easing.Linear.None, true);
                
                this.time.events.add(1000, function() {
                
                    this.add.tween(cigup).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
                    cigdown = this.add.sprite(267, 528, 'cigdown');
                    cigdown.alpha=0;
                    smoke = this.add.sprite(130, 230, 'smoke');
                    smoke.alpha=0;
                    this.add.tween(cigdown).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                    this.add.tween(smoke).to({alpha: 1}, 1500, Phaser.Easing.Linear.None, true);
                    
                        this.time.events.add(3000, function() {
                            
                                this.add.tween(smoke).to({alpha: 0}, 3500, Phaser.Easing.Linear.None, true);
                                this.add.tween(cigdown).to({alpha: 0}, 3500, Phaser.Easing.Linear.None, true);
                            
                         }, this)
                        
                        this.time.events.add(4000, function() {
                            
                                this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                                txt = this.add.sprite(330, 390, 'f2txt4');
                                txt.alpha = 0;
                                click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
                                click.alpha=0;
                                this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                                this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    
                                click.inputEnabled=true;
                                click.events.onInputOver.add(hovernext, this);
                                click.events.onInputOut.add(hoverOutnext, this);
                                click.events.onInputDown.add(clicktxtnext, this);
                                input_mode_txt=true;
                                cigarette = this.time.events.repeat(Phaser.Timer.SECOND * 8, 6, smoking, this);
                                                                    
                         }, this)
            
             }, this)
            
             }, this)
        }
        
        else if(counterflat2==5){
            
            click.inputEnabled=false;
            input_mode_txt=false;
            
            this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(txtlayer).to({alpha: 0}, 1500, Phaser.Easing.Linear.None, true);
            
            selviky.inputEnabled=true;
            selviky.events.onInputOver.add(hoverSve, this);
            selviky.events.onInputOut.add(hoverOutSve, this);
            selviky.events.onInputDown.add(clickViky, this);
            selbed.inputEnabled=true;
            selbed.events.onInputOver.add(hoverSve, this);
            selbed.events.onInputOut.add(hoverOutSve, this);
            selbed.events.onInputDown.add(clickBed, this);
        }
        
        else if(counterflat2==12){
            
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        click.events.onInputDown.add(clicktxtnod, this);
        input_mode_txt=false;
            
            
        }
        
        
        
        
        else{
            
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        click.events.onInputDown.add(clicktxtnext, this);
        input_mode_txt=true;
                     
        }
             
    }
}


//Funkcija za klik na viky:

function clickViky(event, sprite){      
    
    click_sound.play();
    this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    txt = this.add.sprite(330, 390, 'selvikytxt');
    txt.alpha = 0;
    click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
    click.inputEnabled=true;
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    click.events.onInputDown.add(nextViky, this);
    input_mode_viky=true;
    
}

//Funkcija za potvrdu teksta na viky:

function nextViky(event, sprite){
    
if((input_mode_viky==true) && (counterflat2<6)){
    
    click_sound.play();
    click.inputEnabled=false;
    input_mode_viky=false;
            
    this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
    this.add.tween(txtlayer).to({alpha: 0}, 1500, Phaser.Easing.Linear.None, true);
    
}

}

//Funkcija za klik na krevet:
function clickBed(event, sprite){
    
selbed.inputEnabled=false;    
click_sound.play();  
this.add.tween(flat2).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
cuffs.play();
kadar2 = this.add.sprite(0, 0, 'kadar2');
kadar2.alpha=0;
    
this.time.events.remove(cigarette);

this.time.events.add(1000, function() {
    
        selbed.destroy();
        selviky.destroy();
        cigup.destroy();
        cigdown.destroy();
        smoke.destroy();
    
}, this)
    
this.time.events.add(15000, function() {
                            
            this.add.tween(kadar2).to({alpha: 1}, 2000, Phaser.Easing.Linear.None, true);
    
            this.time.events.add(3000, function() {
                
                heels.play();
                    
                    this.time.events.add(4000, function() {
                        
                        
                        this.time.events.add(1000, function() {
                            music.loop=true;
                            music.play();
                            
                        }, this)
                    
                        viky = this.add.sprite(605, 345, 'viky');
                        viky.alpha=0;
                        
                        this.add.tween(viky).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
                            
                            this.time.events.add(2000, function() {
                    
                            txtlayer = this.add.sprite(15, 345, 'f2txtlyr2');
                            txtlayer.alpha=0;
                            txt = this.add.sprite(25, 385, 'vttxt1');
                            txt.alpha=0;
                            click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
                            click.alpha=0;
                                
                            this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                                
                            counterflat2++;
    
                            click.inputEnabled=true;
                            input_mode_txt=true;
                            click.events.onInputOver.add(hovernext, this);
                            click.events.onInputOut.add(hoverOutnext, this);
                            click.events.onInputDown.add(clicktxtnext, this);
                    
                    }, this)
                    
                    }, this)
                
                }, this)
                                
}, this)

    
}

//Funkcija za vrisak:

function clickscream(event, sprite){
    
    if(counterflat2==14){
        
                            scream1.destroy();
        
    }
    
    else if(counterflat2==15){
        
                            scream1.destroy();
                            scream2.destroy();
        
    }
    
    else if(counterflat2==16){
        
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                                
    
        
    }
    
    else if(counterflat2==17){
        
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                            scream5.destroy();
                            scream6.destroy();
                            scream7.destroy();
                            scream8.destroy();
                            
        
    }
    
    else if(counterflat2==18){
        
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                            scream5.destroy();
                            scream6.destroy();
                            scream7.destroy();
                            scream8.destroy();
                            scream9.destroy();
                            scream10.destroy();
                            scream11.destroy();
                            scream12.destroy();
                            scream13.destroy();
                            scream14.destroy();
                            scream15.destroy();
                            scream16.destroy();
                                
        
    }
    
    else if(counterflat2==19){
    
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                            scream5.destroy();
                            scream6.destroy();
                            scream7.destroy();
                            scream8.destroy();
                            scream9.destroy();
                            scream10.destroy();
                            scream11.destroy();
                            scream12.destroy();
                            scream13.destroy();
                            scream14.destroy();
                            scream15.destroy();
                            scream16.destroy();
                            scream17.destroy();
                            scream18.destroy();
                            scream19.destroy();
                            scream20.destroy();
                            scream21.destroy();
                            scream22.destroy();
                            scream23.destroy();
                            scream24.destroy();
                            scream25.destroy();
                            scream26.destroy();
                            scream27.destroy();
                            scream28.destroy();
                            scream29.destroy();
                            scream30.destroy();
                            scream31.destroy();
                            scream32.destroy();
                                    
    }
    
    else if(counterflat2==20){
        
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                            scream5.destroy();
                            scream6.destroy();
                            scream7.destroy();
                            scream8.destroy();
                            scream9.destroy();
                            scream10.destroy();
                            scream11.destroy();
                            scream12.destroy();
                            scream13.destroy();
                            scream14.destroy();
                            scream15.destroy();
                            scream16.destroy();
                            scream17.destroy();
                            scream18.destroy();
                            scream19.destroy();
                            scream20.destroy();
                            scream21.destroy();
                            scream22.destroy();
                            scream23.destroy();
                            scream24.destroy();
                            scream25.destroy();
                            scream26.destroy();
                            scream27.destroy();
                            scream28.destroy();
                            scream29.destroy();
                            scream30.destroy();
                            scream31.destroy();
                            scream32.destroy();
                            scream33.destroy();
                            scream34.destroy();
                            scream35.destroy();
                            scream36.destroy();
                            scream37.destroy();
                            scream38.destroy();
                            scream39.destroy();
                            scream40.destroy();
                            scream41.destroy();
                            scream42.destroy();
                            scream43.destroy();
                            scream44.destroy();
                            scream45.destroy();
                            scream46.destroy();
                            scream47.destroy();
                            scream48.destroy();
                            scream49.destroy();
                            scream50.destroy();
                            scream51.destroy();
                            scream52.destroy();
                            scream53.destroy();
                            scream54.destroy();
                            scream55.destroy();
                            scream56.destroy();
                            scream57.destroy();
                            scream58.destroy();
                            scream59.destroy();
                            scream60.destroy();
                            scream61.destroy();
                            scream62.destroy();
                            scream63.destroy();
                            scream64.destroy();    
        
    }                       
                            this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
                            this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
                            click.inputEnabled = false;
                            input_mode_txt=false;
                            moan.play();
                            txt = this.add.sprite(25, 345, 'vttxtfail');
                            txt.alpha=0;
                            this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            
                            this.time.events.add(7000, function() {
                                
                                music.fadeOut(4000);                        
                                this.add.tween(txtlayer).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(txt).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(kadar2).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(viky).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(click).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                                                
                                    this.time.events.add(4000, function() {
                                                
                                            counterflat2=0;
                                            input_mode_txt = false;
                                            input_mode_viky = false;
                                        
                                            this.state.start("Video1");
                                                                                                
                                    }, this)
                                    
                             }, this)


}

function clickdie (event,sprite){
    
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                            scream5.destroy();
                            scream6.destroy();
                            scream7.destroy();
                            scream8.destroy();
                            scream9.destroy();
                            scream10.destroy();
                            scream11.destroy();
                            scream12.destroy();
                            scream13.destroy();
                            scream14.destroy();
                            scream15.destroy();
                            scream16.destroy();
                            scream17.destroy();
                            scream18.destroy();
                            scream19.destroy();
                            scream20.destroy();
                            scream21.destroy();
                            scream22.destroy();
                            scream23.destroy();
                            scream24.destroy();
                            scream25.destroy();
                            scream26.destroy();
                            scream27.destroy();
                            scream28.destroy();
                            scream29.destroy();
                            scream30.destroy();
                            scream31.destroy();
                            scream32.destroy();
                            scream33.destroy();
                            scream34.destroy();
                            scream35.destroy();
                            scream36.destroy();
                            scream37.destroy();
                            scream38.destroy();
                            scream39.destroy();
                            scream40.destroy();
                            scream41.destroy();
                            scream42.destroy();
                            scream43.destroy();
                            scream44.destroy();
                            scream45.destroy();
                            scream46.destroy();
                            scream47.destroy();
                            scream48.destroy();
                            scream49.destroy();
                            scream50.destroy();
                            scream51.destroy();
                            scream52.destroy();
                            scream53.destroy();
                            scream54.destroy();
                            scream55.destroy();
                            scream56.destroy();
                            scream57.destroy();
                            scream58.destroy();
                            scream59.destroy();
                            scream60.destroy();
                            scream61.destroy();
                            scream62.destroy();
                            scream63.destroy();
                            scream64.destroy();  

f2otxt = this.add.sprite(this.world.centerX, this.world.centerY, 'flat2outrotxt');
    f2otxt.anchor.setTo(0.5, 0.5);
    f2otxt.alpha=0;
                            
    this.time.events.add(8000, function() {  
                                    
        this.add.tween(f2otxt).to({alpha: 1}, 1500, Phaser.Easing.Linear.None, true);

        this.time.events.add(4000, function() {
            
            music.fadeOut(4000);                                                                                                           
            this.add.tween(f2otxt).to({alpha: 0}, 3500, Phaser.Easing.Linear.None, true);
                                                
            this.time.events.add(10000, function() {
                
                counterflat2=0;
                input_mode_txt = false;
                input_mode_viky = false;
                                                
                this.state.start("Video1");
                                                
            }, this)
                                                
        }, this)
        
    }, this)

}

//Funkcija za nod:
function clicktxtnod(event, sprite){ 
        
        click.inputEnabled=false;
        input_mode_talk=false;
        
        click_sound.play();
        
        this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
        
        counterflat2++;
    
        if(counterflat2>=12){
            
            click.inputEnabled=false;
                        
            this.add.tween(txt).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            this.add.tween(txtlayer).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
            
            bldscr = this.add.sprite(0,0,'bloodscr');
            bldscr.alpha=0;
            
            this.time.events.add(1000, function() {
                    
                    knife.play();
                    this.add.tween(bldscr).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
                    
                    if(counterflat2==19){
                         
                         this.time.events.add(300, function() {
                            
                            this.add.tween(bldscr).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
                            knife.play();
                             
                         }, this)
                    }
                
                
                    if(counterflat2==20){
                         
                         this.time.events.add(300, function() {
                            
                            this.add.tween(bldscr).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
                            knife.play();
                                                                                  
                         }, this)
                    }
                            
                        this.time.events.add(1000, function() {
                            this.add.tween(bldscr).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                            if(counterflat2==21){
                                
                                
                            scream1.destroy();
                            scream2.destroy();
                            scream3.destroy();
                            scream4.destroy();
                            scream5.destroy();
                            scream6.destroy();
                            scream7.destroy();
                            scream8.destroy();
                            scream9.destroy();
                            scream10.destroy();
                            scream11.destroy();
                            scream12.destroy();
                            scream13.destroy();
                            scream14.destroy();
                            scream15.destroy();
                            scream16.destroy();
                            scream17.destroy();
                            scream18.destroy();
                            scream19.destroy();
                            scream20.destroy();
                            scream21.destroy();
                            scream22.destroy();
                            scream23.destroy();
                            scream24.destroy();
                            scream25.destroy();
                            scream26.destroy();
                            scream27.destroy();
                            scream28.destroy();
                            scream29.destroy();
                            scream30.destroy();
                            scream31.destroy();
                            scream32.destroy();
                            scream33.destroy();
                            scream34.destroy();
                            scream35.destroy();
                            scream36.destroy();
                            scream37.destroy();
                            scream38.destroy();
                            scream39.destroy();
                            scream40.destroy();
                            scream41.destroy();
                            scream42.destroy();
                            scream43.destroy();
                            scream44.destroy();
                            scream45.destroy();
                            scream46.destroy();
                            scream47.destroy();
                            scream48.destroy();
                            scream49.destroy();
                            scream50.destroy();
                            scream51.destroy();
                            scream52.destroy();
                            scream53.destroy();
                            scream54.destroy();
                            scream55.destroy();
                            scream56.destroy();
                            scream57.destroy();
                            scream58.destroy();
                            scream59.destroy();
                            scream60.destroy();
                            scream61.destroy();
                            scream62.destroy();
                            scream63.destroy();
                            scream64.destroy();  

                            this.time.events.add(2000, function() {
                                
                                    scream1 = this.add.sprite(840,110,'die');
                                        scream1.inputEnabled=true;
                                        scream1.events.onInputOver.add(hovernext, this);
                                        scream1.events.onInputOut.add(hoverOutnext, this);
                                        scream1.events.onInputDown.add(clickdie, this);
                                
                                    scream2 = this.add.sprite(690,800,'die');
                                        scream2.inputEnabled=true;
                                        scream2.events.onInputOver.add(hovernext, this);
                                        scream2.events.onInputOut.add(hoverOutnext, this);
                                        scream2.events.onInputDown.add(clickdie, this);
                                
                                    scream3 = this.add.sprite(25,180,'die');
                                        scream3.inputEnabled=true;
                                        scream3.events.onInputOver.add(hovernext, this);
                                        scream3.events.onInputOut.add(hoverOutnext, this);
                                        scream3.events.onInputDown.add(clickdie, this);
                                
                                    scream4 = this.add.sprite(990,490,'die');
                                        scream4.inputEnabled=true;
                                        scream4.events.onInputOver.add(hovernext, this);
                                        scream4.events.onInputOut.add(hoverOutnext, this);
                                        scream4.events.onInputDown.add(clickdie, this);
                                
                                    scream5 = this.add.sprite(888,350,'die');
                                        scream5.inputEnabled=true;
                                        scream5.events.onInputOver.add(hovernext, this);
                                        scream5.events.onInputOut.add(hoverOutnext, this);
                                        scream5.events.onInputDown.add(clickdie, this);
                                    
                                    scream6 = this.add.sprite(780,512,'die');
                                        scream6.inputEnabled=true;
                                        scream6.events.onInputOver.add(hovernext, this);
                                        scream6.events.onInputOut.add(hoverOutnext, this);
                                        scream6.events.onInputDown.add(clickdie, this);
                                    
                                    scream7 = this.add.sprite(275,30,'die');
                                        scream7.inputEnabled=true;
                                        scream7.events.onInputOver.add(hovernext, this);
                                        scream7.events.onInputOut.add(hoverOutnext, this);
                                        scream7.events.onInputDown.add(clickdie, this);
                                    
                                    scream8 = this.add.sprite(190,904,'die');
                                        scream8.inputEnabled=true;
                                        scream8.events.onInputOver.add(hovernext, this);
                                        scream8.events.onInputOut.add(hoverOutnext, this);
                                        scream8.events.onInputDown.add(clickdie, this);
                                
                                    scream9 = this.add.sprite(640,411,'die');
                                        scream9.inputEnabled=true;
                                        scream9.events.onInputOver.add(hovernext, this);
                                        scream9.events.onInputOut.add(hoverOutnext, this);
                                        scream9.events.onInputDown.add(clickdie, this);
                                    
                                    scream10 = this.add.sprite(793,793,'die');
                                        scream10.inputEnabled=true;
                                        scream10.events.onInputOver.add(hovernext, this);
                                        scream10.events.onInputOut.add(hoverOutnext, this);
                                        scream10.events.onInputDown.add(clickdie, this);
                                    
                                    scream11 = this.add.sprite(666,55,'die');
                                        scream11.inputEnabled=true;
                                        scream11.events.onInputOver.add(hovernext, this);
                                        scream11.events.onInputOut.add(hoverOutnext, this);
                                        scream11.events.onInputDown.add(clickdie, this);
                                    
                                    scream12 = this.add.sprite(1105,299,'die');
                                        scream12.inputEnabled=true;
                                        scream12.events.onInputOver.add(hovernext, this);
                                        scream12.events.onInputOut.add(hoverOutnext, this);
                                        scream12.events.onInputDown.add(clickdie, this);
                                    
                                    scream13 = this.add.sprite(333,166,'die');
                                        scream13.inputEnabled=true;
                                        scream13.events.onInputOver.add(hovernext, this);
                                        scream13.events.onInputOut.add(hoverOutnext, this);
                                        scream13.events.onInputDown.add(clickdie, this);
                                    
                                    scream14 = this.add.sprite(512,256,'die');
                                        scream14.inputEnabled=true;
                                        scream14.events.onInputOver.add(hovernext, this);
                                        scream14.events.onInputOut.add(hoverOutnext, this);
                                        scream14.events.onInputDown.add(clickdie, this);
                                    
                                    scream15 = this.add.sprite(111,444,'die');
                                        scream15.inputEnabled=true;
                                        scream15.events.onInputOver.add(hovernext, this);
                                        scream15.events.onInputOut.add(hoverOutnext, this);
                                        scream15.events.onInputDown.add(clickdie, this);
                                    
                                    scream16 = this.add.sprite(140,213,'die');
                                        scream16.inputEnabled=true;
                                        scream16.events.onInputOver.add(hovernext, this);
                                        scream16.events.onInputOut.add(hoverOutnext, this);
                                        scream16.events.onInputDown.add(clickdie, this);
                                
                                    scream17 = this.add.sprite(320,710,'die');
                                        scream17.inputEnabled=true;
                                        scream17.events.onInputOver.add(hovernext, this);
                                        scream17.events.onInputOut.add(hoverOutnext, this);
                                        scream17.events.onInputDown.add(clickdie, this);
                                    
                                    scream18 = this.add.sprite(599,714,'die');
                                        scream18.inputEnabled=true;
                                        scream18.events.onInputOver.add(hovernext, this);
                                        scream18.events.onInputOut.add(hoverOutnext, this);
                                        scream18.events.onInputDown.add(clickdie, this);
                                    
                                    scream19 = this.add.sprite(425,571,'die');
                                        scream19.inputEnabled=true;
                                        scream19.events.onInputOver.add(hovernext, this);
                                        scream19.events.onInputOut.add(hoverOutnext, this);
                                        scream19.events.onInputDown.add(clickdie, this);
                                    
                                    scream20 = this.add.sprite(850,900,'die');
                                        scream20.inputEnabled=true;
                                        scream20.events.onInputOver.add(hovernext, this);
                                        scream20.events.onInputOut.add(hoverOutnext, this);
                                        scream20.events.onInputDown.add(clickdie, this);
                                    
                                    scream21 = this.add.sprite(388,950,'die');
                                        scream21.inputEnabled=true;
                                        scream21.events.onInputOver.add(hovernext, this);
                                        scream21.events.onInputOut.add(hoverOutnext, this);
                                        scream21.events.onInputDown.add(clickdie, this);
                                    
                                    scream22 = this.add.sprite(180,112,'die');
                                        scream22.inputEnabled=true;
                                        scream22.events.onInputOver.add(hovernext, this);
                                        scream22.events.onInputOut.add(hoverOutnext, this);
                                        scream22.events.onInputDown.add(clickdie, this);
                                    
                                    scream23 = this.add.sprite(373,221,'die');
                                        scream23.inputEnabled=true;
                                        scream23.events.onInputOver.add(hovernext, this);
                                        scream23.events.onInputOut.add(hoverOutnext, this);
                                        scream23.events.onInputDown.add(clickdie, this);
                                    
                                    scream24 = this.add.sprite(90,904,'die');
                                        scream24.inputEnabled=true;
                                        scream24.events.onInputOver.add(hovernext, this);
                                        scream24.events.onInputOut.add(hoverOutnext, this);
                                        scream24.events.onInputDown.add(clickdie, this);
                                
                                    scream25 = this.add.sprite(640,411,'die');
                                        scream25.inputEnabled=true;
                                        scream25.events.onInputOver.add(hovernext, this);
                                        scream25.events.onInputOut.add(hoverOutnext, this);
                                        scream25.events.onInputDown.add(clickdie, this);
                                    
                                    scream26 = this.add.sprite(293,493,'die');
                                        scream26.inputEnabled=true;
                                        scream26.events.onInputOver.add(hovernext, this);
                                        scream26.events.onInputOut.add(hoverOutnext, this);
                                        scream26.events.onInputDown.add(clickdie, this);
                                    
                                    scream27 = this.add.sprite(266,755,'die');
                                        scream27.inputEnabled=true;
                                        scream27.events.onInputOver.add(hovernext, this);
                                        scream27.events.onInputOut.add(hoverOutnext, this);
                                        scream27.events.onInputDown.add(clickdie, this);
                                    
                                    scream28 = this.add.sprite(405,1000,'die');
                                        scream28.inputEnabled=true;
                                        scream28.events.onInputOver.add(hovernext, this);
                                        scream28.events.onInputOut.add(hoverOutnext, this);
                                        scream28.events.onInputDown.add(clickdie, this);
                                    
                                    scream29 = this.add.sprite(733,900,'die');
                                        scream29.inputEnabled=true;
                                        scream29.events.onInputOver.add(hovernext, this);
                                        scream29.events.onInputOut.add(hoverOutnext, this);
                                        scream29.events.onInputDown.add(clickdie, this);
                                    
                                    scream30 = this.add.sprite(64,512,'die');
                                        scream30.inputEnabled=true;
                                        scream30.events.onInputOver.add(hovernext, this);
                                        scream30.events.onInputOut.add(hoverOutnext, this);
                                        scream30.events.onInputDown.add(clickdie, this);
                                    
                                    scream31 = this.add.sprite(333,999,'die');
                                        scream31.inputEnabled=true;
                                        scream31.events.onInputOver.add(hovernext, this);
                                        scream31.events.onInputOut.add(hoverOutnext, this);
                                        scream31.events.onInputDown.add(clickdie, this);
                                    
                                    scream32 = this.add.sprite(876,213,'die');
                                        scream32.inputEnabled=true;
                                        scream32.events.onInputOver.add(hovernext, this);
                                        scream32.events.onInputOut.add(hoverOutnext, this);
                                        scream32.events.onInputDown.add(clickdie, this);
                                
                                    scream33 = this.add.sprite(240,910,'die');
                                        scream33.inputEnabled=true;
                                        scream33.events.onInputOver.add(hovernext, this);
                                        scream33.events.onInputOut.add(hoverOutnext, this);
                                        scream33.events.onInputDown.add(clickdie, this);
                                    
                                    scream34 = this.add.sprite(140,638,'die');
                                        scream34.inputEnabled=true;
                                        scream34.events.onInputOver.add(hovernext, this);
                                        scream34.events.onInputOut.add(hoverOutnext, this);
                                        scream34.events.onInputDown.add(clickdie, this);
                                    
                                    scream35 = this.add.sprite(780,480,'die');
                                        scream35.inputEnabled=true;
                                        scream35.events.onInputOver.add(hovernext, this);
                                        scream35.events.onInputOut.add(hoverOutnext, this);
                                        scream35.events.onInputDown.add(clickdie, this);
                                    
                                    scream36 = this.add.sprite(930,360,'die');
                                        scream36.inputEnabled=true;
                                        scream36.events.onInputOver.add(hovernext, this);
                                        scream36.events.onInputOut.add(hoverOutnext, this);
                                        scream36.events.onInputDown.add(clickdie, this);
                                    
                                    scream37 = this.add.sprite(688,57,'die');
                                        scream37.inputEnabled=true;
                                        scream37.events.onInputOver.add(hovernext, this);
                                        scream37.events.onInputOut.add(hoverOutnext, this);
                                        scream37.events.onInputDown.add(clickdie, this);
                                    
                                    scream38 = this.add.sprite(205,145,'die');
                                        scream38.inputEnabled=true;
                                        scream38.events.onInputOver.add(hovernext, this);
                                        scream38.events.onInputOut.add(hoverOutnext, this);
                                        scream38.events.onInputDown.add(clickdie, this);
                                    
                                    scream39 = this.add.sprite(850,330,'die');
                                        scream39.inputEnabled=true;
                                        scream39.events.onInputOver.add(hovernext, this);
                                        scream39.events.onInputOut.add(hoverOutnext, this);
                                        scream39.events.onInputDown.add(clickdie, this);
                                    
                                    scream40 = this.add.sprite(913,925,'die');
                                        scream40.inputEnabled=true;
                                        scream40.events.onInputOver.add(hovernext, this);
                                        scream40.events.onInputOut.add(hoverOutnext, this);
                                        scream40.events.onInputDown.add(clickdie, this);
                                
                                    scream41 = this.add.sprite(736,834,'die');
                                        scream41.inputEnabled=true;
                                        scream41.events.onInputOver.add(hovernext, this);
                                        scream41.events.onInputOut.add(hoverOutnext, this);
                                        scream41.events.onInputDown.add(clickdie, this);
                                    
                                    scream42 = this.add.sprite(854,203,'die');
                                        scream42.inputEnabled=true;
                                        scream42.events.onInputOver.add(hovernext, this);
                                        scream42.events.onInputOut.add(hoverOutnext, this);
                                        scream42.events.onInputDown.add(clickdie, this);
                                    
                                    scream43 = this.add.sprite(437,143,'die');
                                        scream43.inputEnabled=true;
                                        scream43.events.onInputOver.add(hovernext, this);
                                        scream43.events.onInputOut.add(hoverOutnext, this);
                                        scream43.events.onInputDown.add(clickdie, this);
                                    
                                    scream44 = this.add.sprite(561,375,'die');
                                        scream44.inputEnabled=true;
                                        scream44.events.onInputOver.add(hovernext, this);
                                        scream44.events.onInputOut.add(hoverOutnext, this);
                                        scream44.events.onInputDown.add(clickdie, this);
                                    
                                    scream45 = this.add.sprite(386,386,'die');
                                        scream45.inputEnabled=true;
                                        scream45.events.onInputOver.add(hovernext, this);
                                        scream45.events.onInputOut.add(hoverOutnext, this);
                                        scream45.events.onInputDown.add(clickdie, this);
                                    
                                    scream46 = this.add.sprite(390,245,'die');
                                        scream46.inputEnabled=true;
                                        scream46.events.onInputOver.add(hovernext, this);
                                        scream46.events.onInputOut.add(hoverOutnext, this);
                                        scream46.events.onInputDown.add(clickdie, this);
                                        
                                    scream47 = this.add.sprite(136,582,'die');
                                        scream47.inputEnabled=true;
                                        scream47.events.onInputOver.add(hovernext, this);
                                        scream47.events.onInputOut.add(hoverOutnext, this);
                                        scream47.events.onInputDown.add(clickdie, this);
                                    
                                    scream48 = this.add.sprite(746,378,'die');
                                        scream48.inputEnabled=true;
                                        scream48.events.onInputOver.add(hovernext, this);
                                        scream48.events.onInputOut.add(hoverOutnext, this);
                                        scream48.events.onInputDown.add(clickdie, this);
                                    
                                    scream49 = this.add.sprite(642,263,'die');
                                        scream49.inputEnabled=true;
                                        scream49.events.onInputOver.add(hovernext, this);
                                        scream49.events.onInputOut.add(hoverOutnext, this);
                                        scream49.events.onInputDown.add(clickdie, this);
                                    
                                    scream50 = this.add.sprite(957,836,'die');
                                        scream50.inputEnabled=true;
                                        scream50.events.onInputOver.add(hovernext, this);
                                        scream50.events.onInputOut.add(hoverOutnext, this);
                                        scream50.events.onInputDown.add(clickdie, this);
                                    
                                    scream51 = this.add.sprite(85,836,'die');
                                        scream51.inputEnabled=true;
                                        scream51.events.onInputOver.add(hovernext, this);
                                        scream51.events.onInputOut.add(hoverOutnext, this);
                                        scream51.events.onInputDown.add(clickdie, this);
                                    
                                    scream52 = this.add.sprite(864,83,'die');
                                        scream52.inputEnabled=true;
                                        scream52.events.onInputOver.add(hovernext, this);
                                        scream52.events.onInputOut.add(hoverOutnext, this);
                                        scream52.events.onInputDown.add(clickdie, this);
                                    
                                    scream53 = this.add.sprite(864,56,'die');
                                        scream53.inputEnabled=true;
                                        scream53.events.onInputOver.add(hovernext, this);
                                        scream53.events.onInputOut.add(hoverOutnext, this);
                                        scream53.events.onInputDown.add(clickdie, this);
                                    
                                    scream54 = this.add.sprite(468,468,'die');
                                        scream54.inputEnabled=true;
                                        scream54.events.onInputOver.add(hovernext, this);
                                        scream54.events.onInputOut.add(hoverOutnext, this);
                                        scream54.events.onInputDown.add(clickdie, this);
                                    
                                    scream55 = this.add.sprite(435,129,'die');
                                        scream55.inputEnabled=true;
                                        scream55.events.onInputOver.add(hovernext, this);
                                        scream55.events.onInputOut.add(hoverOutnext, this);
                                        scream55.events.onInputDown.add(clickdie, this);
                                    
                                    scream56 = this.add.sprite(217,223,'die');
                                        scream56.inputEnabled=true;
                                        scream56.events.onInputOver.add(hovernext, this);
                                        scream56.events.onInputOut.add(hoverOutnext, this);
                                        scream56.events.onInputDown.add(clickdie, this);
                                
                                    scream57 = this.add.sprite(456,542,'die');
                                        scream57.inputEnabled=true;
                                        scream57.events.onInputOver.add(hovernext, this);
                                        scream57.events.onInputOut.add(hoverOutnext, this);
                                        scream57.events.onInputDown.add(clickdie, this);
                                    
                                    scream58 = this.add.sprite(348,45,'die');
                                        scream58.inputEnabled=true;
                                        scream58.events.onInputOver.add(hovernext, this);
                                        scream58.events.onInputOut.add(hoverOutnext, this);
                                        scream58.events.onInputDown.add(clickdie, this);
                                    
                                    scream59 = this.add.sprite(87,298,'die');
                                        scream59.inputEnabled=true;
                                        scream59.events.onInputOver.add(hovernext, this);
                                        scream59.events.onInputOut.add(hoverOutnext, this);
                                        scream59.events.onInputDown.add(clickdie, this);
                                    
                                    scream60 = this.add.sprite(428,849,'die');
                                        scream60.inputEnabled=true;
                                        scream60.events.onInputOver.add(hovernext, this);
                                        scream60.events.onInputOut.add(hoverOutnext, this);
                                        scream60.events.onInputDown.add(clickdie, this);
                                    
                                    scream61 = this.add.sprite(952,285,'die');
                                        scream61.inputEnabled=true;
                                        scream61.events.onInputOver.add(hovernext, this);
                                        scream61.events.onInputOut.add(hoverOutnext, this);
                                        scream61.events.onInputDown.add(clickdie, this);
                                    
                                    scream62 = this.add.sprite(375,735,'die');
                                        scream62.inputEnabled=true;
                                        scream62.events.onInputOver.add(hovernext, this);
                                        scream62.events.onInputOut.add(hoverOutnext, this);
                                        scream62.events.onInputDown.add(clickdie, this);
                                    
                                    scream63 = this.add.sprite(592,357,'die');
                                        scream63.inputEnabled=true;
                                        scream63.events.onInputOver.add(hovernext, this);
                                        scream63.events.onInputOut.add(hoverOutnext, this);
                                        scream63.events.onInputDown.add(clickdie, this);
                                    
                                    scream64 = this.add.sprite(386,530,'die');
                                        scream64.inputEnabled=true;
                                        scream64.events.onInputOver.add(hovernext, this);
                                        scream64.events.onInputOut.add(hoverOutnext, this);
                                        scream64.events.onInputDown.add(clickdie, this);
                                
                            }, this)
                                
                        }
                            
                        }, this)
                
                    }, this)
            
                    this.time.events.add(4000, function() {
                    
                        txtlayer = this.add.sprite(15, 345, 'f2txtlyr2');
                        txtlayer.alpha=0;
                        
                        if(counterflat2==13)
                            txt = this.add.sprite(25, 385, 'vttxt8');
                        
                        else if(counterflat2==14)
                            txt = this.add.sprite(25, 385, 'vttxt9');
                        
                        else if(counterflat2==15)
                            txt = this.add.sprite(25, 385, 'vttxt10');
                        
                        else if(counterflat2==16)
                            txt = this.add.sprite(25, 385, 'vttxt11');
                        
                        else if(counterflat2==17)
                            txt = this.add.sprite(25, 385, 'vttxt12');
                        
                        else if(counterflat2==18)
                            txt = this.add.sprite(25, 385, 'vttxt13');
                        
                        else if(counterflat2==19)
                            txt = this.add.sprite(25, 385, 'vttxt14');
                        
                        else if(counterflat2==20)
                            txt = this.add.sprite(25, 385, 'vttxt15');
                        
                        if(counterflat2==21){
                            
                            txt = this.add.sprite(25, 385, 'vttxt16');
                            txt.alpha=0;
                            this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            
                            this.time.events.add(5000, function() {
                        
                                this.add.tween(txtlayer).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(txt).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(kadar2).to({alpha: 0}, 2000, Phaser.Easing.Linear.None, true);
                                this.add.tween(viky).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                this.add.tween(click).to({alpha: 0}, 3000, Phaser.Easing.Linear.None, true);
                                
                                            
                             }, this)
                                    
                             
                        }
                                                
                        else {
                            
                                if(counterflat2==14){
                                    
                                    scream1 = this.add.sprite(840,110,'scream');
                                        scream1.inputEnabled=true;
                                        scream1.events.onInputOver.add(hovernext, this);
                                        scream1.events.onInputOut.add(hoverOutnext, this);
                                        scream1.events.onInputDown.add(clickscream, this);
                                    
                                }
                            
                                if(counterflat2==15){
                                    
                                    scream2 = this.add.sprite(690,800,'scream');
                                        scream2.inputEnabled=true;
                                        scream2.events.onInputOver.add(hovernext, this);
                                        scream2.events.onInputOut.add(hoverOutnext, this);
                                        scream2.events.onInputDown.add(clickscream, this);
                                    
                                }
                                
                                else if(counterflat2==16){
                                    
                                    scream3 = this.add.sprite(25,180,'scream');
                                        scream3.inputEnabled=true;
                                        scream3.events.onInputOver.add(hovernext, this);
                                        scream3.events.onInputOut.add(hoverOutnext, this);
                                        scream3.events.onInputDown.add(clickscream, this);
                                    
                                    scream4 = this.add.sprite(990,490,'scream');
                                        scream4.inputEnabled=true;
                                        scream4.events.onInputOver.add(hovernext, this);
                                        scream4.events.onInputOut.add(hoverOutnext, this);
                                        scream4.events.onInputDown.add(clickscream, this);
                                }
                            
                                else if(counterflat2==17){
                                    
                                    scream5 = this.add.sprite(888,350,'scream');
                                        scream5.inputEnabled=true;
                                        scream5.events.onInputOver.add(hovernext, this);
                                        scream5.events.onInputOut.add(hoverOutnext, this);
                                        scream5.events.onInputDown.add(clickscream, this);
                                    
                                    scream6 = this.add.sprite(780,512,'scream');
                                        scream6.inputEnabled=true;
                                        scream6.events.onInputOver.add(hovernext, this);
                                        scream6.events.onInputOut.add(hoverOutnext, this);
                                        scream6.events.onInputDown.add(clickscream, this);
                                    
                                    scream7 = this.add.sprite(275,30,'scream');
                                        scream7.inputEnabled=true;
                                        scream7.events.onInputOver.add(hovernext, this);
                                        scream7.events.onInputOut.add(hoverOutnext, this);
                                        scream7.events.onInputDown.add(clickscream, this);
                                    
                                    scream8 = this.add.sprite(190,904,'scream');
                                        scream8.inputEnabled=true;
                                        scream8.events.onInputOver.add(hovernext, this);
                                        scream8.events.onInputOut.add(hoverOutnext, this);
                                        scream8.events.onInputDown.add(clickscream, this);
                                }
                            
                            else if(counterflat2==18){
                                    
                                    scream9 = this.add.sprite(640,411,'scream');
                                        scream9.inputEnabled=true;
                                        scream9.events.onInputOver.add(hovernext, this);
                                        scream9.events.onInputOut.add(hoverOutnext, this);
                                        scream9.events.onInputDown.add(clickscream, this);
                                    
                                    scream10 = this.add.sprite(793,793,'scream');
                                        scream10.inputEnabled=true;
                                        scream10.events.onInputOver.add(hovernext, this);
                                        scream10.events.onInputOut.add(hoverOutnext, this);
                                        scream10.events.onInputDown.add(clickscream, this);
                                    
                                    scream11 = this.add.sprite(666,55,'scream');
                                        scream11.inputEnabled=true;
                                        scream11.events.onInputOver.add(hovernext, this);
                                        scream11.events.onInputOut.add(hoverOutnext, this);
                                        scream11.events.onInputDown.add(clickscream, this);
                                    
                                    scream12 = this.add.sprite(1105,299,'scream');
                                        scream12.inputEnabled=true;
                                        scream12.events.onInputOver.add(hovernext, this);
                                        scream12.events.onInputOut.add(hoverOutnext, this);
                                        scream12.events.onInputDown.add(clickscream, this);
                                    
                                    scream13 = this.add.sprite(333,166,'scream');
                                        scream13.inputEnabled=true;
                                        scream13.events.onInputOver.add(hovernext, this);
                                        scream13.events.onInputOut.add(hoverOutnext, this);
                                        scream13.events.onInputDown.add(clickscream, this);
                                    
                                    scream14 = this.add.sprite(512,256,'scream');
                                        scream14.inputEnabled=true;
                                        scream14.events.onInputOver.add(hovernext, this);
                                        scream14.events.onInputOut.add(hoverOutnext, this);
                                        scream14.events.onInputDown.add(clickscream, this);
                                    
                                    scream15 = this.add.sprite(111,444,'scream');
                                        scream15.inputEnabled=true;
                                        scream15.events.onInputOver.add(hovernext, this);
                                        scream15.events.onInputOut.add(hoverOutnext, this);
                                        scream15.events.onInputDown.add(clickscream, this);
                                    
                                    scream16 = this.add.sprite(140,213,'scream');
                                        scream16.inputEnabled=true;
                                        scream16.events.onInputOver.add(hovernext, this);
                                        scream16.events.onInputOut.add(hoverOutnext, this);
                                        scream16.events.onInputDown.add(clickscream, this);
                                }
                            
                            else if(counterflat2==19){
                                    
                                    scream17 = this.add.sprite(320,710,'scream');
                                        scream17.inputEnabled=true;
                                        scream17.events.onInputOver.add(hovernext, this);
                                        scream17.events.onInputOut.add(hoverOutnext, this);
                                        scream17.events.onInputDown.add(clickscream, this);
                                    
                                    scream18 = this.add.sprite(599,714,'scream');
                                        scream18.inputEnabled=true;
                                        scream18.events.onInputOver.add(hovernext, this);
                                        scream18.events.onInputOut.add(hoverOutnext, this);
                                        scream18.events.onInputDown.add(clickscream, this);
                                    
                                    scream19 = this.add.sprite(425,571,'scream');
                                        scream19.inputEnabled=true;
                                        scream19.events.onInputOver.add(hovernext, this);
                                        scream19.events.onInputOut.add(hoverOutnext, this);
                                        scream19.events.onInputDown.add(clickscream, this);
                                    
                                    scream20 = this.add.sprite(850,900,'scream');
                                        scream20.inputEnabled=true;
                                        scream20.events.onInputOver.add(hovernext, this);
                                        scream20.events.onInputOut.add(hoverOutnext, this);
                                        scream20.events.onInputDown.add(clickscream, this);
                                    
                                    scream21 = this.add.sprite(388,950,'scream');
                                        scream21.inputEnabled=true;
                                        scream21.events.onInputOver.add(hovernext, this);
                                        scream21.events.onInputOut.add(hoverOutnext, this);
                                        scream21.events.onInputDown.add(clickscream, this);
                                    
                                    scream22 = this.add.sprite(180,112,'scream');
                                        scream22.inputEnabled=true;
                                        scream22.events.onInputOver.add(hovernext, this);
                                        scream22.events.onInputOut.add(hoverOutnext, this);
                                        scream22.events.onInputDown.add(clickscream, this);
                                    
                                    scream23 = this.add.sprite(373,221,'scream');
                                        scream23.inputEnabled=true;
                                        scream23.events.onInputOver.add(hovernext, this);
                                        scream23.events.onInputOut.add(hoverOutnext, this);
                                        scream23.events.onInputDown.add(clickscream, this);
                                    
                                    scream24 = this.add.sprite(90,904,'scream');
                                        scream24.inputEnabled=true;
                                        scream24.events.onInputOver.add(hovernext, this);
                                        scream24.events.onInputOut.add(hoverOutnext, this);
                                        scream24.events.onInputDown.add(clickscream, this);
                                
                                    scream25 = this.add.sprite(640,411,'scream');
                                        scream25.inputEnabled=true;
                                        scream25.events.onInputOver.add(hovernext, this);
                                        scream25.events.onInputOut.add(hoverOutnext, this);
                                        scream25.events.onInputDown.add(clickscream, this);
                                    
                                    scream26 = this.add.sprite(293,493,'scream');
                                        scream26.inputEnabled=true;
                                        scream26.events.onInputOver.add(hovernext, this);
                                        scream26.events.onInputOut.add(hoverOutnext, this);
                                        scream26.events.onInputDown.add(clickscream, this);
                                    
                                    scream27 = this.add.sprite(266,755,'scream');
                                        scream27.inputEnabled=true;
                                        scream27.events.onInputOver.add(hovernext, this);
                                        scream27.events.onInputOut.add(hoverOutnext, this);
                                        scream27.events.onInputDown.add(clickscream, this);
                                    
                                    scream28 = this.add.sprite(405,1000,'scream');
                                        scream28.inputEnabled=true;
                                        scream28.events.onInputOver.add(hovernext, this);
                                        scream28.events.onInputOut.add(hoverOutnext, this);
                                        scream28.events.onInputDown.add(clickscream, this);
                                    
                                    scream29 = this.add.sprite(733,900,'scream');
                                        scream29.inputEnabled=true;
                                        scream29.events.onInputOver.add(hovernext, this);
                                        scream29.events.onInputOut.add(hoverOutnext, this);
                                        scream29.events.onInputDown.add(clickscream, this);
                                    
                                    scream30 = this.add.sprite(64,512,'scream');
                                        scream30.inputEnabled=true;
                                        scream30.events.onInputOver.add(hovernext, this);
                                        scream30.events.onInputOut.add(hoverOutnext, this);
                                        scream30.events.onInputDown.add(clickscream, this);
                                    
                                    scream31 = this.add.sprite(333,999,'scream');
                                        scream31.inputEnabled=true;
                                        scream31.events.onInputOver.add(hovernext, this);
                                        scream31.events.onInputOut.add(hoverOutnext, this);
                                        scream31.events.onInputDown.add(clickscream, this);
                                    
                                    scream32 = this.add.sprite(876,213,'scream');
                                        scream32.inputEnabled=true;
                                        scream32.events.onInputOver.add(hovernext, this);
                                        scream32.events.onInputOut.add(hoverOutnext, this);
                                        scream32.events.onInputDown.add(clickscream, this);
                                }
                            
                            else if(counterflat2==20){
                                    
                                    scream33 = this.add.sprite(240,910,'scream');
                                        scream33.inputEnabled=true;
                                        scream33.events.onInputOver.add(hovernext, this);
                                        scream33.events.onInputOut.add(hoverOutnext, this);
                                        scream33.events.onInputDown.add(clickscream, this);
                                    
                                    scream34 = this.add.sprite(140,638,'scream');
                                        scream34.inputEnabled=true;
                                        scream34.events.onInputOver.add(hovernext, this);
                                        scream34.events.onInputOut.add(hoverOutnext, this);
                                        scream34.events.onInputDown.add(clickscream, this);
                                    
                                    scream35 = this.add.sprite(780,480,'scream');
                                        scream35.inputEnabled=true;
                                        scream35.events.onInputOver.add(hovernext, this);
                                        scream35.events.onInputOut.add(hoverOutnext, this);
                                        scream35.events.onInputDown.add(clickscream, this);
                                    
                                    scream36 = this.add.sprite(930,360,'scream');
                                        scream36.inputEnabled=true;
                                        scream36.events.onInputOver.add(hovernext, this);
                                        scream36.events.onInputOut.add(hoverOutnext, this);
                                        scream36.events.onInputDown.add(clickscream, this);
                                    
                                    scream37 = this.add.sprite(688,57,'scream');
                                        scream37.inputEnabled=true;
                                        scream37.events.onInputOver.add(hovernext, this);
                                        scream37.events.onInputOut.add(hoverOutnext, this);
                                        scream37.events.onInputDown.add(clickscream, this);
                                    
                                    scream38 = this.add.sprite(205,145,'scream');
                                        scream38.inputEnabled=true;
                                        scream38.events.onInputOver.add(hovernext, this);
                                        scream38.events.onInputOut.add(hoverOutnext, this);
                                        scream38.events.onInputDown.add(clickscream, this);
                                    
                                    scream39 = this.add.sprite(850,330,'scream');
                                        scream39.inputEnabled=true;
                                        scream39.events.onInputOver.add(hovernext, this);
                                        scream39.events.onInputOut.add(hoverOutnext, this);
                                        scream39.events.onInputDown.add(clickscream, this);
                                    
                                    scream40 = this.add.sprite(913,925,'scream');
                                        scream40.inputEnabled=true;
                                        scream40.events.onInputOver.add(hovernext, this);
                                        scream40.events.onInputOut.add(hoverOutnext, this);
                                        scream40.events.onInputDown.add(clickscream, this);
                                
                                    scream41 = this.add.sprite(736,834,'scream');
                                        scream41.inputEnabled=true;
                                        scream41.events.onInputOver.add(hovernext, this);
                                        scream41.events.onInputOut.add(hoverOutnext, this);
                                        scream41.events.onInputDown.add(clickscream, this);
                                    
                                    scream42 = this.add.sprite(854,203,'scream');
                                        scream42.inputEnabled=true;
                                        scream42.events.onInputOver.add(hovernext, this);
                                        scream42.events.onInputOut.add(hoverOutnext, this);
                                        scream42.events.onInputDown.add(clickscream, this);
                                    
                                    scream43 = this.add.sprite(437,143,'scream');
                                        scream43.inputEnabled=true;
                                        scream43.events.onInputOver.add(hovernext, this);
                                        scream43.events.onInputOut.add(hoverOutnext, this);
                                        scream43.events.onInputDown.add(clickscream, this);
                                    
                                    scream44 = this.add.sprite(561,375,'scream');
                                        scream44.inputEnabled=true;
                                        scream44.events.onInputOver.add(hovernext, this);
                                        scream44.events.onInputOut.add(hoverOutnext, this);
                                        scream44.events.onInputDown.add(clickscream, this);
                                    
                                    scream45 = this.add.sprite(386,386,'scream');
                                        scream45.inputEnabled=true;
                                        scream45.events.onInputOver.add(hovernext, this);
                                        scream45.events.onInputOut.add(hoverOutnext, this);
                                        scream45.events.onInputDown.add(clickscream, this);
                                    
                                    scream46 = this.add.sprite(390,245,'scream');
                                        scream46.inputEnabled=true;
                                        scream46.events.onInputOver.add(hovernext, this);
                                        scream46.events.onInputOut.add(hoverOutnext, this);
                                        scream46.events.onInputDown.add(clickscream, this);
                                        
                                    scream47 = this.add.sprite(136,582,'scream');
                                        scream47.inputEnabled=true;
                                        scream47.events.onInputOver.add(hovernext, this);
                                        scream47.events.onInputOut.add(hoverOutnext, this);
                                        scream47.events.onInputDown.add(clickscream, this);
                                    
                                    scream48 = this.add.sprite(746,378,'scream');
                                        scream48.inputEnabled=true;
                                        scream48.events.onInputOver.add(hovernext, this);
                                        scream48.events.onInputOut.add(hoverOutnext, this);
                                        scream48.events.onInputDown.add(clickscream, this);
                                    
                                    scream49 = this.add.sprite(642,263,'scream');
                                        scream49.inputEnabled=true;
                                        scream49.events.onInputOver.add(hovernext, this);
                                        scream49.events.onInputOut.add(hoverOutnext, this);
                                        scream49.events.onInputDown.add(clickscream, this);
                                    
                                    scream50 = this.add.sprite(957,836,'scream');
                                        scream50.inputEnabled=true;
                                        scream50.events.onInputOver.add(hovernext, this);
                                        scream50.events.onInputOut.add(hoverOutnext, this);
                                        scream50.events.onInputDown.add(clickscream, this);
                                    
                                    scream51 = this.add.sprite(85,836,'scream');
                                        scream51.inputEnabled=true;
                                        scream51.events.onInputOver.add(hovernext, this);
                                        scream51.events.onInputOut.add(hoverOutnext, this);
                                        scream51.events.onInputDown.add(clickscream, this);
                                    
                                    scream52 = this.add.sprite(864,83,'scream');
                                        scream52.inputEnabled=true;
                                        scream52.events.onInputOver.add(hovernext, this);
                                        scream52.events.onInputOut.add(hoverOutnext, this);
                                        scream52.events.onInputDown.add(clickscream, this);
                                    
                                    scream53 = this.add.sprite(864,56,'scream');
                                        scream53.inputEnabled=true;
                                        scream53.events.onInputOver.add(hovernext, this);
                                        scream53.events.onInputOut.add(hoverOutnext, this);
                                        scream53.events.onInputDown.add(clickscream, this);
                                    
                                    scream54 = this.add.sprite(468,468,'scream');
                                        scream54.inputEnabled=true;
                                        scream54.events.onInputOver.add(hovernext, this);
                                        scream54.events.onInputOut.add(hoverOutnext, this);
                                        scream54.events.onInputDown.add(clickscream, this);
                                    
                                    scream55 = this.add.sprite(435,129,'scream');
                                        scream55.inputEnabled=true;
                                        scream55.events.onInputOver.add(hovernext, this);
                                        scream55.events.onInputOut.add(hoverOutnext, this);
                                        scream55.events.onInputDown.add(clickscream, this);
                                    
                                    scream56 = this.add.sprite(217,223,'scream');
                                        scream56.inputEnabled=true;
                                        scream56.events.onInputOver.add(hovernext, this);
                                        scream56.events.onInputOut.add(hoverOutnext, this);
                                        scream56.events.onInputDown.add(clickscream, this);
                                
                                    scream57 = this.add.sprite(456,542,'scream');
                                        scream57.inputEnabled=true;
                                        scream57.events.onInputOver.add(hovernext, this);
                                        scream57.events.onInputOut.add(hoverOutnext, this);
                                        scream57.events.onInputDown.add(clickscream, this);
                                    
                                    scream58 = this.add.sprite(348,45,'scream');
                                        scream58.inputEnabled=true;
                                        scream58.events.onInputOver.add(hovernext, this);
                                        scream58.events.onInputOut.add(hoverOutnext, this);
                                        scream58.events.onInputDown.add(clickscream, this);
                                    
                                    scream59 = this.add.sprite(87,298,'scream');
                                        scream59.inputEnabled=true;
                                        scream59.events.onInputOver.add(hovernext, this);
                                        scream59.events.onInputOut.add(hoverOutnext, this);
                                        scream59.events.onInputDown.add(clickscream, this);
                                    
                                    scream60 = this.add.sprite(428,849,'scream');
                                        scream60.inputEnabled=true;
                                        scream60.events.onInputOver.add(hovernext, this);
                                        scream60.events.onInputOut.add(hoverOutnext, this);
                                        scream60.events.onInputDown.add(clickscream, this);
                                    
                                    scream61 = this.add.sprite(952,285,'scream');
                                        scream61.inputEnabled=true;
                                        scream61.events.onInputOver.add(hovernext, this);
                                        scream61.events.onInputOut.add(hoverOutnext, this);
                                        scream61.events.onInputDown.add(clickscream, this);
                                    
                                    scream62 = this.add.sprite(375,735,'scream');
                                        scream62.inputEnabled=true;
                                        scream62.events.onInputOver.add(hovernext, this);
                                        scream62.events.onInputOut.add(hoverOutnext, this);
                                        scream62.events.onInputDown.add(clickscream, this);
                                    
                                    scream63 = this.add.sprite(592,357,'scream');
                                        scream63.inputEnabled=true;
                                        scream63.events.onInputOver.add(hovernext, this);
                                        scream63.events.onInputOut.add(hoverOutnext, this);
                                        scream63.events.onInputDown.add(clickscream, this);
                                    
                                    scream64 = this.add.sprite(386,530,'scream');
                                        scream64.inputEnabled=true;
                                        scream64.events.onInputOver.add(hovernext, this);
                                        scream64.events.onInputOut.add(hoverOutnext, this);
                                        scream64.events.onInputDown.add(clickscream, this);
                                
                            
                                }
                            
                            
                            
                            
                                                                
                            txt.alpha=0;
                        
                            click = this.add.sprite(500, 650, 'nod');
                            click.alpha=0;
                            
                            this.add.tween(txtlayer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                            this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
                                
                            click.inputEnabled=true;
                                                    
                            click.events.onInputOver.add(hovernext, this);
                            click.events.onInputOut.add(hoverOutnext, this);
                            click.events.onInputDown.add(clicktxtnod, this);
                            
                        }
                    
                    }, this)
        }
    
    else{
            
        txt.alpha = 0;
        click = this.add.sprite(txt.x + txt.width + 20, txt.y + txt.height - 30, 'click');
        click.alpha=0;
        this.add.tween(txt).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        
        click.inputEnabled=true;
        click.events.onInputOver.add(hovernext, this);
        click.events.onInputOut.add(hoverOutnext, this);
        click.events.onInputDown.add(clicktxtnod, this);
                     
        }

}
///////////////////////////////////////////////////////////////////////////////////////



