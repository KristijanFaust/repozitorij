var game = {};

game.beginning = function (the_game) {};

//konfig rezolucije
game.beginning.prototype = {
    init: function () {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.minWidth = 1024;
		this.scale.minHeight = 768;
        this.scale.maxWidth = 1280;
        this.scale.maxHeight = 1024;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
    },
    
    preload: function () {
         
    },
    
    create: function () {
        this.stage.disableVisibilityChange = false;
        this.input.maxPointers = 1;
        //sljedeci state
        this.state.start("Loading"); 
    }
};
