var input_mode_misli = false;
var input_mode_pricanje1 = false;
var input_mode_pricanje2 = false;
var input_mode_pricanje3 = false;

var chkobject = false;
var chklik1 = false;
var chklik2 = false;
var chklik3 = false;

var metrocounterlik1 = 0;
var metrocounterlik2 = 0;
var metrocounterlik3 = 0;


var spaceKey; //Varijabla za SPACEBAR
var spacecount=0;//Brojac udarca na SPACEBAR za tonove

game.metro = function (the_game) {};

game.metro.prototype = {
          
    create: function () {
        
    spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR); //Dodjeljivanje signala
        
//Dodavanje zvukova://////////////////////////////////////////////////////////
        
    hover_sound = this.add.audio("hoover");
    click_sound = this.add.audio("click");
    metrovozi = this.add.audio("metromoving");
    metrostaje = this.add.audio("metrostoping");
    metrostoji = this.add.audio("metronoise");
    theme = this.add.audio("basstheme");
        
    D = this.add.audio("D");
    Dm = this.add.audio("Dm");
    C = this.add.audio("C");
    Dsh = this.add.audio("Dsh");
        
//////////////////////////////////////////////////////////////////////////////

        
//Dodavanje pocetnih slika////////////////////////////////////////////////////
   
    metro = this.add.sprite(0, 0, 'metro');
        metro.alpha=0;
        
//Pokretanje/////////////////////////////////////////////////////////////////////////////////
        
    metrovozi.onDecoded.add(fademetro, this); //Bez onDecoded ne radi, vjerovatno jer ga ne stigne ucitati...
            
/////////////////////////////////////////////////////////////////////////////////////////////
        
    
    },
    
    update: function () {
    
//Provjere za input SPACEBAR tipke za svaki frame u igri/////////////////////////////////////
        
    spaceKey.onDown.add(noclickmetro, this);
    spaceKey.onDown.add(nextmisli, this);
    spaceKey.onDown.add(lik1nxt, this);
    spaceKey.onDown.add(lik2nxt, this);
    spaceKey.onDown.add(lik3nxt, this);
        
    if (chkobject && chklik1 && chklik2){
        
        chkobject = false;
        chklik1 = false;
        chklik2 = false;
        
        this.time.events.remove(zamracenje1);
        this.time.events.remove(zamracenje2);
        
        vrata.inputEnabled=false;
        novine.inputEnabled=false;
        nigdje.inputEnabled=false;
        lik1.inputEnabled=false;
        lik2.inputEnabled=false;
        
        this.time.events.add(5000, function() {
            this.add.tween(metro).to( { alpha: 0 }, 10000, Phaser.Easing.Linear.None, true );
        }, this)
        
        metrovozi.fadeOut(2000);
        metrostaje.fadeIn(2000);
        
        this.time.events.add(18000, function() {
            
            theme.fadeIn(2000, true);
            
            metrostoji.fadeIn(500,true);
            
            metro2 = this.add.sprite(0, 0, 'metroizlaz');
            metro2.alpha=0;
            
            lik3 = this.add.sprite(644, 293, 'lik3');
            lik3.alpha=0;
            
            this.add.tween(metro2).to( { alpha: 1 }, 5000, Phaser.Easing.Linear.None, true );
            
            this.time.events.add(5000, function() {
             
                this.add.tween(lik3).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true );
                
                this.time.events.add(1000, function() {
                    
                    txtl3 = this.add.sprite(55, 155, 'mtxtl3');
                    txtl3.alpha = 0;
                    txtlm = this.add.sprite(10, 795, 'mtxtlm');
                    txtlm.alpha = 0;
    
                    txt = this.add.sprite(90, 245, 'ml3txt1');
                    txt.alpha = 0;
                    click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 20, 'click');
                    click.alpha=0;
    
                    this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
                    this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
                    this.add.tween(txtl3).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
                    this.add.tween(txtlm).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
        
                    click.inputEnabled=true;
                    input_mode_pricanje3=true;
    
                    click.events.onInputOver.add(hovernext, this);
                    click.events.onInputOut.add(hoverOutnext, this);
                    click.events.onInputDown.add(lik3nxt, this);
                    
                }, this)
                
            }, this)
            
        }, this)
        
        
    }
        
    if (chklik3){
        
        chklik3=false;
        
        click.inputEnabled=false;
        input_mode_pricanje3=false;
        
        vrata.inputEnabled=false;
        novine.inputEnabled=false;
        nigdje.inputEnabled=false;
        lik1.inputEnabled=false;
        lik2.inputEnabled=false;
        
        this.add.tween(metro2).to( { alpha: 0 }, 4000, Phaser.Easing.Linear.None, true );
        this.add.tween(lik3).to( { alpha: 0 }, 1000, Phaser.Easing.Linear.None, true );
                
        this.time.events.add(5000, function() {
            
            metrostoji.fadeOut(4000);
            txt = this.add.sprite(this.world.centerX, this.world.centerY, 'mout');
            txt.anchor.setTo(0.5, 0.5);
            txt.alpha = 0;
            this.add.tween(txt).to( { alpha: 1 }, 2000, Phaser.Easing.Linear.None, true );
            
            this.time.events.add(6000, function() {
                
                theme.fadeOut(4000);
                this.add.tween(txt).to( { alpha: 0 }, 2000, Phaser.Easing.Linear.None, true );
                
                this.time.events.add(4000, function() {
                    
                    input_mode_misli = false;
                    input_mode_pricanje1 = false;
                    input_mode_pricanje2 = false;
                    input_mode_pricanje3 = false;

                    chkobject = false;
                    chklik1 = false;
                    chklik2 = false;
                    chklik3 = false;

                    metrocounterlik1 = 0;
                    metrocounterlik2 = 0;
                    metrocounterlik3 = 0;
                    
                    this.state.start("Viky");
                    
                }, this)
                
            }, this)
            
        
        }, this)
   
    }
                
/////////////////////////////////////////////////////////////////////////////////////////////        
    },
};

//Funkcija za pokretanje:
function fademetro() {
    
    zamracenje1 = this.time.events.repeat(Phaser.Timer.SECOND * 17, 1000, mracenje1, this);
    zamracenje2 = this.time.events.repeat(Phaser.Timer.SECOND * 10, 1000, mracenje2, this);
    
    this.time.events.add(3000, function() {
        
        metrovozi.fadeIn(4000,true);
        
        this.time.events.add(4000, function() {
        
        this.add.tween(metro).to( { alpha: 1 }, 3000, Phaser.Easing.Linear.None, true );
            
        lik1 = this.add.sprite(5,440,'lik1');
        lik1.alpha=0;
        lik1.inputEnabled=true;
        lik1.events.onInputOver.add(hoverSve, this);
        lik1.events.onInputOut.add(hoverOutSve, this);
        lik1.events.onInputDown.add(clicklik1, this);
        
        lik2 = this.add.sprite(460,300,'lik2');
        lik2.alpha=0;
        lik2.inputEnabled=true;
        lik2.events.onInputOver.add(hoverSve, this);
        lik2.events.onInputOut.add(hoverOutSve, this);
        lik2.events.onInputDown.add(clicklik2, this);
    
        nigdje = this.add.sprite(580,180,'mnigdje');
        nigdje.alpha=0;
        nigdje.inputEnabled=true;
        nigdje.events.onInputOver.add(hoverSve, this);
        nigdje.events.onInputOut.add(hoverOutSve, this);
        nigdje.events.onInputDown.add(clicknigdje, this);
        
        novine = this.add.sprite(840,910,'mnovine');
        novine.alpha=0;
        novine.inputEnabled=true;
        novine.events.onInputOver.add(hoverSve, this);
        novine.events.onInputOut.add(hoverOutSve, this);
        novine.events.onInputDown.add(clicknovine, this);
        
        vrata = this.add.sprite(1080,110,'mvrata');
        vrata.alpha=0;
        vrata.inputEnabled=true;
        vrata.events.onInputOver.add(hoverSve, this);
        vrata.events.onInputOut.add(hoverOutSve, this);
        vrata.events.onInputDown.add(clickvrata, this);
        
        
        
        }, this)
    
    }, this)

}

//Funkcija za hover iznad odabira:
function hoverSve(event, sprite){      
        hover_sound.play();
        this.add.tween(event).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za izlazak iz hovera:
function hoverOutSve(event, sprite){      
        this.add.tween(event).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za hover strelice:
function hovernext(event, sprite){
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
}

//Funkcija za izlaz iz hoovera strelice:
function hoverOutnext (event, sprite) {
        event.scale.setTo(1.0, 1.0);
}

//Funkcija za hover strelice:
function hovernext(event, sprite){
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
}

//Funkcija za izlaz iz hoovera strelice:
function hoverOutnext (event, sprite) {
        event.scale.setTo(1.0, 1.0);
}

//Funkcija za SPACEBAR kada nema input na klik:
function noclickmetro (event, sprite) {
    
    if((input_mode_misli==false) && (input_mode_pricanje1==false) && (input_mode_pricanje2==false) && (input_mode_pricanje3==false)){
        
        if(spacecount%4==0)
            D.play();
        else if(spacecount%4==1)
            Dm.play();
        else if(spacecount%4==2)
            C.play();
        else if(spacecount%4==3)
            Dsh.play();
    
        spacecount++;
        
    }
        
}

function mracenje1() {
    
this.add.tween(metro).to({alpha: 0.2}, 100, Phaser.Easing.Linear.None, true);
    
this.time.events.add(200, function() {    
this.add.tween(metro).to({alpha: 1}, 10, Phaser.Easing.Linear.None, true);
}, this)

this.time.events.add(600, function() {    
this.add.tween(metro).to({alpha: 0.2}, 100, Phaser.Easing.Linear.None, true);
}, this)

this.time.events.add(800, function() {    
this.add.tween(metro).to({alpha: 1}, 10, Phaser.Easing.Linear.None, true);
}, this)

this.time.events.add(1400, function() {    
this.add.tween(metro).to({alpha: 0.2}, 100, Phaser.Easing.Linear.None, true);
}, this) 

this.time.events.add(1700, function() {    
this.add.tween(metro).to({alpha: 1}, 10, Phaser.Easing.Linear.None, true);
}, this)

this.time.events.add(2200, function() {    
this.add.tween(metro).to({alpha: 0.2}, 100, Phaser.Easing.Linear.None, true);
}, this)

this.time.events.add(2300, function() {    
this.add.tween(metro).to({alpha: 1}, 10, Phaser.Easing.Linear.None, true);
}, this)


}

function mracenje2() {

this.add.tween(metro).to({alpha: 0}, 150, Phaser.Easing.Linear.None, true);

this.time.events.add(150, function() {    
this.add.tween(metro).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
}, this)

}

function clicknigdje(event, sprite){
    
    vrata.inputEnabled=false;
    novine.inputEnabled=false;
    nigdje.inputEnabled=false;
    lik1.inputEnabled=false;
    lik2.inputEnabled=false;
    
    click_sound.play();
    
    this.add.tween(event).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
    txt = this.add.sprite(60, 35, 'mnigdjetxt');
    txt.alpha = 0;
    click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_misli = true;
    click.events.onInputDown.add(nextmisli, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

function clicknovine(event, sprite){
    
    vrata.inputEnabled=false;
    novine.inputEnabled=false;
    nigdje.inputEnabled=false;
    lik1.inputEnabled=false;
    lik2.inputEnabled=false;
    
    click_sound.play();
    
    this.add.tween(event).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
    txt = this.add.sprite(85, 35, 'mnovinetxt');
    txt.alpha = 0;
    click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_misli = true;
    click.events.onInputDown.add(nextmisli, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

function clickvrata(event, sprite){
    
    vrata.inputEnabled=false;
    novine.inputEnabled=false;
    nigdje.inputEnabled=false;
    lik1.inputEnabled=false;
    lik2.inputEnabled=false;
    
    click_sound.play();
    
    this.add.tween(event).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
    txt = this.add.sprite(125, 35, 'mvratatxt');
    txt.alpha = 0;
    click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 30, 'click');
    click.alpha=0;
    this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
    click.inputEnabled=true;
    input_mode_misli = true;
    click.events.onInputDown.add(nextmisli, this);
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    
}

//Funkcija za potvrdu na misao:

function nextmisli(event, sprite){
    
    if (input_mode_misli==true) {
    
        vrata.inputEnabled=true;
        novine.inputEnabled=true;
        nigdje.inputEnabled=true;
        lik1.inputEnabled=true;
        lik2.inputEnabled=true;
    
        click_sound.play();
    
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
        input_mode_misli=false;
        click.inputEnabled=false;
        
        chkobject=true;
            
    }
    
    
}

function clicklik1(event, sprite){
    
    vrata.inputEnabled=false;
    novine.inputEnabled=false;
    nigdje.inputEnabled=false;
    lik1.inputEnabled=false;
    lik2.inputEnabled=false;
    
    click_sound.play();
    
    txtl1 = this.add.sprite(245, 275, 'mtxtl1');
    txtl1.alpha = 0;
    txtlm = this.add.sprite(10, 795, 'mtxtlm');
    txtlm.alpha = 0;
    if (metrocounterlik1==0)
        txt = this.add.sprite(310, 365, 'ml1txt1');
    else
        txt = this.add.sprite(310, 365, 'ml1txt3');
    txt.alpha = 0;
    click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 20, 'click');
    click.alpha=0;
    
    this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(txtl1).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
    if (metrocounterlik1<1)
        this.add.tween(txtlm).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
        
    click.inputEnabled=true;
    input_mode_pricanje1=true;
    
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    click.events.onInputDown.add(lik1nxt, this);
    
    
}

function lik1nxt(event, sprite){
    
if(input_mode_pricanje1==true){
    
    if(metrocounterlik1<4){
    
        click.inputEnabled=false;
        input_mode_pricanje1=false;
    
        click_sound.play();
    
        metrocounterlik1++;

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
        if (metrocounterlik1==1)
            txt = this.add.sprite(85, 890, 'mlm1txt1');
    
        else if (metrocounterlik1==2)
            txt = this.add.sprite(300, 310, 'ml1txt2');

        else if (metrocounterlik1==3)
            txt = this.add.sprite(85, 890, 'mlm1txt2');
        
        if (metrocounterlik1!=4){
            
            txt.alpha=0;
            click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 20, 'click');
            click.alpha=0;
    
            this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
            click.inputEnabled=true;
            input_mode_pricanje1=true;
    
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(lik1nxt, this);
        }
        
    
        else {
            
        click.inputEnabled=false;
        input_mode_pricanje1=false;
            
        vrata.inputEnabled=true;
        novine.inputEnabled=true;
        nigdje.inputEnabled=true;
        lik1.inputEnabled=true;
        lik2.inputEnabled=true;
    
        click_sound.play();

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtl1).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtlm).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
            
        chklik1 = true;
            
        }

    }
    
    else {
        
        click.inputEnabled=false;
        input_mode_pricanje1=false;
        
        vrata.inputEnabled=true;
        novine.inputEnabled=true;
        nigdje.inputEnabled=true;
        lik1.inputEnabled=true;
        lik2.inputEnabled=true;
    
        click_sound.play();

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtl1).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        
        }
    }
}

function clicklik2(event, sprite){
    
    vrata.inputEnabled=false;
    novine.inputEnabled=false;
    nigdje.inputEnabled=false;
    lik1.inputEnabled=false;
    lik2.inputEnabled=false;
    
    click_sound.play();
    
    txtl2 = this.add.sprite(575, 185, 'mtxtl2');
    txtl2.alpha = 0;
    txtlm = this.add.sprite(10, 795, 'mtxtlm');
    txtlm.alpha = 0;
    if (metrocounterlik2==0)
        txt = this.add.sprite(605, 235, 'ml2txt1');
    else
        txt = this.add.sprite(605, 235, 'ml2txt3');
    txt.alpha = 0;
    click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 20, 'click');
    click.alpha=0;
    
    this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    this.add.tween(txtl2).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
    if (metrocounterlik2<1)
        this.add.tween(txtlm).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
        
    click.inputEnabled=true;
    input_mode_pricanje2=true;
    
    click.events.onInputOver.add(hovernext, this);
    click.events.onInputOut.add(hoverOutnext, this);
    click.events.onInputDown.add(lik2nxt, this);
    
    
}

function lik2nxt(event, sprite){
    
if(input_mode_pricanje2==true){
    
    if(metrocounterlik2<4){
    
        click.inputEnabled=false;
        input_mode_pricanje2=false;
    
        click_sound.play();
    
        metrocounterlik2++;

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
        if (metrocounterlik2==1)
            txt = this.add.sprite(85, 890, 'mlm2txt1');
    
        else if (metrocounterlik2==2)
            txt = this.add.sprite(605, 235, 'ml2txt2');

        else if (metrocounterlik2==3)
            txt = this.add.sprite(85, 890, 'mlm2txt2');
        
        if (metrocounterlik2!=4){
            
            txt.alpha=0;
            click = this.add.sprite(txt.x + txt.width + 10, txt.y + txt.height - 20, 'click');
            click.alpha=0;
    
            this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
            click.inputEnabled=true;
            input_mode_pricanje2=true;
    
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(lik2nxt, this);
        }
        
    
        else {
            
        click.inputEnabled=false;
        input_mode_pricanje2=false;
            
        vrata.inputEnabled=true;
        novine.inputEnabled=true;
        nigdje.inputEnabled=true;
        lik1.inputEnabled=true;
        lik2.inputEnabled=true;
    
        click_sound.play();

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtl2).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtlm).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
            
        chklik2 = true;
            
        }

    }
    
    else {
        
        click.inputEnabled=false;
        input_mode_pricanje2=false;
        
        vrata.inputEnabled=true;
        novine.inputEnabled=true;
        nigdje.inputEnabled=true;
        lik1.inputEnabled=true;
        lik2.inputEnabled=true;
    
        click_sound.play();

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtl2).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        
        }
    }
}

function lik3nxt(){
    
    if(input_mode_pricanje3==true){
        
        metrocounterlik3++;
    
    if(metrocounterlik3<6){
    
        click.inputEnabled=false;
        input_mode_pricanje3=false;
    
        click_sound.play();
    
        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
        if (metrocounterlik3==1)
            txt = this.add.sprite(65, 875, 'mlm3txt1');
    
        else if (metrocounterlik3==2)
            txt = this.add.sprite(90, 245, 'ml3txt2');

        else if (metrocounterlik3==3)
            txt = this.add.sprite(85, 890, 'mlm3txt2');
        
        else if (metrocounterlik3==4)
            txt = this.add.sprite(105, 210, 'ml3txt3');
        
        else if (metrocounterlik3==5)
            txt = this.add.sprite(85, 890, 'mlm3txt3');
        
            txt.alpha=0;
            click = this.add.sprite(txt.x + txt.width + 5, txt.y + txt.height - 20, 'click');
            click.alpha=0;
    
            this.add.tween(txt).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
            this.add.tween(click).to({alpha: 1}, 100, Phaser.Easing.Linear.None, true);
    
            click.inputEnabled=true;
            input_mode_pricanje3=true;
    
            click.events.onInputOver.add(hovernext, this);
            click.events.onInputOut.add(hoverOutnext, this);
            click.events.onInputDown.add(lik3nxt, this);
        }
        
    
        else {
            
        click.inputEnabled=false;
        input_mode_pricanje3=false;
    
        click_sound.play();

        this.add.tween(txt).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(click).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtl3).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
        this.add.tween(txtlm).to({alpha: 0}, 100, Phaser.Easing.Linear.None, true);
    
        chklik3 = true;
            
        }

    }
}
