game.mainmenu = function (the_game) {};

game.mainmenu.prototype = {
          
    create: function () {
        
        //Muzika i zvukovi:
        mainmenu_music = this.add.audio("mainmenu_loop");
        hover_sound = this.add.audio("hoover");
        click_sound = this.add.audio("click");
        mainmenu_music.loop = true;
        mainmenu_music.play();     
                
        //Dodavanje sprajtova:
        bg = this.add.sprite(0, 0, 'mainmenu_bg');
            bg.alpha = 0;
		title = this.add.sprite(220, 300, 'title');
            title.alpha = 0;
        newgame = this.add.sprite(220, 450, 'new_game');
            newgame.alpha = 0;
        credits = this.add.sprite(220, 600, 'credits');
            credits.alpha = 0;
        
        aura1 = this.add.sprite(600, 50, 'aura1');
             aura1.alpha = 0;
        aura2 = this.add.sprite(600, 50, 'aura2');
             aura2.alpha = 0;
        aura3 = this.add.sprite(600, 50, 'aura3');
            aura3.alpha = 0;
        aura4 = this.add.sprite(600, 50, 'aura4');
            aura4.alpha = 0;
        
        //Fade in za sliku:
        this.add.tween(bg).to( { alpha: 1 }, 2500, Phaser.Easing.Linear.None, true );
        
        //Fade in i kretnja za naslov:
        this.time.events.add(2000, function() {
        this.add.tween(title).to({y: 200}, 2000, Phaser.Easing.Linear.None, true); 
        this.add.tween(title).to({alpha: 1}, 2000, Phaser.Easing.Linear.None, true);
        }, 
        this);
        
        //Fade in za narancaste aure:
        this.time.events.add(4000, function() {
        this.add.tween(aura1).to( { alpha: 1 }, 400, Phaser.Easing.Linear.None, true, 0, 100, true);
        this.add.tween(aura2).to( { alpha: 1 }, 900, Phaser.Easing.Linear.None, true, 0, 300, true);
        }, 
        this);
        
        //Fade in za plave aure:
        this.time.events.add(5000, function() {
        ta3 = this.add.tween(aura3).to( { alpha: 1 }, 1400, Phaser.Easing.Linear.None, true, 0, 400, true);
        ta4 = this.add.tween(aura4).to( { alpha: 1 }, 1600, Phaser.Easing.Linear.None, true, 0, 600, true);
        }, 
        this);
        
        //Fade in izbornika:
        this.time.events.add(4000, function() {
        this.add.tween(newgame).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
            newgame.inputEnabled = true;
            newgame.events.onInputDown.add(Start, this);
            newgame.events.onInputOver.add(hoverIzbornik, this);
            newgame.events.onInputOut.add(hoverOutIzbornik, this);
        this.add.tween(credits).to({alpha: 1}, 1200, Phaser.Easing.Linear.None, true);
            credits.inputEnabled = true;
            credits.events.onInputOver.add(hoverIzbornik, this);
            credits.events.onInputOut.add(hoverOutIzbornik, this);  
        }, 
        this);
    }
};

//Funkcija za hover iznad odabira:
function hoverIzbornik(event, sprite){      
        hover_sound.play();
        event.scale.setTo(1.1, 1.1);
        mark = this.add.sprite(event.x-20, event.y+48, 'select');
        mark.alpha = 0;
        this.add.tween(mark).to({alpha: 1}, 1000, Phaser.Easing.Linear.None, true); 
}

//Funkcija za izlaz iz hoovera:
function hoverOutIzbornik (event, sprite) {  
        event.scale.setTo(1.0, 1.0);
        this.add.tween(mark).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
}

//Funkcija za klik na newgame:
function Start (event, sprite) {   
    newgame.inputEnabled = false;
    credits.inputEnabled = false;
    newgame.scale.setTo(1.0, 1.0);
    click_sound.play();
    this.add.tween(title).to({alpha: 0}, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(bg).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(aura1).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(aura2).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(aura3).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(aura4).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(newgame).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(credits).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    this.add.tween(mark).to( { alpha: 0 }, 1500, Phaser.Easing.Linear.None, true );
    mainmenu_music.fadeOut();
    this.time.events.add(1500, function() {
    aura1.destroy();
    aura2.destroy();
    aura3.destroy();
    aura4.destroy();
    newgame.destroy();
    credits.destroy();
    mainmenu_music.fadeOut();
    }, 
    this);
    this.time.events.add(3000, function() {
    this.state.start("Intro");
    }, 
    this);
    
}