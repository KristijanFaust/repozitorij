#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAGNITUDA 1e-9f //Vrijednost magnitude utjecaja sile nekog tjela
#define VELICINA 256 //Broj zauzetih niti po bloku

const float korak = 0.01f; //Vrijednost jedne mjere za pomak 

//Struktura nad kojom ce se vrsiti obrada podataka:
struct Tijelo { float x, y, z, vx, vy, vz; };

//Funkcija za postavljanje početnih nasumičnih vrijednosti za svako tijelo u sustavu:
void postavljanjeVrijednosti(float *tijela, int n){
  for (int i = 0; i < n; i++){
    tijela[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
  }
}

//Kernel funkcija kojom GPU uređaj radi izračun utjecaja sile među tjelima:
__global__ void silaTijela(Tijelo *tijelo, float korak, int br_tijela){

  int i = blockDim.x * blockIdx.x + threadIdx.x; //Definiranje indeksa za obradu podataka
  if (i < br_tijela){
    
    //Postavljanje sile na 0 u svim smjerovima:
    float Fx = 0.0f; 
    float Fy = 0.0f; 
    float Fz = 0.0f;

    for (int j = 0; j < br_tijela; j++) {

	  //Racunanje udaljenosti izmedju zasebnih kordinata dva tjela:       
      float dx = tijelo[j].x - tijelo[i].x;
      float dy = tijelo[j].y - tijelo[i].y;
      float dz = tijelo[j].z - tijelo[i].z;

	   //Racunanje ukupne udaljenosti izmedu dva tijela uz pribrojavanje magnitude utjecaja sile tjela:
      float udaljenost = dx*dx + dy*dy + dz*dz + MAGNITUDA; 
	  //Reciprocna vrijednost drugog korijena vrijednosti udaljenosti:
      float inverzUdaljenosti = rsqrtf(udaljenost); 
	  //Kubiranje reciprocne vrijednosti drugog korijena udaljenosti:
      float kub = inverzUdaljenosti * inverzUdaljenosti * inverzUdaljenosti; 

	  //Izracunavanje vrijednosti sile u svim smjerovima izmedju dva tijela:
      Fx += dx * kub; 
      Fy += dy * kub; 
      Fz += dz * kub;

    }

	//Izracunavanje brzine kretnje i-tog tijela prema izracunatoj sili koja djeluje na njega i mjerne vrijednosti pomaka:
    tijelo[i].vx += korak*Fx; 
    tijelo[i].vy += korak*Fy; 
    tijelo[i].vz += korak*Fz;
  
  }
}

int main() {
  
  //Inicijalizacija i unos zeljenih parametra za obradu:
  int brojTijela = 0;
  int brojIteracija = 0;
  
  printf ("Unesite zeljeni broj tijela u sustavu: ");
  scanf("%d", &brojTijela);
  
  printf ("\nUnesite zeljeni broj iteracija na sustav: ");
  scanf("%d", &brojIteracija);
  
  //Alokacija memorije na domacinu za zeljeni broj tijela:
  int alokacija = brojTijela * sizeof(Tijelo); //Broj potrebnih bajtova za alociranje
  float *pokazivac = (float*)malloc(alokacija); //Deklaracija pokazivaca na alocirane bajtove
  Tijelo *tijelo = (Tijelo*)pokazivac; //Umetanje podataka u alociranu memoriju 
  
  //Postavljanje nasumicnih vrijednosti za alocirana tjela:
  postavljanjeVrijednosti(pokazivac, 6*brojTijela); 
  
  //Alociranje memorije na GPU uredjaju:
  float *d_pokazivac; //Deklaraicija pokazivaca za GPU uredjaj
  cudaMalloc(&d_pokazivac, alokacija); //Alokacija potrebnih bajtova na GPU uređaju
  Tijelo *d_tijelo = (Tijelo*)d_pokazivac; //Umetanje podataka u alociranu memoriju na GPU uredjaju
  
  //Broj potrebnih blokova za obradu podataka:
  int broj_blokova = (brojTijela + VELICINA - 1) / VELICINA;
  
  //Glavna petlja za obradu podataka na domacinu:
  for (int iteracija = 0; iteracija < brojIteracija; iteracija++) {
    
    printf("\n\n%d. iteracija: \n\n", iteracija+1);
    
	//Kopiranje vrijednosti podataka sa domacina na GPU uredjaj:
    cudaMemcpy(d_pokazivac, pokazivac, alokacija, cudaMemcpyHostToDevice);
    
	//Pozivanje kernel funkcije na GPU uredjaju sa odgovarajucim parametrima za izracunavanje sile djelovanja izmedju dva tijela:
    silaTijela<<<broj_blokova, VELICINA>>>(d_tijelo, korak, brojTijela); 
    
	//Vracanje obradjenih podataka domacinu:
    cudaMemcpy(pokazivac, d_pokazivac, alokacija, cudaMemcpyDeviceToHost);

	//Petlja za izracunavanje nove pozicije i-tog tijela na domacinu:
    for (int i = 0 ; i < brojTijela; i++) { 
      
      tijelo[i].x += tijelo[i].vx*korak;
      tijelo[i].y += tijelo[i].vy*korak;
      tijelo[i].z += tijelo[i].vz*korak;
      
      printf("Tijelo: %d -- Pozicija x: %.3f , Pozicija y: %.3f , Pozicija z: %.3f .\n" ,  i+1, tijelo[i].x, tijelo[i].y, tijelo[i].z );
    }

    printf("\n");

  }
  
  //Dealokacija memorije:
  free(pokazivac);
  cudaFree(d_pokazivac);

  return 0;
}
