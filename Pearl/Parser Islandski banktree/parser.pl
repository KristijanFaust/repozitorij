use Parse::RecDescent;
use Data::Dumper;
use JSON;

$::RD_AUTOACTION = q { [@item] };
$gramatika= q {

Parser: ulaz(s)

ulaz: Pocetak '('Rekurzija')' Zavrsetak

Paralelna_rekurzija: '('Rekurzija')'

Zavrsetak: Oznaka_paragrafa Kraj | Kraj

Rekurzija: NEZAVRSNI zavrsni 
	 | NEZAVRSNI Paralelna_rekurzija(s)



NEZAVRSNI: /([A-Z]+(-|\+)?\w*(-|\+|=)?\w*(-|\+|=)?\w*(-|\+|=)?\w*\d*|)\W/
zavrsni: /[^()]+/
Oznaka_paragrafa: /\([^()]+\)/ 
Pocetak: /\(/
Kraj: /\)/

};

#Za testni niz uzeta su prva tri isparsirana paragrafa iz datoteke 1150.firstgrammar.sci-lin.psd
$testni_niz='( (IP-MAT (PP (P Í-í)
	      (NP (QS-D flestum-margur) (NS-D löndum-land)))
	  (VBPI setja-setja)
	  (NP-SBJ (NS-N menn-maður))
	  (PP (P á-á)
	      (NP (NS-A bækur-bók)))
	  (NP-OB1 (NP-ADV (OTHER-A annað-annar)
			  (NP-POS (NUM-G tveggja-tveir)))
		  (NP (D-A þann-sá)
		      (N-A fróðleik-fróðleikur)
		      (CP-REL (WNP-1 0)
			      (C er-er)
			      (IP-SUB (NP-SBJ *T*-1)
				      (ADVP-LOC (ADV þar-þar))
				      (PP (P innan-innan)
					  (NP (N-G lands-land)))
				      (HVPI hefir-hafa)
				      (DON gjörst-gera))))
		  (, –-–)
		  (CONJP (CONJ eða-eða)
			 (NP (D-A þann-sá)
			     (OTHER-A annan-annar)
			     (CP-REL (WNP-2 0)
				     (C er-er)
				     (IP-SUB (NP-SBJ *T*-2)
					     (ADJP (ADJS-N minnisamlegastur-minnisamlegur))
					     (VBPI þykir-þykja))))))
	  (PP (P þó-þó)
	      (CP-ADV (C að-að)
		      (IP-SUB (NP-SBJ *pro*)
			      (NP-ADV (OTHER-G annars-annar) (N-G staðar-staður))
			      (HVPS hafi-hafa)
			      (ADVP (ADVR heldur-heldur))
			      (DON gjörst-gera))))
	  (. –-–))
  (ID 1150.FIRSTGRAMMAR.SCI-LIN,.1))

( (IP-MAT (CONJ eða-eða)
	  (NP-OB1 (NS-A lög-lög)
		  (NP-POS (PRO-A sín-sinn)))
	  (VBPI setja-setja)
	  (NP-SBJ (NS-N menn-maður))
	  (PP (P á-á)
	      (NP (NS-A bækur-bók)))
	  (IP-ABS (NP-SBJ (Q-N hver-hver) (N-N þjóð-þjóð))
		  (PP (P á-á)
		      (NP (NP-POS (PRO-A sína-sinn))
			  (N-A tungu-tunga))))
	  (. .-.))
  (ID 1150.FIRSTGRAMMAR.SCI-LIN,.2))

( (IP-MAT (IP-MAT-1 (CONJ En-en)
		    (PP-LFD (P af-af)
			    (NP (PRO-D því-það)
				(CP-THT-PRN (C að-að)
					    (IP-SUB (NP-SBJ (NS-N tungur$-tunga)
							    (D-N $nar-hinn)
							    (NP-PRN *ICH*-2))
						    (BEPI eru-vera)
						    (ADJP (ADJ-N ólíkar-ólíkur)
							  (NP (NP (Q-N hver-hver))
							      (OTHER-D annarri-annar)))
						    (, ,-,)
						    (NP-PRN-2 (PRO-N þær-hún)
							      (CP-REL *ICH*-4)
							      (ADVP-3 (ADV þegar-þegar))
							      (CP-REL-4 (WNP-5 0)
									(C er-er)
									(IP-SUB (NP-SBJ *T*-5)
										(ADVP *ICH*-3)
										(PP (P úr-úr)
										    (NP (NP (ONE-D einni-einn))
											(CONJP (CONJ og-og)
											       (NP (D-D hinni-hinn) (ADJ-D sömu-samur) (N-D tungu-tunga)))))
										(HVPI hafa-hafa)
										(VBN (VBN gengist-ganga) (CONJ eða-eða) (VBN greinst-greina)))))))))
		    (NP-SBJ *pro*)
		    (, ,-,)
		    (ADVP-TMP-RSP (ADV þá-þá))
		    (VBPI þarf-þurfa)
		    (IP-INF (NP-OB1 (ADJ-A ólíka-ólíkur) (NS-A stafi-stafur))
			    (RP í-í)
			    (TO að-að)
			    (HV hafa-hafa)))
	  (CONJP (CONJ en-en)
		 (IP-MAT=1 (NEG eigi-ekki)
			   (NP-OB1 (D-A hina-hinn) (ADJ-A sömu-samur) (Q-A alla-allur))
			   (PP (P í-í)
			       (NP (Q-D öllum-allur)))
			   (, ,-,)
			   (CP-ADV (WADVP-6 0)
				   (C sem-sem)
				   (IP-SUB (IP-SUB-7 (ADVP *T*-6)
						     (NEG eigi-ekki)
						     (VBPI rita-rita)
						     (NP-SBJ (NPRS-N grikkir-grikki))
						     (NP-ADT (NS-D latínustöfum-latínustafur))
						     (NP-OB1 (N-A girsku$-gríska) (D-A $na-hinn)))
					   (CONJP (CONJ og-og)
						  (IP-SUB=7 (ADVP *T*-6)
							    (NEG eigi-ekki)
							    (NP-SBJ (NS-N latínumenn-latínumaður))
							    (NP-ADT (ADJ-D girskum-grískur) (NS-D stöfum-stafur))
							    (NP-OB1 (N-A latínu-latína))))
					   (, ,-,)
					   (CONJP (CONJ né-né)
						  (IP-SUB=7 (ADVP *T*-6)
							    (ADVP (ADV enn-enn) (ADVR heldur-heldur))
							    (NP-SBJ (ADJ-N hebreskir-hebreskur) (NS-N menn-maður))
							    (NP-OB1 (N-A hebresku$-hebreska) (D-A $na-hinn))
							    (NP-ADT (CONJ hvorki-hvorki)
								    (NP (ADJ-D grískum-grískur) (NS-D stöfum-stafur))
								    (CONJP (CONJ né-né)
									   (NP (N-D latínu-latína))))))))))
	  (. ,-,))
  (ID 1150.FIRSTGRAMMAR.SCI-LIN,.3))';

$parser=Parse::RecDescent->new($gramatika);
$stablo = $parser->Parser($testni_niz);

print Dumper($stablo);

$json = encode_json $stablo;
open($datoteka, '>', 'Rezultat.json');
print $datoteka $json;
close $datoteka;

open($datoteka, '>', 'Stablo.txt');
print $datoteka Dumper($stablo);
close $datoteka;

print "\n Json format zapisan u datoteci Rezultat.json \n\n";
print "\n Kraj programa \n\n";
