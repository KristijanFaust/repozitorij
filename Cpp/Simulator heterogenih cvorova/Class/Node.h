#ifndef NODE_H_
#define NODE_H_

class Node{

private:

    std::string model; //ime cvora
    int CPUs; //broj procesora po cvoru
    int cores; //broj jezgri po procesoru
    int coresTotal; //broj jezgri u cvoru
    int ops; //broj operacija po sekundi
    float freq; //takt rada cvora
    float nodeRpeak; //Rpeak cvora (broj jezgi * broj operacija po sekundi * takt rada)

public:

    //Konstruktori:
    Node();
    Node(std::string arg1, int arg2, int arg3, int arg4, float arg5);
    //

    //Get metode:
    std::string getModel();
    int getCPUs();
    int getCores();
    int getOps();
    float getFreq();
    float getNodeRpeak();
    //

    //Metoda za ispis podataka:
    void getData();
    //

    //Destruktor:
    ~Node();
    //
};

#endif /* NODE_H_ */
