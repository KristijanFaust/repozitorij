#include <iostream>
#include "Node.h"

//Konstruktori:

Node::Node(){

    model = "probni";
    CPUs = 4;
    cores = 4;
    ops = 2;
    freq = 2.50;

}

Node::Node(std::string arg1, int arg2, int arg3, int arg4, float arg5){

    model = arg1;
    CPUs = arg2;
    cores = arg3;
    coresTotal = arg2*arg3;
    ops   = arg4;
    freq  = arg5; //Ghz
    nodeRpeak = float(coresTotal) * freq * float(ops);
}

//

//Get metode:

std::string Node::getModel(){
    return model;
}

int Node::getCPUs(){
    return CPUs;
}

int Node::getCores(){
    return cores;
}

int Node::getOps(){
    return ops;
}

float Node::getFreq(){
    return freq;
}

float Node::getNodeRpeak(){
    return nodeRpeak;
}

//

//Metoda za ispis podataka:

void Node::getData(){

    std::cout<<"  Model:      "<<getModel()<<std::endl;
    std::cout<<"  CPUs:       "<<getCPUs()<<std::endl;
    std::cout<<"  Cores:      "<<getCores()<<std::endl;
    std::cout<<"  OPS:        "<<getOps()<<std::endl;
    std::cout<<"  Frequency:  "<<getFreq()<<" Ghz"<<std::endl;
    std::cout<<"  Node Rpeak: "<<getNodeRpeak()<<std::endl;
    std::cout<<std::endl;

}

//

//Destruktor:

    Node::~Node(){};

//
