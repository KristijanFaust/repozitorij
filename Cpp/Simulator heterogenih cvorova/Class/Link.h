#ifndef LINK_H_
#define LINK_H_

#include "Node.h"

class Link{

private:

    std::string model; //ime poveznice
    float speed; //brzina poveznice
    Node sideA; //1. Cvor kojeg povezuje
    Node sideB; //2. Drugi cvor kojeg povezuje

public:

    //Konstruktori:
    Link();
    Link(std::string arg1, float arg2, Node arg3, Node arg4);
    //

    //Get metode:
    std::string getModel();
    int getSpeed();
    //

    //Metoda za ispis:
    void getData();
    //

    //Destruktor
    ~Link();
    //
};
#endif /* LINK_H_ */
