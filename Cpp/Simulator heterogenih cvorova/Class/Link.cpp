#include <iostream>
#include "Link.h"

//Konstruktori:

Link::Link(){};
Link::Link(std::string arg1, float arg2, Node arg3, Node arg4){

    model = arg1;
    speed = arg2; //Gb/s
    sideA = arg3;
    sideB = arg4;
}

//

//Get metode:

std::string Link::getModel(){
    return model;
}

int Link::getSpeed(){
    return speed;
}

//

//Metoda za ispis podataka:

void Link::getData(){

    std::cout<<"Model:     "<<getModel()<<std::endl;
    std::cout<<"Bandwidth: "<<getSpeed()<<" Gb/s"<<std::endl;
    std::cout<<"Node A:    "<<sideA.getModel()<<std::endl;
    std::cout<<"Node B:    "<<sideB.getModel()<<std::endl;
    std::cout<<std::endl;

}

//

//Destruktor:

Link::~Link(){};

//
