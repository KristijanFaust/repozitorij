#include <iostream>
#include "Class/Node.h" //zaglavlje klase za cvorove
#include "Class/Link.h" //zaglavlje klase za poveznice

static float Rpeak = 0; //Globalna variabla za racunanje Rpeak vrijednosti (u planu je implementirati System klasu koja bi pratila opæe atribute i vrijednosti simuliranog sustava)

int main()
{
    //Stvaranje 4 probna cvora
    Node node1("Probni Cvor 1", 2, 4, 4, 3.30);
    Node node2("Probni Cvor 2", 4, 4, 2, 3.40);
    Node node3("Probni Cvor 3", 2, 8, 2, 2.20);
    Node node4("Probni Cvor 4", 4, 8, 6, 3.70);

    //Stvaranje 4 probne poveznice
    Link link1("Probna Poveznica 1-2", 10, node1, node2 );
    Link link2("Probna Poveznica 3-4", 40, node3, node4 );
    Link link3("Probna Poveznica 1-3", 10, node1, node3 );
    Link link4("Probna Poveznica 2-4", 40, node2, node4 );

    //Ispis podataka o poveznicama
    link1.getData();
    link2.getData();
    link3.getData();
    link4.getData();

    //Ispis podataka o cvorovima
    node1.getData();
    node2.getData();
    node3.getData();
    node4.getData();

    //Izracun Rpeak vriejdnosti sustava i njen ispis
    Rpeak = node1.getNodeRpeak() + node2.getNodeRpeak() + node3.getNodeRpeak() + node4.getNodeRpeak();
    std::cout<<"System Rpeak: "<<Rpeak<<std::endl;


    return 0;
}
