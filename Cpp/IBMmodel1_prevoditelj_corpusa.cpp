//Implementacija EM algoritma za IBM model 1
// -std=c++11

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <iomanip>

int main()
{

//Otvaranje rjeènika
std::ifstream ulaz1 ("toy.en");
if (!ulaz1.is_open())
    {
         std::cout<<"Greska: Prvi rijecnik nije pronaden!"<<std::endl;
         return 1;
    }

std::cout<<"Prvi rjecnik otvoren."<<std::endl;

std::ifstream ulaz2 ("toy.de");
if (!ulaz2.is_open())
    {
         std::cout<<"Greska: Drugi rijecnik nije pronaden!"<<std::endl;
         return 1;
    }

std::cout<<"Drugi rjecnik otvoren."<<std::endl;

//Alokacija glavnog spremnika rjeæi i pomoæne varijable za unos rijeèi
std::vector<std::vector<std::string>> ulazni_podaci;
std::string recenica;
std::set<std::string> skup_rijeci;

std::cout<<"Spremnik rjeci alociran, unosim rijeci..."<<std::endl;

//Unos 1. rjeènika
while (std::getline(ulaz1, recenica))
    {
        std::vector<std::string> privremeni_vektor;
        std::istringstream linija (recenica);
        std::string rijec;

        while (linija>>rijec)
            {
                privremeni_vektor.push_back(rijec);
                skup_rijeci.insert(rijec);
            }
        if (privremeni_vektor.size())
        ulazni_podaci.emplace_back(std::move(privremeni_vektor));
    }

//Unos 2. rjeènika
while (std::getline(ulaz2, recenica))
    {
        std::vector<std::string> privremeni_vektor;
        std::istringstream linija (recenica);
        std::string rijec;

        while (linija>>rijec)
            privremeni_vektor.push_back(rijec);

        if (privremeni_vektor.size())
        ulazni_podaci.emplace_back(std::move(privremeni_vektor));
    }

ulaz1.close();
ulaz2.close();

std::cout<<"Podaci uneseni:"<<std::endl;

//Provjera postivanja IBM modela 1:
if (ulazni_podaci.size()/2!=ulazni_podaci.size()-ulazni_podaci.size()/2)
    {
        std::cout<<"Uneseni korpus ne postuje IBM model 1. Prekid obrade."<<std::endl;
        return 2;
    }

for (unsigned int i=0; i<ulazni_podaci.size()/2;i++)
    {
        if (ulazni_podaci[i].size()!=ulazni_podaci[i+ulazni_podaci.size()/2].size())
            {
                std::cout<<"Uneseni korpus ne postuje IBM model 1. Prekid obrade."<<std::endl;
                return 2;
            }
    }

//Ispis svih rjeci
for (unsigned int i=0; i<ulazni_podaci.size();i++)
    {
        std::cout<<" "<<std::endl;
        if (i==ulazni_podaci.size()/2)
            std::cout<<std::endl;
        for (unsigned int j=0; j<ulazni_podaci[i].size();j++)
            {
                std::cout<<" "<<ulazni_podaci[i][j];
            }
    }

std::cout<<"\n\nBroj zasebnih rijeci za prijevod: "<<skup_rijeci.size()<<std::endl;

// Uparivanje rijeci i dodjeljivanje uniformne vrijednosti:

std::map<std::pair<std::string, std::string>, float> parovi_rijeci;
std::pair <std::string, std::string> privremeni_parovi;
float broj_rijeci=skup_rijeci.size();

for (unsigned int i=0; i<ulazni_podaci.size()/2; i++)
    {
     for (unsigned int j=0; j<ulazni_podaci[i].size();j++)
        {
            for (unsigned int x=0; x<ulazni_podaci[i].size();x++)
            {
                privremeni_parovi = std::make_pair(ulazni_podaci[i][j], ulazni_podaci[i+ulazni_podaci.size()/2][x]);
                parovi_rijeci[privremeni_parovi] = 1/broj_rijeci;
            }
        }
    }

std::cout<<"\n Algoritam u tijeku... \n "<<std::endl;

//EM algoritam
unsigned int konvergencija=0;
//Konvergencija
for (;konvergencija<broj_rijeci;konvergencija++)
{
    float vjerovatnost=0;
    float normalizacija=0;
    std::vector < std::vector < float > > spremnik_vjerovatnosti;
    std::vector < float > pomocni_spremnik;

    //Petlja za prva 3 koraka algoritma
    for (unsigned int i=0; i<ulazni_podaci.size()/2;i++)
        {
            normalizacija=0;
            unsigned int brojac=0;
            while (brojac<ulazni_podaci[i].size()*ulazni_podaci[i].size()+ulazni_podaci[i].size())
                {
                    vjerovatnost=0;
                    for (unsigned int j=0; j<ulazni_podaci[i].size();j++)
                        {
                            //Pronalazenje vjerovatnosti iz parova rijeci
                            privremeni_parovi = std::make_pair(ulazni_podaci[i][j],ulazni_podaci[i+(ulazni_podaci.size()/2)][brojac%ulazni_podaci[i].size()]);
                            std::map< std::pair<std::string, std::string>, float >::iterator pokazivac = parovi_rijeci.find(privremeni_parovi);
                            if (vjerovatnost==0)
                            vjerovatnost=pokazivac->second;
                            else
                            vjerovatnost*=pokazivac->second;
                            brojac++;
                        }
                    brojac++;
                    pomocni_spremnik.push_back(vjerovatnost);
                    if (pomocni_spremnik.size()==ulazni_podaci[i].size())
                        {
                            spremnik_vjerovatnosti.push_back(pomocni_spremnik);
                            pomocni_spremnik.clear();
                        }
                    normalizacija+=vjerovatnost;
                }
            //Normalizacija dobivenih vjerovatnosti
            for (unsigned int y=0;y<spremnik_vjerovatnosti[i].size();y++)
                {
                    spremnik_vjerovatnosti[i][y]/=normalizacija;
                }
        }

    //Brisanje starih vrijednosti i dodavanje sumiranih frakcija

    for ( std::map< std::pair<std::string, std::string>, float >::iterator pokazivac = parovi_rijeci.begin();pokazivac != parovi_rijeci.end(); ++pokazivac )
        pokazivac->second=0;

    for (unsigned int i=0; i<ulazni_podaci.size()/2;i++)
        {
            unsigned int drugi_indeks=0;
            unsigned int brojac=0;
            while (brojac<ulazni_podaci[i].size()*ulazni_podaci[i].size()+ulazni_podaci[i].size())
                {
                    for (unsigned int j=0; j<ulazni_podaci[i].size();j++)
                        {
                            privremeni_parovi = std::make_pair(ulazni_podaci[i][j], ulazni_podaci[i+(ulazni_podaci.size()/2)][brojac%ulazni_podaci[i].size()]);
                            std::map< std::pair<std::string, std::string>, float >::iterator pokazivac = parovi_rijeci.find(privremeni_parovi);
                            pokazivac->second+=spremnik_vjerovatnosti[i][drugi_indeks%spremnik_vjerovatnosti[i].size()];
                            brojac++;
                        }
                    brojac++;
                    drugi_indeks++;
                }
        }

    //Normalizacija sumiranih frakcija:
    std::vector <std::string> zasebna_rijec( skup_rijeci.begin(), skup_rijeci.end() );
    std::vector <float> tablica_normalizacije (zasebna_rijec.size());
    std::string trazena_rijec;
    float trazena_vrijednost=0;

    for (unsigned int i=0; i<zasebna_rijec.size();i++)
        {
            trazena_rijec=zasebna_rijec[i];
            for ( std::map< std::pair<std::string, std::string>, float >::iterator pokazivac = parovi_rijeci.begin();pokazivac != parovi_rijeci.end(); ++pokazivac )
                {
                    if (pokazivac->first.first==trazena_rijec)
                        {
                            trazena_vrijednost=pokazivac->second;
                            tablica_normalizacije[i]+=trazena_vrijednost;
                        }
                }
        }

    for (unsigned int i=0; i<zasebna_rijec.size();i++)
        {
            trazena_rijec=zasebna_rijec[i];
            for ( std::map< std::pair<std::string, std::string>, float >::iterator pokazivac = parovi_rijeci.begin();pokazivac != parovi_rijeci.end(); ++pokazivac )
                {
                    if (pokazivac->first.first==trazena_rijec)
                        {
                            pokazivac->second/=tablica_normalizacije[i];
                        }
                }
        }
}

//Ispis vjerovatnosti:
std::cout<<" Tablica vjerovatnosti:\n\n";
std::cout.precision(4);
for ( std::map< std::pair<std::string, std::string>, float >::const_iterator pokazivac = parovi_rijeci.begin();pokazivac != parovi_rijeci.end(); ++pokazivac )
    std::cout<<" ("<<pokazivac->first.first<<" | "<<pokazivac->first.second<<") = "<<std::fixed<<pokazivac->second<<std::endl;

//Ispis prevedenih rijeci
std::cout<<"\n\n Najvjerovatniji prijevodi zasebnih rijeci: \n\n";
int novi_red;
for ( std::map< std::pair<std::string, std::string>, float >::const_iterator pokazivac = parovi_rijeci.begin();pokazivac != parovi_rijeci.end(); ++pokazivac )
    {
        if (pokazivac->second>0.5)
            {
                std::cout<<" "<<pokazivac->first.first<<" -> "<<pokazivac->first.second<<"; ";
                novi_red++;
                if (novi_red%4==0)
                    std::cout<<std::endl;
            }
    }
if (novi_red==0)
std::cout<<" Nijedna rijec nema vjerovatni prijevod"<<std::endl;

std::cout<<"\n\n";

return 0;
}
