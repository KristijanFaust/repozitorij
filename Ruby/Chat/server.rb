require 'socket'

server = TCPServer.open(2000) 

loop do

	puts "Server up. Waiting for a connection..."
  
	if client = server.accept
	
		puts "Connected to client."
		print "Starting time of conversation: "
		puts(Time.now.ctime)
		puts "To exit the convesation and close the connection type 'exit()'"
		puts
	
		sentence = ""
	
		readline = Thread.new do
	
			until sentence.chomp == "exit()" do
				
				if message = client.gets
				
					if message.chop == "exit()" 
					
						puts "Client has closed the conversation and dissconected. Press RETURN"
						break						
					
					else
					
						puts ("Client: " + message.chop)
						
					end
				
				end
					
			end
		
		end
		
		sendline = Thread.new do
	
			until readline.status == false || sentence == "exit()" do
				
				sentence = gets.chomp
				client.puts(sentence)
				
				if sentence == "exit()"
				
					readline.exit
					
				end
					
			end

		end
		
		readline.join
		sendline.join
		
		client.close
		
		puts "Connection closed. Terminating server."
		break
  
	end
	
end