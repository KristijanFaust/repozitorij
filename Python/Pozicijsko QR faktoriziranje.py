import math
import numpy as np

#Funkcija za racunanje udaljenosti izmedju dva odasiljaca:
def udaljenost_odasiljaca(Bi,i,j):
	return math.sqrt((Bi[i,0]-Bi[j,0])**2 + (Bi[i,1]-Bi[j,1])**2 + (Bi[i,2]-Bi[j,2])**2)

#Koordinate odasiljaca:
Bi = np.matrix([[475060, 1096300, 4670],[481500, 1094900, 4694],[482230, 1088430, 4831],[478050, 1087810, 4775],[471430, 1088580, 4752],[468720, 1091240, 4803],[467400, 1093980, 4705],[468730, 1097340, 4747]])

#Ispis tablice
print
print 'Tablica pozicija odasiljaca izrazena koordinatama x, y i z:'
print
print '           x            y          z'
for i in range (0,8):
	print
	print '        ',
	for j in range (0,3):
		print Bi[i,j], '    ',
print '\n'

#Tocne udaljenosti objekta i koordinate:
P1 = np.array([480000, 1093000, 4668])
r = np.array([5940.893, 2420.883, 5087.666, 5545.271, 9643.044, 11417.270, 12638.110, 12077.030])

#Aproksimativne udaljenosti objekta:
delta_r=np.array([5940.381, 2421.056, 5087.983, 5545.080, 9643.512, 11417.411, 12638.439, 12077.640])

#Ispis udaljenosti:
print '\n'
print 'Tocne udaljenosti odasiljaca i objekta: '
for i in range (0,8):
	print 'B%d' % (i), '=', r[i],'  ', 
print '\n'

print 'Izmjerene udaljenosti odasiljaca i objekta: '
for i in range (0,8):
	print 'B%d' % (i),'=',delta_r[i],'  ',
print '\n'

print 'Apsolutne greske izmjerenih udaljenosti: '
for i in range (0,8):
	print 'B%d'% (i),'=',round(abs(r[i]-delta_r[i]), 3),'  ',
print '\n'

#Odasiljac za linearizaciju:
j = 0

#Linearizacija koordinata odasiljaca i stvaranje matrice A:
A = []
for i in range (1,8):
	A.append([Bi[i,0]-Bi[j,0],Bi[i,1]-Bi[j,1],Bi[i,2]-Bi[j,2]])
A = np.asmatrix(A)

#Linearizacija koordinata objekta i stvaranje vektora x za prave udaljenosti:
x = np.matrix([[P1[0]-Bi[j,0]],[P1[1]-Bi[j,1]],[P1[2]-Bi[j,2]]])

#Linearizacija pravih udaljenosti objekta od odasiljaca i stvaranje vektora b:
b = []
for i in range (1,8):
	b.append([1./2. *(r[j]**2 - r[i]**2 + udaljenost_odasiljaca(Bi, i, j)**2)])
b = np.asmatrix(b)

#Linearizacija izmjerenih udaljenosti objekta od odasiljaca i stvaranje vektora c:
c = []
for i in range (1,8):
	c.append([1./2. *(delta_r[j]**2 - delta_r[i]**2 + udaljenost_odasiljaca(Bi, i, j)**2)])
c = np.asmatrix(c)

#Rastavljanje matrice A na Q i R:
Q, R = np.linalg.qr(A)

#Izrazavanje vektora x pravih udaljenosti pomocu QR faktorizacije:
QRx = R.I*(Q.T*b)

#Izrazavanje vektora x izmjerenih udaljenosti pomocu QR faktorizacije:
delta_x = R.I*(Q.T*c)

#Ispis rezultata:
print '\n'
print 'Prave vrijednosti vektora x:'
for i in range (0,3):
	print '   ', x[i,0]
print '\n'
print 'Vrijednosti vektora x dobivene QR faktorizacijom na bazi pravih vrijednosti:'
for i in range (0,3):
	print '   ', round(QRx[i,0],3)
print '\n'
print 'Vrijednosti vektora x dobivene QR faktorizacijom na bazi izmjerenih vrijednosti:'
for i in range (0,3):
	print '   ', round(delta_x[i,0],3)
print '\n'
print 'Apsolutna greska pravih vrijednosti vektora x i vrijednosti dobivenih QR faktorizacijom na bazi pravih vrijednosti:'
for i in range (0,3):
	print '   ', round(abs(x[i,0]-QRx[i,0]),)
print '\n'
print '\n'
print 'Apsolutna greska pravih vrijednosti vektora x i vrijednosti dobivenih QR faktorizacijom na bazi izmjerenih vrijednosti:'
for i in range (0,3):
	print '   ', round(abs(x[i,0]-delta_x[i,0]))
print '\n'





