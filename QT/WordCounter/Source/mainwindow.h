#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "resultsview.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_OpenFile_clicked();

    void on_ExitButton_clicked();

    void on_AnalyzeFile_clicked();

    void on_IgnoreLineEdit_textEdited(const QString &txt);

    void on_ConsiderLineEdit_textEdited(const QString &txt);

    void on_rawdataCheck_toggled(bool checked);

    void on_wordsonlyCheck_toggled(bool checked);

    void on_wordsandrestCheck_toggled(bool checked);

    void on_punctationsCheck_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    ResultsView *rv;

};

#endif // MAINWINDOW_H
