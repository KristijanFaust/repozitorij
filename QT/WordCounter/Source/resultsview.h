#ifndef RESULTSVIEW_H
#define RESULTSVIEW_H

#include <QMainWindow>

namespace Ui {
class ResultsView;
}

class ResultsView : public QMainWindow
{
    Q_OBJECT

public:
    explicit ResultsView(QWidget *parent = 0);
    ~ResultsView();

private:
    Ui::ResultsView *ui;
};

#endif // RESULTSVIEW_H
