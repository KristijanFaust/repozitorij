#ifndef REULSTSVIEW_H
#define REULSTSVIEW_H

#include <QMainWindow>

namespace Ui {
class ReulstsView;
}

class ReulstsView : public QMainWindow
{
    Q_OBJECT

public:
    explicit ReulstsView(QWidget *parent = 0);
    ~ReulstsView();

private:
    Ui::ReulstsView *ui;
};

#endif // REULSTSVIEW_H
