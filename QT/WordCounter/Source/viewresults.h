#ifndef VIEWRESULTS_H
#define VIEWRESULTS_H

#include <QWidget>

namespace Ui {
class viewresults;
}

class viewresults : public QWidget
{
    Q_OBJECT

public:
    explicit viewresults(QWidget *parent = 0);
    ~viewresults();

private:
    Ui::viewresults *ui;
};

#endif // VIEWRESULTS_H
