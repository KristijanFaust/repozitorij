#-------------------------------------------------
#
# Project created by QtCreator 2016-07-14T18:19:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WordCounter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    resultsview.cpp \
    globals.cpp

HEADERS  += mainwindow.h \
    resultsview.h \
    globals.h

FORMS    += mainwindow.ui \
    resultsview.ui
