#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QTableWidget>
#include <QTableWidgetItem>
#include "globals.h"
#include "resultsview.h"
#include "ui_resultsview.h"

class TableItem : public QTableWidgetItem
{
public:

    TableItem(const QString & text):QTableWidgetItem(text){}
    TableItem(int num):QTableWidgetItem(QString::number(num)){}

    bool operator< (const QTableWidgetItem &other) const

    {
        if (other.column() == 1) { return text().toInt() < other.text().toInt();}
        return other.text() < text();
    }

};

ResultsView::ResultsView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ResultsView)
{
    ui->setupUi(this);
    this->setWindowTitle(windowTitle() + " - " + Globals::resultsTitle);

    QFile results(Globals::resultsFile);
    if (!results.open(QIODevice::ReadOnly))
        QMessageBox::information(0, "Error", results.errorString());

    QTextStream in(&results);

    ui->resultsBrowser->setRowCount(0);
    ui->resultsBrowser->setColumnCount(2);
    ui->resultsBrowser->setHorizontalHeaderLabels(QStringList() << "Sequence" << "Occurrence");
    ui->resultsBrowser->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    while(!in.atEnd()) {
        QString line = in.readLine();
        QStringList cell = line.split(" ");
        ui->resultsBrowser->insertRow( ui->resultsBrowser->rowCount() );

        QTableWidgetItem *Cell1 = new QTableWidgetItem();
        Cell1->setText(cell[1]);
        ui->resultsBrowser->setItem(ui->resultsBrowser->rowCount()-1, 0, Cell1);

        ui->resultsBrowser->setItem(ui->resultsBrowser->rowCount()-1, 1, new TableItem(cell[2]));

    }

    results.close();
    ui->resultsBrowser->setSortingEnabled(true);

}

ResultsView::~ResultsView()
{
    delete ui;
}
