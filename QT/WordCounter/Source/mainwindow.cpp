#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QProcess>
#include <QCheckBox>
#include <QLineEdit>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "globals.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFile path("Communications/path.txt");
    if (!path.open(QIODevice::ReadOnly))
        ui->PathLabel->setText("No file is loaded.\n");
    else {

        QTextStream in(&path);
        QString pathName;
        in>>pathName;
        if (pathName!=""){
            ui->PathLabel->setText("Current file loaded: " + pathName + "\n");
            if(pathName.size()>80)
                ui->PathLabel->setText("File loaded. Path is too long to display properly.");
            }
        else
            ui->PathLabel->setText("No file is loaded.\n");

    path.close();
    }

    ui->RawLabelDescription->setText("Raw data scaning will not be perforemd.\n");
    ui->WordsLabelDescription->setText("Words only scaning will not be perforemd.\n");
    ui->RestLabelDescription->setText("Scaning that separates puncations and digits from sequences and counts them indipendently will not be perforemd.\n");
    ui->PunctationsLabelDescription->setText("Scaning that separates: , . ; ! and ? and counts them indipendently will not be perforemd.\n");
    ui->IgnoreLabelDescription->setText("No sequence will be ignored in the results.\n");
    ui->ConsiderLabelDescription->setText("No specific sequence targeted.\n");



}

MainWindow::~MainWindow()
{
    QFile ignore("Communications/ignore.txt");
    ignore.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream ignoreout(&ignore);
    ignoreout<<"";
    ignore.close();

    QFile consider("Communications/consider.txt");
    consider.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream considerout(&consider);
    considerout<<"";
    consider.close();

    delete ui;
}

void MainWindow::on_OpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(), tr("Text Files (*.txt);;All Files (*.*)"));

    if (!fileName.isEmpty()) {

        QFile path("Communications/path.txt");
        path.open(QIODevice::ReadWrite | QIODevice::Truncate);
        QTextStream out(&path);
        out<<fileName;
        path.close();
        statusBar()->showMessage("File loaded:   " + fileName, 5000);
        ui->PathLabel->setText("Current file loaded: " + fileName + "\n");
        }

    else{

        QMessageBox::information( this, tr("Error"), tr("File is unreadable.") );

    }

}


void MainWindow::on_ExitButton_clicked()
{
    QFile ignore("Communications/ignore.txt");
    ignore.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream ignoreout(&ignore);
    ignoreout<<"";
    ignore.close();

    QFile consider("Communications/consider.txt");
    consider.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream considerout(&consider);
    considerout<<"";
    consider.close();

    qApp->quit();
}

void MainWindow::on_AnalyzeFile_clicked()
{
    if (ui->wordsonlyCheck->isChecked()){

    QString program = "Tools/words_only.exe";
    QProcess *AnalyzeFile = new QProcess(this);
    AnalyzeFile->start(program);
    AnalyzeFile->waitForFinished();

    QByteArray statusoutput = AnalyzeFile->readAllStandardOutput();
    QFile status("Communications/statuslog_wordsonly.txt");
    status.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream out(&status);
    out<<statusoutput;
    status.close();

    Globals::resultsFile = "Communications/results_words.txt";
    Globals::resultsTitle = "Words only";

    rv = new ResultsView(this);
    rv->show();

    }

    if (ui->rawdataCheck->isChecked()){

    QString program = "Tools/raw_data.exe";
    QProcess *AnalyzeFile = new QProcess(this);
    AnalyzeFile->start(program);
    AnalyzeFile->waitForFinished();

    QByteArray statusoutput = AnalyzeFile->readAllStandardOutput();
    QFile status("Communications/statuslog_rawdata.txt");
    status.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream out(&status);
    out<<statusoutput;
    status.close();

    Globals::resultsFile = "Communications/results_raw.txt";
    Globals::resultsTitle = "Raw data";

    rv = new ResultsView(this);
    rv->show();

    }

    if (ui->punctationsCheck->isChecked()){

    QString program = "Tools/separate_punctations.exe";
    QProcess *AnalyzeFile = new QProcess(this);
    AnalyzeFile->start(program);
    AnalyzeFile->waitForFinished();

    QByteArray statusoutput = AnalyzeFile->readAllStandardOutput();
    QFile status("Communications/statuslog_puncations.txt");
    status.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream out(&status);
    out<<statusoutput;
    status.close();

    Globals::resultsFile = "Communications/results_punctations.txt";
    Globals::resultsTitle = "Separated punctations";

    rv = new ResultsView(this);
    rv->show();

    }

    if (ui->wordsandrestCheck->isChecked()){

    QString program = "Tools/words_and_rest.exe";
    QProcess *AnalyzeFile = new QProcess(this);
    AnalyzeFile->start(program);
    AnalyzeFile->waitForFinished();

    QByteArray statusoutput = AnalyzeFile->readAllStandardOutput();
    QFile status("Communications/statuslog_wordsandrest.txt");
    status.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream out(&status);
    out<<statusoutput;
    status.close();

    Globals::resultsFile = "Communications/results_wordsandrest.txt";
    Globals::resultsTitle = "Separated words and rest";

    rv = new ResultsView(this);
    rv->show();

    }



    if (ui->rawdataCheck->isChecked()==0 && ui->wordsonlyCheck->isChecked()==0 && ui->punctationsCheck->isChecked()==0 && ui->wordsandrestCheck->isChecked()==0 ) {

        QMessageBox::information( this, tr("Please chose an option."), tr("No option is checked."));

    }
}

void MainWindow::on_IgnoreLineEdit_textEdited(const QString &txt)
{
    QFile path("Communications/ignore.txt");
    path.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream out(&path);
    out<<txt;
    path.close();
    statusBar()->showMessage("Ignoring: " + txt, 5000);
    ui->IgnoreLabelDescription->setText("Entered character sequences separated by whitespaces will be ignored in the results.");
    if (txt=="")
        ui->IgnoreLabelDescription->setText("No sequence will be ignored in the results.\n");
}

void MainWindow::on_ConsiderLineEdit_textEdited(const QString &txt)
{
    QFile path("Communications/consider.txt");
    path.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream out(&path);
    out<<txt;
    path.close();
    statusBar()->showMessage("Considering only: " + txt, 5000);
    ui->ConsiderLabelDescription->setText("Only entered character sequences separated by whitespaces will be considered in the results. Any other sequences will be ignored. This filter overrides the ignoring sequences.");
    if (txt=="")
        ui->ConsiderLabelDescription->setText("No specific sequence targeted.\n");
}

void MainWindow::on_rawdataCheck_toggled(bool checked)
{
    if (checked==1)
        ui->RawLabelDescription->setText("Every sequence separated with a whitespace will be counted separately as it is. \n");
    else
        ui->RawLabelDescription->setText("Raw data scaning will not be perforemd.\n");
}

void MainWindow::on_wordsonlyCheck_toggled(bool checked)
{
    if (checked==1)
        ui->WordsLabelDescription->setText("Every sequence separated with a whitespace will be filtered to contain only letters and counted as such. \n");
    else
        ui->WordsLabelDescription->setText("Words only scaning will not be perforemd.\n");
}

void MainWindow::on_wordsandrestCheck_toggled(bool checked)
{
    if (checked==1)
        ui->RestLabelDescription->setText("Any digits or punctations will be separated at any position from all sequences and counted indipendetly.\n");
    else
        ui->RestLabelDescription->setText("Scaning that separates puncations and digits from sequences and counts them indipendently will not be perforemd.\n");

}

void MainWindow::on_punctationsCheck_toggled(bool checked)
{
    if (checked==1)
        ui->PunctationsLabelDescription->setText("Punctations: , . ; ! ? will be separated from all sequences at any position and counted indipendently.\n");
    else
        ui->PunctationsLabelDescription->setText("Scaning that separates: , . ; ! and ? and counts them indipendently will not be perforemd.\n");
}
