#ifndef GLOBALS_H
#define GLOBALS_H

#include <QString>

class Globals
{
    public:
    static QString resultsFile;
    static QString resultsTitle;
};

#endif // GLOBALS_H
