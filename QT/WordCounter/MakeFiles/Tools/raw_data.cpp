#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

int main()
{
	
std::string pathName;
std::ifstream path ("Communications/path.txt");
std::getline(path, pathName);
path.close();

std::ifstream scan (pathName); 

if (!scan.is_open())
    {
         std::cout<<"["<<pathName<<"] - Error: File not found!"<<std::endl;
         return 1;
    }

std::ifstream ignoreStrings ("Communications/ignore.txt");
if (!ignoreStrings.is_open())
    {
         std::cout<<"[ignore.txt] - No strings to ignore."<<std::endl;
    }

std::ifstream considerStrings ("Communications/consider.txt");
if (!considerStrings.is_open())
    {
         std::cout<<"[consider.txt] - No strings to consider."<<std::endl;
    }

std::cout<<"["<<pathName<<"] - File open"<<std::endl;

std::vector<std::vector<std::string>> main_data_container;
std::string line;

std::vector<std::string> ignore;
std::vector<std::string> consider;
std::string ignore_chunk;
std::string consider_chunk;

if (ignoreStrings.is_open()){
	while (ignoreStrings>>ignore_chunk)
		{
			ignore.push_back(ignore_chunk);
		}
	
	std::cout<<"[ignore.txt] - Ignoring: ";
	for ( std::vector<std::string>::const_iterator it = ignore.begin();it != ignore.end(); ++it )
	{
		std::cout<<*it<<" ";
	}
	
	ignoreStrings.close();
}

if (considerStrings.is_open()){
	while (considerStrings>>consider_chunk)
		{
			consider.push_back(consider_chunk);
		}
	
	std::cout<<"[consider.txt] - Considering only: ";
	for ( std::vector<std::string>::const_iterator it = consider.begin();it != consider.end(); ++it )
	{
		std::cout<<*it<<" ";
	}

	considerStrings.close();
}

while (std::getline(scan, line))
    {
        std::vector<std::string> temporary_vector;
        std::string chunk;
        std::istringstream linestream (line);


        while (linestream>>chunk)
            {
                temporary_vector.push_back(chunk);
            }
			
        if (temporary_vector.size())
        main_data_container.emplace_back(std::move(temporary_vector));
    }


scan.close();

std::cout<<"[main_data_container] - Data imported"<<std::endl;
std::cout<<"[main_data_container] - Processing..."<<std::endl;

std::map<std::string, int> chunk_occurrences;

for (unsigned int i=0; i<main_data_container.size(); i++)
    {
     for (unsigned int j=0; j<main_data_container[i].size();j++)
        {
                main_data_container[i][j];
                auto it = chunk_occurrences.find(main_data_container[i][j]);
                if (it != chunk_occurrences.end())
                    it->second+=1;
                else
                    chunk_occurrences[main_data_container[i][j]] = 1;
        }
    }

std::cout<<"[chunk_occurrences] - Data processed"<<std::endl;

std::vector<std::pair<int, std::string>> results;
std::cout<<"[results] - Sorting data by occurrence..."<<std::endl;

for ( std::map< std::string, int >::iterator it = chunk_occurrences.begin();it != chunk_occurrences.end(); ++it )
{
    results.push_back(std::make_pair(it->second,it->first));
}

std::sort(results.begin(), results.end());

std::cout<<"[results] - Data sorted"<<std::endl;

std::cout<<"[results] - Outputing data..."<<std::endl;

std::ofstream output_data ("Communications/results_raw.txt");

//std::cout<<" \nResults:\n\n";

if (consider.size()!=0){
	
	bool considering = false;
	for ( std::vector<std::pair<int, std::string>>::const_iterator it = results.begin();it != results.end(); ++it )
		{
			considering = false;
			for ( std::vector<std::string>::const_iterator itv = consider.begin();itv != consider.end(); ++itv )
				{
					if(it->second==*itv)
						considering = true;
				}
				
			if(considering == true)
				output_data<<" "<<it->second<<" "<<std::fixed<<it->first<<std::endl;

		}
}

else if (ignore.size()!=0){
	
	bool ignoring = false;
	for ( std::vector<std::pair<int, std::string>>::const_iterator it = results.begin();it != results.end(); ++it )
		{
			ignoring = false;
			for ( std::vector<std::string>::const_iterator itv = ignore.begin();itv != ignore.end(); ++itv )
				{
					if(it->second==*itv)
						ignoring = true;
				}
				
			if(ignoring == false)
				output_data<<" "<<it->second<<" "<<std::fixed<<it->first<<std::endl;

			else
				std::cout<<it->second<<" ignored."<<std::endl;
    }
}

else {

	for ( std::vector<std::pair<int, std::string>>::const_iterator it = results.begin();it != results.end(); ++it )
	{
		//std::cout<<" "<<it->second<<" = "<<std::fixed<<it->first<<std::endl;
		output_data<<" "<<it->second<<" "<<std::fixed<<it->first<<std::endl;
	}

}

output_data.close();

std::cout<<"\n[output_data] - Data out. Program Terminated\n";

return 0;
}
