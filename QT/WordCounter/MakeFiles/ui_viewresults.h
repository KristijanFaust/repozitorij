/********************************************************************************
** Form generated from reading UI file 'viewresults.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWRESULTS_H
#define UI_VIEWRESULTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_viewresults
{
public:
    QPushButton *pushButton;

    void setupUi(QWidget *viewresults)
    {
        if (viewresults->objectName().isEmpty())
            viewresults->setObjectName(QStringLiteral("viewresults"));
        viewresults->setWindowModality(Qt::WindowModal);
        viewresults->resize(400, 300);
        pushButton = new QPushButton(viewresults);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(90, 160, 75, 23));

        retranslateUi(viewresults);

        QMetaObject::connectSlotsByName(viewresults);
    } // setupUi

    void retranslateUi(QWidget *viewresults)
    {
        viewresults->setWindowTitle(QApplication::translate("viewresults", "Form", 0));
        pushButton->setText(QApplication::translate("viewresults", "PushButton", 0));
    } // retranslateUi

};

namespace Ui {
    class viewresults: public Ui_viewresults {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWRESULTS_H
