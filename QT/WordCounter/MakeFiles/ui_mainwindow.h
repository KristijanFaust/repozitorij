/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *OpenFile;
    QPushButton *ExitButton;
    QPushButton *AnalyzeFile;
    QLabel *OptionsLabel;
    QCheckBox *wordsonlyCheck;
    QCheckBox *wordsandrestCheck;
    QCheckBox *punctationsCheck;
    QCheckBox *rawdataCheck;
    QLineEdit *IgnoreLineEdit;
    QLabel *Ignoretext;
    QLabel *Considertext;
    QLineEdit *ConsiderLineEdit;
    QLabel *PathLabel;
    QLabel *RawLabelDescription;
    QLabel *IgnoreLabelDescription;
    QLabel *WordsLabelDescription;
    QLabel *RestLabelDescription;
    QLabel *PunctationsLabelDescription;
    QLabel *ConsiderLabelDescription;
    QLabel *Hlabel_1;
    QLabel *Hlabel_2;
    QLabel *Hlabel_3;
    QLabel *Hlabel_4;
    QLabel *Hlabel_5;
    QLabel *Hlabel_6;
    QLabel *Hlabel_7;
    QLabel *Vlabel_1;
    QLabel *Vlabel_2;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(897, 588);
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0);"));
        OpenFile = new QPushButton(centralWidget);
        OpenFile->setObjectName(QStringLiteral("OpenFile"));
        OpenFile->setGeometry(QRect(10, 20, 50, 50));
        OpenFile->setMouseTracking(true);
        OpenFile->setStyleSheet(QLatin1String("QPushButton#OpenFile:hover {\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    border-color: orange;\n"
" }\n"
"\n"
"QPushButton#OpenFile:hover:pressed\n"
"{\n"
"    border-width: 2px;\n"
"    border-style: solid;\n"
"    border-color: orange;\n"
"	background-color: \"yellow\";\n"
"}"));
        QIcon icon;
        icon.addFile(QStringLiteral("../Elements/open.png"), QSize(), QIcon::Normal, QIcon::Off);
        OpenFile->setIcon(icon);
        OpenFile->setIconSize(QSize(32, 32));
        ExitButton = new QPushButton(centralWidget);
        ExitButton->setObjectName(QStringLiteral("ExitButton"));
        ExitButton->setGeometry(QRect(190, 20, 50, 50));
        ExitButton->setStyleSheet(QLatin1String("QPushButton#ExitButton:hover {\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    border-color: orange;\n"
" }\n"
"\n"
"QPushButton#ExitButton:hover:pressed\n"
"{\n"
"    border-width: 2px;\n"
"    border-style: solid;\n"
"    border-color: orange;\n"
"	background-color: \"yellow\";\n"
"}"));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../Elements/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        ExitButton->setIcon(icon1);
        ExitButton->setIconSize(QSize(32, 32));
        AnalyzeFile = new QPushButton(centralWidget);
        AnalyzeFile->setObjectName(QStringLiteral("AnalyzeFile"));
        AnalyzeFile->setGeometry(QRect(100, 20, 50, 50));
        AnalyzeFile->setMouseTracking(true);
        AnalyzeFile->setStyleSheet(QLatin1String("QPushButton#AnalyzeFile:hover {\n"
"    border-width: 1px;\n"
"    border-style: solid;\n"
"    border-color: orange;\n"
" }\n"
"\n"
"QPushButton#AnalyzeFile:hover:pressed\n"
"{\n"
"    border-width: 2px;\n"
"    border-style: solid;\n"
"    border-color: orange;\n"
"	background-color: \"yellow\";\n"
"}"));
        QIcon icon2;
        icon2.addFile(QStringLiteral("../Elements/analyze.png"), QSize(), QIcon::Normal, QIcon::Off);
        AnalyzeFile->setIcon(icon2);
        AnalyzeFile->setIconSize(QSize(32, 32));
        OptionsLabel = new QLabel(centralWidget);
        OptionsLabel->setObjectName(QStringLiteral("OptionsLabel"));
        OptionsLabel->setGeometry(QRect(10, 100, 71, 16));
        QFont font;
        font.setFamily(QStringLiteral("Arial"));
        font.setPointSize(9);
        font.setBold(true);
        font.setWeight(75);
        OptionsLabel->setFont(font);
        OptionsLabel->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        OptionsLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        wordsonlyCheck = new QCheckBox(centralWidget);
        wordsonlyCheck->setObjectName(QStringLiteral("wordsonlyCheck"));
        wordsonlyCheck->setGeometry(QRect(10, 190, 221, 17));
        wordsonlyCheck->setFont(font);
        wordsonlyCheck->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        wordsandrestCheck = new QCheckBox(centralWidget);
        wordsandrestCheck->setObjectName(QStringLiteral("wordsandrestCheck"));
        wordsandrestCheck->setGeometry(QRect(10, 240, 221, 17));
        wordsandrestCheck->setFont(font);
        wordsandrestCheck->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        punctationsCheck = new QCheckBox(centralWidget);
        punctationsCheck->setObjectName(QStringLiteral("punctationsCheck"));
        punctationsCheck->setGeometry(QRect(10, 290, 221, 17));
        punctationsCheck->setFont(font);
        punctationsCheck->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        rawdataCheck = new QCheckBox(centralWidget);
        rawdataCheck->setObjectName(QStringLiteral("rawdataCheck"));
        rawdataCheck->setGeometry(QRect(10, 140, 221, 17));
        rawdataCheck->setFont(font);
        rawdataCheck->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        IgnoreLineEdit = new QLineEdit(centralWidget);
        IgnoreLineEdit->setObjectName(QStringLiteral("IgnoreLineEdit"));
        IgnoreLineEdit->setGeometry(QRect(10, 400, 231, 20));
        IgnoreLineEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        Ignoretext = new QLabel(centralWidget);
        Ignoretext->setObjectName(QStringLiteral("Ignoretext"));
        Ignoretext->setGeometry(QRect(10, 370, 141, 16));
        Ignoretext->setFont(font);
        Ignoretext->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        Considertext = new QLabel(centralWidget);
        Considertext->setObjectName(QStringLiteral("Considertext"));
        Considertext->setGeometry(QRect(10, 450, 181, 16));
        Considertext->setFont(font);
        Considertext->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        ConsiderLineEdit = new QLineEdit(centralWidget);
        ConsiderLineEdit->setObjectName(QStringLiteral("ConsiderLineEdit"));
        ConsiderLineEdit->setGeometry(QRect(10, 480, 231, 20));
        ConsiderLineEdit->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        PathLabel = new QLabel(centralWidget);
        PathLabel->setObjectName(QStringLiteral("PathLabel"));
        PathLabel->setGeometry(QRect(320, 30, 511, 51));
        QFont font1;
        font1.setPointSize(8);
        PathLabel->setFont(font1);
        PathLabel->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        PathLabel->setScaledContents(true);
        PathLabel->setWordWrap(true);
        RawLabelDescription = new QLabel(centralWidget);
        RawLabelDescription->setObjectName(QStringLiteral("RawLabelDescription"));
        RawLabelDescription->setGeometry(QRect(320, 140, 511, 31));
        RawLabelDescription->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        RawLabelDescription->setScaledContents(true);
        RawLabelDescription->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        RawLabelDescription->setWordWrap(true);
        IgnoreLabelDescription = new QLabel(centralWidget);
        IgnoreLabelDescription->setObjectName(QStringLiteral("IgnoreLabelDescription"));
        IgnoreLabelDescription->setGeometry(QRect(320, 400, 511, 41));
        IgnoreLabelDescription->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        IgnoreLabelDescription->setScaledContents(true);
        IgnoreLabelDescription->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        IgnoreLabelDescription->setWordWrap(true);
        WordsLabelDescription = new QLabel(centralWidget);
        WordsLabelDescription->setObjectName(QStringLiteral("WordsLabelDescription"));
        WordsLabelDescription->setGeometry(QRect(320, 190, 511, 31));
        WordsLabelDescription->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        WordsLabelDescription->setScaledContents(true);
        WordsLabelDescription->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        WordsLabelDescription->setWordWrap(true);
        RestLabelDescription = new QLabel(centralWidget);
        RestLabelDescription->setObjectName(QStringLiteral("RestLabelDescription"));
        RestLabelDescription->setGeometry(QRect(320, 240, 511, 31));
        RestLabelDescription->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        RestLabelDescription->setScaledContents(true);
        RestLabelDescription->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        RestLabelDescription->setWordWrap(true);
        PunctationsLabelDescription = new QLabel(centralWidget);
        PunctationsLabelDescription->setObjectName(QStringLiteral("PunctationsLabelDescription"));
        PunctationsLabelDescription->setGeometry(QRect(320, 290, 511, 31));
        PunctationsLabelDescription->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        PunctationsLabelDescription->setScaledContents(true);
        PunctationsLabelDescription->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        PunctationsLabelDescription->setWordWrap(true);
        ConsiderLabelDescription = new QLabel(centralWidget);
        ConsiderLabelDescription->setObjectName(QStringLiteral("ConsiderLabelDescription"));
        ConsiderLabelDescription->setGeometry(QRect(320, 480, 511, 41));
        ConsiderLabelDescription->setStyleSheet(QStringLiteral("color: rgb(255, 85, 0);"));
        ConsiderLabelDescription->setScaledContents(true);
        ConsiderLabelDescription->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        ConsiderLabelDescription->setWordWrap(true);
        Hlabel_1 = new QLabel(centralWidget);
        Hlabel_1->setObjectName(QStringLiteral("Hlabel_1"));
        Hlabel_1->setGeometry(QRect(310, 70, 541, 16));
        Hlabel_1->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Hlabel_2 = new QLabel(centralWidget);
        Hlabel_2->setObjectName(QStringLiteral("Hlabel_2"));
        Hlabel_2->setGeometry(QRect(310, 170, 541, 16));
        Hlabel_2->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Hlabel_3 = new QLabel(centralWidget);
        Hlabel_3->setObjectName(QStringLiteral("Hlabel_3"));
        Hlabel_3->setGeometry(QRect(310, 220, 541, 16));
        Hlabel_3->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Hlabel_4 = new QLabel(centralWidget);
        Hlabel_4->setObjectName(QStringLiteral("Hlabel_4"));
        Hlabel_4->setGeometry(QRect(310, 270, 541, 16));
        Hlabel_4->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Hlabel_5 = new QLabel(centralWidget);
        Hlabel_5->setObjectName(QStringLiteral("Hlabel_5"));
        Hlabel_5->setGeometry(QRect(310, 320, 541, 16));
        Hlabel_5->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Hlabel_6 = new QLabel(centralWidget);
        Hlabel_6->setObjectName(QStringLiteral("Hlabel_6"));
        Hlabel_6->setGeometry(QRect(310, 430, 541, 16));
        Hlabel_6->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Hlabel_7 = new QLabel(centralWidget);
        Hlabel_7->setObjectName(QStringLiteral("Hlabel_7"));
        Hlabel_7->setGeometry(QRect(310, 520, 541, 16));
        Hlabel_7->setPixmap(QPixmap(QString::fromUtf8("../Elements/Hline.png")));
        Vlabel_1 = new QLabel(centralWidget);
        Vlabel_1->setObjectName(QStringLiteral("Vlabel_1"));
        Vlabel_1->setGeometry(QRect(270, 20, 31, 541));
        Vlabel_1->setPixmap(QPixmap(QString::fromUtf8("../Elements/Vline.png")));
        Vlabel_1->setScaledContents(false);
        Vlabel_1->setWordWrap(false);
        Vlabel_2 = new QLabel(centralWidget);
        Vlabel_2->setObjectName(QStringLiteral("Vlabel_2"));
        Vlabel_2->setGeometry(QRect(830, 20, 31, 541));
        Vlabel_2->setPixmap(QPixmap(QString::fromUtf8("../Elements/Vline.png")));
        Vlabel_2->setScaledContents(false);
        Vlabel_2->setWordWrap(false);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 897, 21));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        QPalette palette;
        QBrush brush(QColor(255, 85, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        statusBar->setPalette(palette);
        statusBar->setStyleSheet(QLatin1String("color: rgb(255, 85, 0);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Word Counter", 0));
#ifndef QT_NO_TOOLTIP
        OpenFile->setToolTip(QApplication::translate("MainWindow", "Click to open a file", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        OpenFile->setWhatsThis(QApplication::translate("MainWindow", "Open file", 0));
#endif // QT_NO_WHATSTHIS
        OpenFile->setText(QString());
#ifndef QT_NO_TOOLTIP
        ExitButton->setToolTip(QApplication::translate("MainWindow", "Click to exit the program", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        ExitButton->setWhatsThis(QApplication::translate("MainWindow", "Exit program", 0));
#endif // QT_NO_WHATSTHIS
        ExitButton->setText(QString());
#ifndef QT_NO_TOOLTIP
        AnalyzeFile->setToolTip(QApplication::translate("MainWindow", "Click to analyze file", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_WHATSTHIS
        AnalyzeFile->setWhatsThis(QApplication::translate("MainWindow", "Analyize file", 0));
#endif // QT_NO_WHATSTHIS
        AnalyzeFile->setText(QString());
        OptionsLabel->setText(QApplication::translate("MainWindow", "Options:", 0));
#ifndef QT_NO_TOOLTIP
        wordsonlyCheck->setToolTip(QApplication::translate("MainWindow", "Clean punctations and digits from file and consider only words. This will erase punctations and numbers at any place in a sequence.", 0));
#endif // QT_NO_TOOLTIP
        wordsonlyCheck->setText(QApplication::translate("MainWindow", "Words only", 0));
#ifndef QT_NO_TOOLTIP
        wordsandrestCheck->setToolTip(QApplication::translate("MainWindow", "Count each word, puncation and digit separately. This will separate puncations and digits at any place in a sequence.", 0));
#endif // QT_NO_TOOLTIP
        wordsandrestCheck->setText(QApplication::translate("MainWindow", "Separate word and rest", 0));
#ifndef QT_NO_TOOLTIP
        punctationsCheck->setToolTip(QApplication::translate("MainWindow", "Separate only , . ; ! ? and count the rest as it is.", 0));
#endif // QT_NO_TOOLTIP
        punctationsCheck->setText(QApplication::translate("MainWindow", "Separate words and punctations", 0));
#ifndef QT_NO_TOOLTIP
        rawdataCheck->setToolTip(QApplication::translate("MainWindow", "Scan the file as it is.", 0));
#endif // QT_NO_TOOLTIP
        rawdataCheck->setText(QApplication::translate("MainWindow", "Raw data", 0));
#ifndef QT_NO_TOOLTIP
        IgnoreLineEdit->setToolTip(QApplication::translate("MainWindow", "Enter sequences to be ignored, separated by whitespaces.", 0));
#endif // QT_NO_TOOLTIP
        Ignoretext->setText(QApplication::translate("MainWindow", "Words to be ignored:", 0));
        Considertext->setText(QApplication::translate("MainWindow", "Words to be considered only:", 0));
#ifndef QT_NO_TOOLTIP
        ConsiderLineEdit->setToolTip(QApplication::translate("MainWindow", "Enter sequences to be considered only, separated by whitespaces. If a sequence is entered it will override the ignored sequences.", 0));
#endif // QT_NO_TOOLTIP
        PathLabel->setText(QString());
        RawLabelDescription->setText(QString());
        IgnoreLabelDescription->setText(QString());
        WordsLabelDescription->setText(QString());
        RestLabelDescription->setText(QString());
        PunctationsLabelDescription->setText(QString());
        ConsiderLabelDescription->setText(QString());
        Hlabel_1->setText(QString());
        Hlabel_2->setText(QString());
        Hlabel_3->setText(QString());
        Hlabel_4->setText(QString());
        Hlabel_5->setText(QString());
        Hlabel_6->setText(QString());
        Hlabel_7->setText(QString());
        Vlabel_1->setText(QString());
        Vlabel_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
