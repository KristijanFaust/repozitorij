/********************************************************************************
** Form generated from reading UI file 'resultsview.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RESULTSVIEW_H
#define UI_RESULTSVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ResultsView
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QTableWidget *resultsBrowser;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ResultsView)
    {
        if (ResultsView->objectName().isEmpty())
            ResultsView->setObjectName(QStringLiteral("ResultsView"));
        ResultsView->resize(800, 600);
        ResultsView->setStyleSheet(QStringLiteral(""));
        centralwidget = new QWidget(ResultsView);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 40, 61, 21));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        label->setStyleSheet(QStringLiteral(""));
        resultsBrowser = new QTableWidget(centralwidget);
        resultsBrowser->setObjectName(QStringLiteral("resultsBrowser"));
        resultsBrowser->setGeometry(QRect(30, 70, 400, 480));
        resultsBrowser->setStyleSheet(QStringLiteral(""));
        ResultsView->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(ResultsView);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        ResultsView->setStatusBar(statusbar);

        retranslateUi(ResultsView);

        QMetaObject::connectSlotsByName(ResultsView);
    } // setupUi

    void retranslateUi(QMainWindow *ResultsView)
    {
        ResultsView->setWindowTitle(QApplication::translate("ResultsView", "Results View", 0));
        label->setText(QApplication::translate("ResultsView", "Results:", 0));
    } // retranslateUi

};

namespace Ui {
    class ResultsView: public Ui_ResultsView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RESULTSVIEW_H
