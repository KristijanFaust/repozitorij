/********************************************************************************
** Form generated from reading UI file 'reulstsview.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REULSTSVIEW_H
#define UI_REULSTSVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReulstsView
{
public:
    QWidget *centralwidget;
    QPushButton *pushButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ReulstsView)
    {
        if (ReulstsView->objectName().isEmpty())
            ReulstsView->setObjectName(QStringLiteral("ReulstsView"));
        ReulstsView->resize(800, 600);
        centralwidget = new QWidget(ReulstsView);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(230, 300, 75, 23));
        ReulstsView->setCentralWidget(centralwidget);
        menubar = new QMenuBar(ReulstsView);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        ReulstsView->setMenuBar(menubar);
        statusbar = new QStatusBar(ReulstsView);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        ReulstsView->setStatusBar(statusbar);

        retranslateUi(ReulstsView);

        QMetaObject::connectSlotsByName(ReulstsView);
    } // setupUi

    void retranslateUi(QMainWindow *ReulstsView)
    {
        ReulstsView->setWindowTitle(QApplication::translate("ReulstsView", "MainWindow", 0));
        pushButton->setText(QApplication::translate("ReulstsView", "PushButton", 0));
    } // retranslateUi

};

namespace Ui {
    class ReulstsView: public Ui_ReulstsView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REULSTSVIEW_H
